try:
    from utils import *
except BaseException as e:
    from .utils import *
import copy
import numpy as np
import multiprocessing
import tensorflow as tf

fix_gpu_memory()

class GeneticSearch:
    CROSS_OVER_SIZE = 8
    RANDOM_MUTATION_COUNT = 2
    BEST_DNA_MUTATION_COUNT = 2
    NUMBER_OF_BEST_DNA = 8

    def __init__(self, n_child, handler, model_maker, number_of_card, number_of_run_per_card=1,
                 number_of_run_per_dna=10, iteration=10, zipper=None):
        self.n_child = n_child
        self.handler = handler
        self.model_maker = model_maker
        self.zipper = zipper
        self.base_cols = copy.deepcopy(self.handler.cols)
        self.number_of_cols = len(self.base_cols) - 1
        self.number_of_run_per_dna = number_of_run_per_dna
        self.iteration = iteration
        self.number_of_graphic_card = number_of_card
        self.number_of_run_per_card = number_of_run_per_card
        self.dna_pool = np.concatenate([np.ones((1, self.number_of_cols)),
                                        np.random.randint(0, 2, (2 * self.NUMBER_OF_BEST_DNA - 1, self.number_of_cols))
                                        ], axis=0)
        self.all_run_res = []

    def start(self):
        for i in range(self.iteration):
            self.one_search_run()

    def one_search_run(self):
        self.mix_dna()
        run_res = []
        for item in self.dna_pool:
            handle = self.make_data_handler_columns(item)
            run_res.append([item, GeneticSearch.one_model_multiple_run(
                                                                   handler=handle,
                                                                   number_of_run_per_dna=self.number_of_run_per_dna,
                                                                   model_maker=self.model_maker, zipper=self.zipper,
                                                                   number_of_graphic_card=self.number_of_graphic_card,
                                                                   number_of_run_per_card=self.number_of_run_per_card,
                                                                   is_for_mlp=False)
                            ])
        run_res = sorted(run_res, key=lambda x: max([trade['netProfit'] for trade in x[1]]), reverse=True)
        self.all_run_res.append(run_res)
        self.dna_pool = [item[0] for item in run_res[:self.NUMBER_OF_BEST_DNA]]
        print('dna best net profit:')
        print([max([trade['netProfit'] for trade in x[1]]) for x in run_res][::-1])

    def make_data_handler_columns(self, dna):
        dna_copy = np.concatenate([copy.deepcopy(dna), [1]], axis=0)
        base_df = copy.deepcopy(self.handler.dataframe)
        handle = copy.deepcopy(self.handler)

        handle.dataframe = np.delete(base_df, np.logical_not(dna_copy), 1)
        handle.cols = [self.base_cols[i] for i, item in enumerate(dna_copy) if item != 0]
        handle.feature_count = len(handle.cols) - 1
        handle.make_windows()
        return handle

    @staticmethod
    def input_wrapper(kwargs):
        return kwargs[0](**kwargs[1])

    @staticmethod
    def one_model_one_run(handler, model_maker, zipper, graphic_card_num, **model_maker_kwargs):
        print(f'graphic_card_num:{graphic_card_num}')
        with tf.device(f'/gpu:{graphic_card_num}'):
            model = model_maker(handler, run=False)
            analizer_obj = run_model(model, handler.make_train_test_data, handle=handler, show_figs=False,
                                     zipper=zipper, save_results=True, **model_maker_kwargs)
            return analizer_obj.trade_info

    @staticmethod
    def one_model_multiple_run(handler, number_of_run_per_dna, model_maker, zipper,
                               number_of_graphic_card, number_of_run_per_card, **model_maker_kwargs):
        res = []
        base_input = {'handler': handler,
                      'model_maker': model_maker,
                      'zipper': zipper}
        base_input.update(model_maker_kwargs)
        all_input = [[GeneticSearch.one_model_one_run,
                    GeneticSearch.copy_and_update_dict(
                        base_input, graphic_card_num=i % number_of_graphic_card)
                    ]
                    for i in range(number_of_run_per_dna)]
        thread_count = number_of_run_per_card*number_of_graphic_card
        with multiprocessing.Pool(thread_count) as pool:
            for i in range(thread_count, number_of_run_per_dna+1, thread_count):
                res.extend(pool.map(GeneticSearch.input_wrapper, all_input[i-thread_count:i]))
        res = sorted(res, key=lambda x: max([item['netProfit'] for item in x]))
        del handler
        return res[-1]

    @staticmethod
    def copy_and_update_dict(dic, **kwargs):
        ndic = {}
        ndic.update(dic)
        ndic.update(kwargs)
        return ndic

    def mix_dna(self):
        self.cross_over()
        self.random_mutation()
        self.from_best_dna_mutation()
        self.save_self_dna()

    def save_self_dna(self):
        with open('genetic_best_dna.pkl', 'wb') as f:
            pickle.dump(self.dna_pool, f)
        with open('genetic_all_run_res', 'wb') as f:
            pickle.dump(self.all_run_res, f)

    def cross_over(self):
        res = []
        res.extend(self.dna_pool[:len(self.dna_pool) // 2])
        number_of_coromosum = len(self.dna_pool)
        length_of_coromosum = len(self.dna_pool[0])

        for i in range((self.n_child - number_of_coromosum // 2) // 2):
            first_idx = np.random.randint(number_of_coromosum)
            second_idx = np.random.randint(number_of_coromosum)

            first = copy.deepcopy(self.dna_pool[first_idx])
            second = copy.deepcopy(self.dna_pool[second_idx])

            bunch = np.random.randint(1, self.CROSS_OVER_SIZE)
            first_slice = slice((bunch - 1) * (length_of_coromosum // self.CROSS_OVER_SIZE),
                                bunch * (length_of_coromosum // self.CROSS_OVER_SIZE))

            f = second[first_slice]
            s = first[first_slice]

            first[first_slice] = f
            second[first_slice] = s

            res.append(first)
            res.append(second)
        self.dna_pool = np.array(res)

    def random_mutation(self):
        for item in self.dna_pool:
            for _ in range(self.RANDOM_MUTATION_COUNT):
                idx = np.random.randint(len(item))
                item[idx] = 1 - item[idx]

    def from_best_dna_mutation(self):
        for i in range(len(self.dna_pool) // 2, len(self.dna_pool)):
            for _ in range(self.BEST_DNA_MUTATION_COUNT):
                candidate_best_dna_idx = np.random.randint(0, len(self.dna_pool) // 2)
                idx = np.random.randint(len(self.dna_pool[i]))
                self.dna_pool[i][idx] = self.dna_pool[candidate_best_dna_idx][idx]


if __name__ == '__main__':
    multiprocessing.set_start_method('spawn')
    zipper = ZipProjectPythonFiles()
    data_frame_paths = ['../dataset_4h_shift_0.csv']
    handle = DataHandler(data_frame_paths[0], kbins=4, encode='ordinal', strategy='quantile',
                         window_size=WINDOW_SIZE,
                         smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                         discretizer='discretizer1', purpose_colname='close_pct_change_3_smoother_3',
                         smooth_make_loss_profit_equal=True,
                         prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True,
                         pct_and_not_pct_together=False,
                         load_from_saved_dataFrame=False, multi_level_res=True,
                         number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                         add_zigzag_subwave=ADD_ZIGZAG_SUBWAVE, multi_level_intervals=MULTI_LEVEL_INTERVALS,
                         zig_zag_mode=ZIG_ZAG_MODE, zigzag_based_learning=ZIGZAG_BASED_LEARNING,
                         spread_base_learning=SPREAD_BASE_LEARNING,
                         back_for_base_learning=BACK_FOR_BASE_LEARNING,
                         back_zigzag_base_learning=BACK_ZIGZAG_BASE_LEARNING, pivot_mode=PIVOTE_MODE,
                         include_pivots_data=INCLUDE_PIVOTS_DATA,
                         inclue_and_update_pivots_data=INCLUDE_AND_UPDATE_PIVOTS_DATA,
                         open_close_1d_base_learning=OPEN_CLOSE_1D_BASE_LEARNING,
                         open_close_2d_base_learning=OPEN_CLOSE_2D_BASE_LEARNING)
    searcher = GeneticSearch(20, handler=handle, model_maker=run_lstm_model, number_of_run_per_card=2,
                             number_of_card=2, zipper=zipper, number_of_run_per_dna=12)

    try:
        searcher.start()

    except BaseException as e:
        print(e)
        searcher.save_self_dna()