from utils import *

import tensorflow as tf
import numpy as np
import functools


class MultiFutureMLPAnalizer:
    def __init__(self, handler: DataHandler, pre_model_path, layers_shape, epochs) -> None:
        self.handler = handler
        self.pre_model_path = pre_model_path
        self.layers = layers_shape
        self.epochs = epochs
        self.block_size = NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE
        self.early_stop_trades_info = []

    def load_model(self):
        self.pre_model = run_mlp_model(self.handler, run=False)
        self.pre_model.load_model(self.pre_model_path)
        print(self.pre_model.model.summary())

    def make_first_step_data(self):
        self.x, self.y, self.x_t, self.y_t = self.handler.make_train_test_data(is_for_mlp=False)
        self.handler.close_ = self.handler.close_[1 if len(self.y) % self.block_size != 0 else 0:]
        for k, v in self.x.items():
            self.x[k] = v[len(self.x['deep_input']) % self.block_size:]
        self.y = self.y[len(self.y) % self.block_size:]
        self.y = self.y[::self.block_size]
        self.y_t = self.y_t[::self.block_size]

    def get_pre_model_output(self):
        self.powers = self.pre_model.predict(self.x)
        idx = MultiFutureIndexSelector.get_index(len(self.powers), self.block_size)
        self.powers = self.powers[idx]
        self.powers = self.powers.reshape(len(self.powers), -1)

        self.powers_t = self.pre_model.predict(self.x_t)
        idx = MultiFutureIndexSelector.get_index(len(self.powers_t), self.block_size)
        self.powers_t = self.powers_t[idx]
        self.powers_t = self.powers_t.reshape(len(self.powers_t), -1)

    def build_model(self):
        deep_layer_input = tf.keras.layers.Input(shape=[self.layers[0]], name='deep_input')

        model = tf.keras.layers.Dense(self.layers[0], activation='relu')(deep_layer_input)
        model = tf.keras.layers.Dropout(.2, name=f'deep_drop0')(model)
        for i, item in enumerate(self.layers[1:-1]):
            model = tf.keras.layers.Dense(item, activation='relu')(model)
            model = tf.keras.layers.Dropout(.2, name=f'deep_drop{i+1}')(model)

        model = tf.keras.layers.Dense(self.layers[-1], activation='softmax')(model)
        model = tf.keras.Model(inputs=deep_layer_input, outputs=model)

        model.compile(loss=LOSS_FUNCTION, optimizer=OPTIMIZER, metrics=['accuracy'])
        print(model.summary())
        self.model = model

    def get_margin(self):
        pred = np.argmax(self.model.predict(self.powers_t), axis=1)
        print('acc:', len(pred[pred == np.argmax(self.y_t, axis=1)]) / len(pred) * 100)
        self.early_stop_trades_info.append([])
        for i in range(FUTURE_STEP + 1):
            y_real = self.handler.close_[-len(self.y_t) - i: -i if i != 0 else None].values
            trade_result = backTradeTesting(self.model, self.powers_t, np.array([0] * len(y_real)), 100000,
                                            100, y_real, True, False, 'power_model', None, 1, ).final()
            self.early_stop_trades_info[-1].append(trade_result)
            print('margin:', trade_result)

    def train(self):
        for epoch in range(self.epochs):
            self.model.fit(self.powers, self.y, epochs=1, batch_size=16, validation_split=0.2)
            self.get_margin()

    def start(self):
        self.load_model()
        self.make_first_step_data()
        self.get_pre_model_output()
        self.build_model()
        self.train()


if __name__ == '__main__':
    handle = DataHandlerManager(['../dataset_4h_shift_0.csv'], kbins=4, encode='ordinal', strategy='quantile',
                                window_size=WINDOW_SIZE,
                                smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                                discretizer='discretizer1', purpose_colname='close_pct_change_3_smoother_3',
                                smooth_make_loss_profit_equal=True,
                                prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True,
                                pct_and_not_pct_together=False,
                                load_from_saved_dataFrame=False, multi_level_res=True,
                                number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                                add_zigzag_subwave=ADD_ZIGZAG_SUBWAVE, multi_level_intervals=MULTI_LEVEL_INTERVALS,
                                zig_zag_mode=ZIG_ZAG_MODE, zigzag_based_learning=ZIGZAG_BASED_LEARNING,
                                spread_base_learning=SPREAD_BASE_LEARNING,
                                back_for_base_learning=BACK_FOR_BASE_LEARNING,
                                back_zigzag_base_learning=BACK_ZIGZAG_BASE_LEARNING, pivot_mode=PIVOTE_MODE,
                                include_pivots_data=INCLUDE_PIVOTS_DATA,
                                inclue_and_update_pivots_data=INCLUDE_AND_UPDATE_PIVOTS_DATA,
                                open_close_1d_base_learning=OPEN_CLOSE_1D_BASE_LEARNING,
                                open_close_2d_base_learning=OPEN_CLOSE_2D_BASE_LEARNING,
                                number_of_future=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE,
                                multi_future=MULTIPLE_FUTURE_BASE_LEARNING,
                                post_processor=functools.partial(MultiFuturePostProcessor,
                                                                 num_of_future=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE,
                                                                 window_size=WINDOW_SIZE))

    analizer = MultiFutureMLPAnalizer(handle, r'D:\E\projects\test_borse\genetic\2023_01_20_14_19_34\2023_01_20_14_19_34_LSTM.h5',
                                      [8, 400, 100, 2], 10)
    analizer.start()
