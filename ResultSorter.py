import glob, os, shutil, pickle, tqdm


def read_all_sub_folder_files(folder_name, file_to_open='basic_data*'):
    files = glob.glob(f'./{folder_name}/**/{file_to_open}.pkl', recursive=True)
    res = {}
    for file in tqdm.tqdm(files):
        with open(file, 'rb') as f:
            res[file] = pickle.load(f)
    return res


def sort_data(datas, sort_parameter='netProfit', function_to_map=max):
    tmp = {}
    for k, v in datas.items():
        if 'trade_details' not in v:
            continue
        tmp[k] = v
    for k, v in tmp.items():
        if isinstance(v['trade_details'], dict):
            v['trade_details'] = [v['trade_details']]
    datas = tmp
    return sorted(datas.items(), key=lambda x: function_to_map([item[sort_parameter]
                                                               for item in x[1]['trade_details']]))


def get_parameter_by_name(li, base_root='trade_details', param_name='netProfit'):
    return [item[param_name] for item in li[base_root]]


def main():
    results_folder = list(filter(lambda x: os.path.isdir(x) and x != 'utils' and not x.startswith('.'), os.listdir()))
    print('folder to search is:\n0 --> all\n' + '\n'.join([f'{i+1} --> {item}' for i, item in enumerate(results_folder)]))
    selected_folder = int(input('which folder to search?: '))
    if selected_folder == 0:
        print('all')
        res = {}
        for item in results_folder:
            res.update(read_all_sub_folder_files(item))
        res = sort_data(res)
    else:
        results_folder = results_folder[selected_folder-1]
        res = read_all_sub_folder_files(results_folder)
        res = sort_data(res)
    count = int(input('how may best to show?(default is 5): ') or 5)
    print(res[-count:])
    res = {item[0]: item[1] for item in res}
    print(*[get_parameter_by_name(item) for item in res.values()], sep='\n')


if __name__ == '__main__':
    main()