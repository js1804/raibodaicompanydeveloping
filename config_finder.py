try:
    from .utils import *
except BaseException as e:
    from utils import *
import numpy as np
import pickle
import time
import copy
import multiprocessing
import traceback


def find_best_future(start_index=0):
    res = []
    search_space = []

    for shift in range(1, 7):
        for smooth in range(3, 9):      
            for future in range(1, smooth+3):
                search_space.append([shift, smooth, future])

    for idx, [shift, smooth, future] in enumerate(search_space[start_index:]):
        print(f'step {idx + start_index} has started...')
        handle = DataHandler(DATAFRAME_PATH, 4, 'ordinal', 'quantile', WINDOW_SIZE, 
                        smooth_shift_size=[shift], smooth_window_size=[smooth],
                        discretizer='discretizer1', purpose_colname=f'close_pct_change_{shift}_smoother_{smooth}', 
                        smooth_make_loss_profit_equal=True, prediction_mode=PREDICTION_MODE, 
                        add_zig_zag_indicators=True, pct_change_on_all=True, pct_and_not_pct_together=False,
                        load_from_saved_dataFrame=False, multi_level_res=True, number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                        add_zigzag_subwave=False, multi_level_intervals=.2, zig_zag_mode=(True, True, True))

        handle.make_windows()  
        _, _, x_t, y_t = handle.make_train_test_data_dictionary(COUPLES, train_test_spl=False, future_step=future)
        model = run_lstmDS_model(handle, COUPLES, future_step=future, save_model=False)

        for i in range(future):
            yReal = handle.close_.iloc[-y_t.shape[0]-i: -i if i!=0 else None].values
            trade_details = backTradeTesting(model, x_t, np.argmax(y_t, axis=1), 100000, 100, yReal, in_tune_mode=True).final()
            res.append({'shift': shift, 'smooth': smooth, 'future': future, 'i': i, 'trade_details': trade_details})
            print(res[-1])

        with open(f'future_result_{start_index}.pkl', 'wb') as f:
            pickle.dump(res, f)
        
        with open(f'last_index_processed_future_search.txt', 'w') as f:
            f.write(str(idx + start_index))

def find_best_window(start_index=0):
    res = []
    # windows_list = list(range(1, 10)) + list(range(10, 20, 2)) + list(range(20,50,5)) + list(range(50,100,10))
    # windows_list = [1, 2, 3, 4, 5, 6]
    windows_list = [3]
    search_parameters = []

    for shift in range(2, 7):
        for smooth in range(3, 10):
            for window in windows_list:
                for futu in range(2, 10):
                    search_parameters.append([shift, smooth, window, futu]) 

    for step, [shift, smooth, window, future] in enumerate(search_parameters[start_index:]) :
        print(f'step {step+start_index} started:\n', '*'*30)
        now = time.time()
        with open('window_step.txt', 'w') as f:
            f.write(str(step+start_index))

        handle = DataHandler(DATAFRAME_PATH, 4, 'ordinal', 'quantile', window, 
                                smooth_shift_size=[shift], smooth_window_size=[smooth],
                                discretizer='discretizer1', purpose_colname=f'close_pct_change_{shift}_smoother_{smooth}', 
                                smooth_make_loss_profit_equal=True, prediction_mode=PREDICTION_MODE, 
                                add_zig_zag_indicators=True, pct_change_on_all=True, pct_and_not_pct_together=False,
                                load_from_saved_dataFrame=False, multi_level_res=True, number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                                add_zigzag_subwave=False, multi_level_intervals=.2, zig_zag_mode=(True, True, True))

        handle.make_windows()  
        _, _, x_t, y_t = handle.make_train_test_data_dictionary(COUPLES, train_test_spl=False, future_step=future)

        model = LSTMModel([1000, NUMBER_OF_OUTPUT_CLASSES], ACTIVATION_FUNCTION, OPTIMIZER, LR, 
                        None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, False, window, handle.feature_count, 
                        dif_shape_lstms=[[2,30,1]]*len(COUPLES), feature_count_diff=[2]*len(COUPLES), prediction_mode=PREDICTION_MODE,
                        save_model=False, in_tune_mode=True)
        run_model(model, handle.make_train_test_data_dictionary, COUPLES, future_step=future)

        for i in range(future):
            try:
                yReal = handle.close_.iloc[-y_t.shape[0]-i: -i if i!=0 else None].values
                trade_details = backTradeTesting(model, x_t, np.argmax(y_t, axis=1), 100000, 100, yReal, in_tune_mode=True).final()
                res.append({'shift': shift, 'smooth': smooth, 'future': future, 'i': i, 'window':window, 'trade_details': trade_details})
                print(res[-1])
            except BaseException as e:
                print('error')
                print(e)

        with open(f'window_future_result_m30_{start_index}.pkl', 'wb') as f:
            pickle.dump(res, f)
        print('*'*20 ,f'time consumpstion:{time.time() - now}', '*'*20, sep='\n')

def keras_tunner_run(shift, smooth, window, future_step, prediction_wait):
    handle = DataHandler('dataset_4h.csv', 4, 'ordinal', 'quantile', window, 
                            smooth_shift_size=[shift], smooth_window_size=[smooth],
                            discretizer='discretizer1', purpose_colname=f'close_pct_change_{shift}_smoother_{smooth}', 
                            smooth_make_loss_profit_equal=True, prediction_mode=PREDICTION_MODE, 
                            add_zig_zag_indicators=True, pct_change_on_all=True, pct_and_not_pct_together=False,
                            load_from_saved_dataFrame=False, multi_level_res=True, number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                            add_zigzag_subwave=False, multi_level_intervals=.2, zig_zag_mode=(True, True, True))

    handle.make_windows()  
    start_tune(handle, 1000, wait_window=prediction_wait, future_step=future_step)

def find_best_zigzag(start_index=0):
    search_parameter = [.01, .02, .03, .04, .002, .003, .004, .005, .006, .007, .008, .009]
    res = {}
    zipper = ZipProjectPythonFiles()
    for step, zig_zag_param in enumerate(search_parameter[start_index:]):
        print(f'step {step + start_index} started:\n', '*' * 30)
        with open('zigzag_step.txt', 'a') as f:
            f.write(str(step + start_index) + '\n')

        handle = DataHandler('../dataset_30min.csv', kbins=4, encode='ordinal', strategy='quantile',
                             window_size=WINDOW_SIZE,
                             smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                             discretizer='discretizer1', purpose_colname='', smooth_make_loss_profit_equal=True,
                             prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True,
                             pct_and_not_pct_together=False,
                             load_from_saved_dataFrame=False, multi_level_res=True,
                             number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                             add_zigzag_subwave=ADD_ZIGZAG_SUBWAVE, multi_level_intervals=MULTI_LEVEL_INTERVALS,
                             zig_zag_mode=ZIG_ZAG_MODE, zigzag_based_learning=ZIGZAG_BASED_LEARNING,
                             spread_base_learning=SPREAD_BASE_LEARNING, back_for_base_learning=BACK_FOR_BASE_LEARNING,
                             back_zigzag_base_learning=BACK_ZIGZAG_BASE_LEARNING, pivot_mode=PIVOTE_MODE,
                             include_pivots_data=INCLUDE_PIVOTS_DATA,
                             inclue_and_update_pivots_data=INCLUDE_AND_UPDATE_PIVOTS_DATA,
                             open_close_1d_base_learning=OPEN_CLOSE_1D_BASE_LEARNING,
                             open_close_2d_base_learning=OPEN_CLOSE_2D_BASE_LEARNING,
                             zig_zag_up_thresh=zig_zag_param, zig_zag_down_thresh=-zig_zag_param)

        handle.make_windows()
        this_trades = []
        try:
            runner = OneModelParallelMultipleRun(run_lstm_model, handle, 'make_train_test_data', 12, False,
                                                 True, zipper, 1, 2,
                                                 False, False, is_for_mlp=False)
            this_trades = runner.start()
        except BaseException as e:
            print(e)
            traceback.print_exc()

        # this_trades = sorted(this_trades, key=lambda x: max([item['netProfit'] for item in x]))
        res[f'zigzag_{zig_zag_param}'] = this_trades
        print(res)

        with open(f'zigzag_result_h4_precision_{start_index}.pkl', 'wb') as f:
            pickle.dump(res, f)

def feature_importance_eleminitive(zig_zag_param=ZIGZAG_UP_THRESHOLD, step=0):
    handle = DataHandler(DATAFRAME_PATH, 4, 'ordinal', 'quantile', WINDOW_SIZE, 
                            smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                            discretizer='discretizer1', purpose_colname='close_pct_change_3_smoother_3', smooth_make_loss_profit_equal=True, 
                            prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True, pct_and_not_pct_together=False,
                            load_from_saved_dataFrame=False, multi_level_res=True, number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                            add_zigzag_subwave=False, multi_level_intervals=.2, zig_zag_mode=(True, True, False),
                            zigzag_based_learning=True, spread_base_learning=False, back_for_base_learning=False,
                            back_zigzag_base_learning=False, only_pivot=True, include_pivots_data=False)
    
    base_df = copy.deepcopy(handle.dataframe)
    base_cols = copy.deepcopy(handle.cols)
    zigag_cols = list(filter(lambda x: 'zigzag' in x, base_cols))

    res = []
    for counter ,item in enumerate(zigag_cols[step:]):
        print(f'step {counter + 1} has started...')
        i = base_cols.index(item)
        handle.dataframe = np.delete(base_df, i, 1)
        handle.cols = base_cols[:i] + base_cols[i+1:]
        handle.feature_count = len(handle.cols) - 1
        handle.make_windows()
        model = LSTMModel([WINDOW_SIZE*handle.feature_count, 300, 200, NUMBER_OF_OUTPUT_CLASSES], ACTIVATION_FUNCTION, OPTIMIZER, LR, 
                None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, WINDOW_SIZE, handle.feature_count, prediction_mode=PREDICTION_MODE)
        analize_obj = run_model(model, handle.make_train_test_data, is_for_mlp=False, handle=handle, show_figs=False, save_results=True)
        res.append({'deleted_col_name': item, 'trade_details':analize_obj.trade_info})
        print(res[-1])
        with open(f'feature_importance_eleminitive_zigzagfiltered_{step}.pkl', 'wb') as f:
            pickle.dump(res, f)
    return res

def feature_importance_additive(zig_zag_param=ZIGZAG_UP_THRESHOLD, step=0):
    handle = DataHandler(DATAFRAME_PATH, 4, 'ordinal', 'quantile', WINDOW_SIZE, 
                            smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                            discretizer='discretizer1', purpose_colname='close_pct_change_3_smoother_3', smooth_make_loss_profit_equal=True, 
                            prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True, pct_and_not_pct_together=False,
                            load_from_saved_dataFrame=False, multi_level_res=True, number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                            add_zigzag_subwave=False, multi_level_intervals=.2, zig_zag_mode=(True, True, False),
                            zigzag_based_learning=True, spread_base_learning=False, back_for_base_learning=False,
                            back_zigzag_base_learning=False, only_pivot=True, include_pivots_data=False)
    
    base_df = copy.deepcopy(handle.dataframe)
    base_cols = copy.deepcopy(handle.cols)
    zigag_cols = list(filter(lambda x: 'zigzag' in x, base_cols))
    non_zigzag_cols = list(filter(lambda x: 'zigzag' not in x and x != 'purpose', base_cols))

    res = []
    for counter ,item in enumerate(zigag_cols[step:]):
        print(f'step {counter + 1} has started...')
        i = base_cols.index(item)
        handle.dataframe = np.concatenate([base_df[:, base_cols.index(col_n)].reshape(-1, 1) for col_n in non_zigzag_cols]
                                             + [base_df[:, base_cols.index(item)].reshape(-1, 1)]
                                             + [base_df[:, -1].reshape(-1, 1)], axis=1)
        handle.cols = non_zigzag_cols + [item] + base_cols[-1:]
        handle.feature_count = len(non_zigzag_cols) + 1
        handle.make_windows()
        model = LSTMModel([WINDOW_SIZE*handle.feature_count, 300, 200, NUMBER_OF_OUTPUT_CLASSES], ACTIVATION_FUNCTION, OPTIMIZER, LR, 
                None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, WINDOW_SIZE, handle.feature_count, prediction_mode=PREDICTION_MODE)
        analize_obj = run_model(model, handle.make_train_test_data, is_for_mlp=False, handle=handle, show_figs=False, save_results=True)
        res.append({'added_col_name': item, 'trade_details':analize_obj.trade_info})
        print(res[-1])
        with open(f'feature_importance_additive_zigzagfiltered_{step}.pkl', 'wb') as f:
            pickle.dump(res, f)
    return res

if __name__=='__main__':
    multiprocessing.set_start_method('spawn')

    # find_best_future(0)
    # find_best_window(0)
    # keras_tunner_run(2, 3, 3, 4, 3)
    find_best_zigzag(0)
    # feature_importance_eleminitive()
    # feature_importance_additive()