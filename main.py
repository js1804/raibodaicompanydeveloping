import numpy as np

try:
    from .utils import *
except BaseException as e:
    from utils import *
import multiprocessing
import tensorflow as tf
import functools
fix_gpu_memory()


def back_test(handle, path=r'/home/user/Desktop/raibodaicompanydeveloping/genetic/2023_03_12_17_14_20/2023_03_12_17_14_20_LSTM.h5'):
    model = run_lstm_model(handle, False)
    model.load_model(path)
    Analyze(model, handle, 'test', True, True, is_for_mlp=False, set_post_processor=True).draw()
    return


if __name__ == '__main__':
    multiprocessing.set_start_method('spawn')

    zipper = ZipProjectPythonFiles()
    data_frame_paths = ['dataset_15min.csv', ]

    handle = DataHandler(data_frame_paths[0], kbins=4, encode='ordinal', strategy='quantile',
                                window_size=WINDOW_SIZE,
                                smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                                discretizer='discretizer1', purpose_colname='close_pct_change_3_smoother_3',
                                smooth_make_loss_profit_equal=True,
                                prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True,
                                pct_and_not_pct_together=False,
                                load_from_saved_dataFrame=False, multi_level_res=True,
                                number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                                add_zigzag_subwave=ADD_ZIGZAG_SUBWAVE, multi_level_intervals=MULTI_LEVEL_INTERVALS,
                                zig_zag_mode=ZIG_ZAG_MODE, zigzag_based_learning=ZIGZAG_BASED_LEARNING,
                                spread_base_learning=SPREAD_BASE_LEARNING,
                                back_for_base_learning=BACK_FOR_BASE_LEARNING,
                                back_zigzag_base_learning=BACK_ZIGZAG_BASE_LEARNING, pivot_mode=PIVOTE_MODE,
                                include_pivots_data=INCLUDE_PIVOTS_DATA,
                                inclue_and_update_pivots_data=INCLUDE_AND_UPDATE_PIVOTS_DATA,
                                open_close_1d_base_learning=OPEN_CLOSE_1D_BASE_LEARNING,
                                open_close_2d_base_learning=OPEN_CLOSE_2D_BASE_LEARNING,
                                number_of_future=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE,
                                multi_future=MULTIPLE_FUTURE_BASE_LEARNING,
                                open_close_1d_base_learning_lin=OPEN_CLOSE_1D_BASE_LEARNING_LIN,
                                bucket_base_check_list=WAVE_LIST_TO_CHECK_BUCKET_BASE,
                                filter_col=False,
                                filter_cols_names=COMPLETE_ZIGZAG_FEATURE_NAMES,
                                post_processor=ReverseBasePostProcessor)
                                # post_processor=functools.partial(ThresholdPostProcessor, up_threshold=.7, down_threshold=.7))
                                # post_processor=SimplePostProcessor)
                                # post_processor=functools.partial(MultiFuturePostProcessor,
                                #                                  num_of_future=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE,
                                #                                  window_size=WINDOW_SIZE))
                                # post_processor=OpenCloseBasePostProcessing)
                                # post_processor=RemoveCodePostProcessor)
    couples = load_ds_mode_from_json()[0]
    handle.make_windows()
    # Analyze(StatisticTradeStrategy(handle.data_handlers[0], 'filter_on_all_w4'), handle.data_handlers[0], 'outputs', ).run()
    # back_test(handle)
    # plotter = PlotThreshold(handle, COUPLES, r'D:\E\projects\test_borse\genetic\2023_01_27_11_26_47\2023_01_27_11_26_47_LSTM.h5')
    # plotter.threshold_plot()
    # plotter.bucket_threshold_plot()
    # exit()
    # plot_curve(handle)
    # partial_plot_curve(handle)
    par = OneModelParallelMultipleRun(run_lstm_model, handle, 'make_train_test_data', REPETITION_OF_RUN, SHOW_FIGS,
                                      SAVE_MODEL_WEIGHTS, zipper, NUMBER_OF_RUN_PER_CARD, NUMBER_OF_GRAPHIC_CARD,
                                      True, True, is_for_mlp=False)
    # par = OneModelParallelMultipleRun(run_mlp_model, handle, 'make_train_test_data', REPETITION_OF_RUN, SHOW_FIGS,
    #                                   SAVE_MODEL_WEIGHTS, zipper, NUMBER_OF_RUN_PER_CARD, NUMBER_OF_GRAPHIC_CARD,
    #                                   True, True, is_for_mlp=True)
    # par = OneModelParallelMultipleRun(run_lstmDS_model, handle, 'make_train_test_data_dictionary', REPETITION_OF_RUN,
    #                                   SHOW_FIGS, SAVE_MODEL_WEIGHTS, zipper, NUMBER_OF_RUN_PER_CARD,
    #                                   NUMBER_OF_GRAPHIC_CARD, True, True, is_for_mlp=False, couples=couples)
    # par = OneModelParallelMultipleRun(run_mlpDS_model, handle, 'make_train_test_data_dictionary', REPETITION_OF_RUN,
    #                                   SHOW_FIGS, SAVE_MODEL_WEIGHTS, zipper, NUMBER_OF_RUN_PER_CARD,
    #                                   NUMBER_OF_GRAPHIC_CARD, True, True, is_for_mlp=True, couples=couples)
    # print(par.start())
    with tf.device('gpu:0'):
        # run_mlp_model(handle, future_step=FUTURE_STEP, zipper=zipper)
        run_lstm_model(handle, zipper=zipper, future_step=FUTURE_STEP)
        # run_mlpDS_model(handle, run=True, future_step=FUTURE_STEP, zipper=zipper save_model=False)
        # run_lstmDS_model(handle, future_step=FUTURE_STEP, zipper=zipper)
    # run_CNN_model(handle)
    # model = run_lstmDS_model(handle, COUPLES, False)
    # load_model_from_saved_instance(model, handle.make_train_test_data_dictionary,
    # COUPLES, path='2022_07_23_09_40_23_LSTM_DS')
    # run_lstm_ds_tuner(handle, COUPLES)
    # start_tune(handle, max_trial=1000)
    # load_tune(handle)
