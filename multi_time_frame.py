from utils import *

import numpy as np


def make_handler(path, filter_col=FILTER_COLUMNS, wanted_col=WANTED_COLS, window=WINDOW_SIZE,
                 processed_df_path=None, pc='close', zigbase=True, zig_val=ZIGZAG_UP_THRESHOLD):
    handler = DataHandler(path, kbins=4, encode='ordinal', strategy='quantile',
                          window_size=window,
                          smooth_shift_size=SMOOTH_SHIFT_SIZE, smooth_window_size=SMOOTH_WINDOW_SIZE,
                          discretizer='discretizer1', purpose_colname=pc,
                          smooth_make_loss_profit_equal=True, zig_zag_up_thresh=zig_val, zig_zag_down_thresh=-zig_val,
                          prediction_mode=PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True,
                          pct_and_not_pct_together=False,
                          load_from_saved_dataFrame=False if processed_df_path is None else True, multi_level_res=True,
                          number_of_class=NUMBER_OF_OUTPUT_CLASSES,
                          add_zigzag_subwave=ADD_ZIGZAG_SUBWAVE, multi_level_intervals=MULTI_LEVEL_INTERVALS,
                          zig_zag_mode=ZIG_ZAG_MODE, zigzag_based_learning=zigbase,
                          spread_base_learning=SPREAD_BASE_LEARNING,
                          back_for_base_learning=BACK_FOR_BASE_LEARNING,
                          back_zigzag_base_learning=BACK_ZIGZAG_BASE_LEARNING, pivot_mode=PIVOTE_MODE,
                          include_pivots_data=INCLUDE_PIVOTS_DATA,
                          inclue_and_update_pivots_data=INCLUDE_AND_UPDATE_PIVOTS_DATA,
                          open_close_1d_base_learning=OPEN_CLOSE_1D_BASE_LEARNING,
                          open_close_2d_base_learning=OPEN_CLOSE_2D_BASE_LEARNING,
                          number_of_future=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE,
                          multi_future=MULTIPLE_FUTURE_BASE_LEARNING,
                          open_close_1d_base_learning_lin=OPEN_CLOSE_1D_BASE_LEARNING_LIN,
                          filter_col=filter_col,
                          filter_cols_names=wanted_col,
                          saved_processed_df_path=processed_df_path,
                          post_processor=SimplePostProcessor)
    handler.make_windows()
    return handler


if __name__ == '__main__':
    five_min_processed_df = 'precessed_df_5min_005.csv'
    # high_model_path = '/home/user/mra/projcet_py_files/multitime/2023_01_31_17_48_28/2023_01_31_17_48_28_LSTM.h5'
    # high_model_path = '/home/user/mra/projcet_py_files/h4_tests/2023_02_02_09_41_24_mlp_001/2023_02_02_09_41_24_MLP_19.h5'
    high_model_path = '/home/user/mra/projcet_py_files/h4_tests/2023_02_02_09_28_39_mlp_009/2023_02_02_09_28_39_MLP_19.h5'
    # low_model_path = '/home/user/mra/projcet_py_files/multitime/2023_01_31_20_24_23/2023_01_31_20_24_23_MLP.h5'
    # low_model_path = '/home/user/mra/projcet_py_files/h4_tests/2023_02_01_21_32_38/2023_02_01_21_32_38_MLP.h5'
    low_model_path = '/home/user/mra/projcet_py_files/h4_tests/2023_02_02_20_48_27/2023_02_02_20_48_27_MLP.h5'
    bio = BioTimeFramePredict(high_model_path, low_model_path,
                              make_handler('../dataset_4h_shift_0.csv', window=2, zig_val=.009),
                              make_handler('../thirtin_min.csv', False, BASIC_ZIGZAG_FEATURE_NAMES,
                                           zig_val=.005), '4h', '5min')
    preds = bio.predict()
    res = backTradeTesting(bio.low_time_model, bio.low_time_x, np.array([0] * len(bio.low_time_x['deep_input'])),
                           100000, 100,
                           bio.low_time_close, save_path='newoutput/', given_preds=preds).final()
    print(res)
