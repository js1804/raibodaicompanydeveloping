from utils import *
import pickle, json, glob
import matplotlib.pyplot as plt

class ShowLastResults:
    def __init__(self, base_folder='test', run_time=None):
        self.folder = sorted(glob.glob(base_folder + '/*'))[-1] if run_time==None else f'{base_folder}/{run_time}'
        self.files = ['all_figs_0.pkl', 'all_figs_1.pkl']# 'power.pkl', 'trades_zigzag_detail.pkl', 'rectangle_power.pkl']
        self.files = list(map(lambda x: self.folder + '/' + x, self.files))
        print(self.folder)
    
    def show_figs(self):
        ax = []
        for item in self.files:
            with open(item, 'rb') as f:
                ax.append(pickle.load(f))

        plt.show()

if __name__=='__main__':
    # ShowLastResults(run_time='2022_11_17_20_06_54').show_figs()
    ShowLastResults().show_figs()