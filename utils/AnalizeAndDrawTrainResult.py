from .BackTraderTesting import backTradeTesting
from .DataHandler import DataHandler
from . import GlobalVariable as gv
from .ZipProjectPython import ZipProjectPythonFiles
from .SectionBasePrepareBackTest import SectionBasePrepareBackTest

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import pickle
import time
import os
import shutil


class Analyze:
    def __init__(self, model, handler: DataHandler, folder_to_save=gv.FOLDER_TO_SAVE_RESULTS,
                show_figs=True, save=True, zipper: ZipProjectPythonFiles=None, set_post_processor=False, **kwargs):
        self.model = model
        self.zipper = zipper
        self.show_figs = show_figs
        self.save = save
        self.trade_info = []
        self.prepare_folder(folder_to_save)
        self.handler = handler
        self.x, self.y, self.x_t, self.y_t, self.test_indices = handler.make_train_test_data(**kwargs)
        self.zigzag_final_pivots = handler.final_pivots
        self.zigzag_condidate_pivots = handler.condidate_pivots
        self.prepare_condidate_zigzag_pivots()
        self.future_step = kwargs['future_step'] if 'future_step' in kwargs else gv.FUTURE_STEP
        self.model.post_processor = handler.post_processor(model=self.model) if set_post_processor else self.model.post_processor
        self.model_accuracy_plot_list = []

    def prepare_condidate_zigzag_pivots(self):
        st = self.zigzag_condidate_pivots[0]
        self.condidate_pivots = [st]
        for i in range(1, len(self.zigzag_condidate_pivots)):
            if self.zigzag_condidate_pivots[i] != 0 and self.zigzag_condidate_pivots[i] != st:
                self.condidate_pivots.append(-st)
                st = -st
            else:
                self.condidate_pivots.append(0)
        self.condidate_pivots = np.array(self.condidate_pivots)

    def save_py_zipfile(self):
        if self.zipper is not None:
            shutil.copy2(self.zipper.name_to_save, self.folder_to_save + 'projcet_py_files.zip')

    def prepare_folder(self, folder_to_save):
        try:
            os.mkdir(folder_to_save)
        except BaseException as e:
            pass
        time_format = '%Y_%m_%d_%H_%M_%S'
        self.save_time = time.strftime(time_format)

        self.folder_to_save = folder_to_save + '/' + self.save_time
        while 1:
            try:
                os.mkdir(self.folder_to_save)
                break
            except BaseException as e:
                print(e)
                self.folder_to_save += '*'
        self.folder_to_save += '/'
        self.model.model.save(self.folder_to_save + f'{self.save_time}_{self.model.model_type}.h5')
        tf.keras.utils.plot_model(
            self.model.model,
            to_file=self.folder_to_save + 'model_structure.png',
            show_shapes=True,
            show_dtype=False,
            show_layer_names=True,
            rankdir='LR',
            expand_nested=False,
            dpi=96,
            layer_range=None,
            show_layer_activations=True
        )
        self.save_py_zipfile()

    def draw(self,):
        for i in range(self.future_step + 1):
            fis, axs = plt.subplots(2, 2, sharex=True, sharey=False)
            spbt = SectionBasePrepareBackTest(self.test_indices, self.handler.close_.values, self.model.post_processor)
            spbt.final(self.x_t, True, i, True, '', None, 1)
            self.trade_info.append(spbt.unify_results())
            self.yReal = spbt.y_real
            print(self.trade_info[-1])

            trade_detail = spbt.unify_actions()
            # spbt.plot(len(self.yReal), axs[1, 0], '', False)
            spbt.save(self.folder_to_save, len(self.yReal), axs[1, 0], show_save_fig=True)
            preds = self.model.predict(self.x_t)

            # self.trade_zigzag_chart(axs[0, 1], trade_detail, i, save=False, show=False)
            self.trade_zigzag_chart(axs[0, 1], trade_detail, i, save=False, show=False)
            # self.power_figs_bar_mode(preds, axs[1, 0], i, save=False, show=False)
            self.power_figs_continues_mode(preds, axs[0, 0], i, save=False, show=False)

            self.save_fig_obj(axs, f'all_figs_{i}', show=self.show_figs)
        self.model_accuracies(show=False)
        self.save_fig_obj(self.model_accuracy_plot_list, 'model_accs', show=self.show_figs)

    def trade_zigzag_chart(self, ax, trade_detail, number=0, save=False, show=False):
        ax.plot(self.yReal)
        close_price = self.handler.close_.values
        base_candle_count = 0
        for item in self.test_indices:
            final_piv = self.zigzag_final_pivots[item[0] + number: item[0] + number + item[2]]
            candid_piv = self.condidate_pivots[item[0] + number: item[0] + number + item[2]]
            ax.scatter(np.where(final_piv!=0)[0] + base_candle_count, close_price[np.where(final_piv!=0)[0] + item[0] + number],
                       s=10, color='red')
            ax.plot(np.where(final_piv!=0)[0] + base_candle_count, close_price[np.where(final_piv!=0)[0] + item[0] + number],
                       color='black')
            ax.scatter(np.where(candid_piv==1)[0] + base_candle_count, close_price[np.where(candid_piv==1)[0] + item[0] + number], 
                             s=25, color='red', marker='^')
            ax.scatter(np.where(candid_piv==-1)[0] + base_candle_count, close_price[np.where(candid_piv==-1)[0] + item[0] + number], 
                             s=25, color='red', marker='v')
            base_candle_count += item[2]

        sell_linex, sell_liney, buy_linex, buy_liney = [], [],[], []
        for i, (step, state) in enumerate(list(trade_detail.items())[:-1]):
            if state['type'] == 'sell':
                sell_linex.append([state['openStep'], state['closeState']])
                sell_liney.append([state['openPrice'], state['closePrice']])
                ax.plot(sell_linex[-1], sell_liney[-1], color='red')
            else:
                buy_linex.append([state['openStep'], state['closeState']])
                buy_liney.append([state['openPrice'], state['closePrice']])
                ax.plot(buy_linex[-1], buy_liney[-1], color='green')
        self.save_fig_obj(ax, f'trades_zigzag_detail_{number}', show=show) if save else None

    def power_figs_continues_mode(self, preds, ax, number=0, save=False, show=False):
        for i in range(preds.shape[1]):
            ax.plot(preds[:, i])
        self.save_fig_obj(ax, f'power_{number}', show=show) if save else None

    def power_figs_bar_mode(self, preds, ax, number=0, save=False, show=False):
        up, down, color = [], [], []
        for item in preds:
            if item[1] > item[0]:
                up.append(item[1])
                down.append(item[0])
                color.append('r')
            else:
                up.append(item[0])
                down.append(item[1])
                color.append('b')
        for i in range(len(color)):
            if color[i] == 'r':
                ax.bar(i, height=up[i]- down[i], width=.3, bottom=down[i], color='red')
            else:
                ax.bar(i, height=up[i]- down[i], width=.3, bottom=down[i], color='green')
        self.save_fig_obj(ax, f'rectangle_power_{number}', show=show) if save else None

    def model_accuracies(self, show=False):
        self.model_accuracy_plot_list.append(self.model.plot_acc_loss(show=show))

    def save_fig_obj(self, ax, name, show=False):
        if self.save:
            with open(f'{self.folder_to_save}/{name}.pkl', 'wb') as f:
                pickle.dump(ax, f)
        plt.show() if show else None

    def save_train_meta_data(self):
        res = {}
        res['zigzag_thresh'] = self.handler.zig_zag_up_thresh
        res['smooth_shift_size'] = self.handler.smooth_shift_size
        res['smooth_window_size'] = self.handler.smooth_window_size
        res['columns'] = self.handler.cols
        res['purpose'] = self.handler.purpose_colname
        res['future_step'] = self.handler.future_step
        res['zigzga_moed'] = self.handler.zig_zag_mode
        res['model'] = self.model.model_type
        res['trade_details'] = self.trade_info
        res['global_variable'] = {item: getattr(gv, item) for item in dir(gv) if not item.startswith('__')}
        res['best_epoch'] = self.model.best_epoch
        res['early_stop_trades_info'] = self.model.early_stop_trades_info
        res['y_pred_list'] = self.model.y_pred_list
        res['y_true'] = self.model.y_true
        res['test_precisions'] = self.model.precisions
        res['test_recalls'] = self.model.recalls
        res['test_counts'] = self.model.counts
        res['multi_future_data'] = self.model.multi_future_data
        with open(f'{self.folder_to_save}/basic_data.pkl', 'wb') as f:
            pickle.dump(res, f)

    def run(self):
        self.draw()
        self.save_train_meta_data()
        plt.close()


if __name__ == '__main__':
    model = tf.keras.models.load_model(r'D:\E\projects\test_borse\test\2022_11_15_14_45_35\2022_11_15_14_45_35_MLP.h5')
    handler = DataHandler(gv.DATAFRAME_PATH, 4, 'ordinal', 'quantile', gv.WINDOW_SIZE, 
                            smooth_shift_size=gv.SMOOTH_SHIFT_SIZE, smooth_window_size=gv.SMOOTH_WINDOW_SIZE,
                            discretizer='discretizer1', purpose_colname='close_pct_change_3_smoother_3', smooth_make_loss_profit_equal=True, 
                            prediction_mode=gv.PREDICTION_MODE, add_zig_zag_indicators=True, pct_change_on_all=True, pct_and_not_pct_together=False,
                            load_from_saved_dataFrame=False, multi_level_res=True, number_of_class=gv.NUMBER_OF_OUTPUT_CLASSES,
                            add_zigzag_subwave=False, multi_level_intervals=.2, zig_zag_mode=(True, False, False), zigzag_based_learning=True)
    handler.make_windows()  
    x, y, x_t, y_t = handler.make_train_test_data(True)
    analizer = Analyze(model, handler, is_for_mlp=True)
    # analizer.draw()
    analizer.save_train_meta_data()