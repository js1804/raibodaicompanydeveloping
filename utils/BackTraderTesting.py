from .GlobalVariable import BUY_POSITION, SELL_POSITION, DO_NOTHING_POSITION, CLOSE_POSITION, \
    SECOND_LAST_WAVE_REMOVE_CODE, LAST_WAVE_REMOVE_CODE

import numpy as np
from tqdm import tqdm
import pandas as pd
import os
import time
import json
import matplotlib.pyplot as plt

BUY = 'buy'
SELL = 'sell'


class backTradeTesting:
    MINUS_SPREAD = .3

    def __init__(self, model, xTest, yTest, margin, leverage, yReal, tradeOnYourPrediction=True,
                 in_tune_mode=False, save_path=None, ax=None, lot=1, exponential_trading=False,
                 exponential_division_factor=1, base_trade_counting=0, base_candle_num=0):
        self.model = model
        self.xTest = xTest
        self.yTest = yTest
        self.margin = margin
        self.leverage = leverage
        self.yReal = yReal
        self.tradeOnYourPrediction = tradeOnYourPrediction
        self.dirPath = '.'
        self.in_tune_mode = in_tune_mode
        self.save_path = save_path
        self.ax = ax
        self.lot = lot
        self.exponential_trading = exponential_trading
        self.exponential_division_factor = exponential_division_factor
        self.base_trade_counting = base_trade_counting
        self.base_candle_num = base_candle_num

    def openingAgen(self, predict, step, margin):
        trade = dict()
        decision = BUY if predict == BUY_POSITION else SELL if predict == SELL_POSITION else None
        if decision == None:
            return

        trade["position"] = "open"
        trade["type"] = decision

        status = dict()
        status["type"] = trade["type"]
        status["openPrice"] = self.yReal[step]
        status["closePrice"] = 0
        status["profit"] = 0
        status["openStep"] = int(step + self.base_candle_num)
        status["closeState"] = 0
        status["margin"] = margin

        return status

    def closingAgent(self, difference, i, last_trade):

        trade = {'close': self.closeCalculation(difference[0], difference[1], trade_kind=last_trade['type'])
                            if difference[1] != -1 and difference[
            0] != -1 else 0}

        if i == len(self.yTest) - 1:
            trade["close"] = 1

        if trade["close"] >= 1:

            last_trade["closePrice"] = self.yReal[(i)]
            last_trade["closeState"] = int(i + self.base_candle_num)
            if last_trade["type"] == "buy":
                last_trade["profit"] = self.leverage * (
                        last_trade["closePrice"] - last_trade["openPrice"] - self.MINUS_SPREAD) * self.lot
            if last_trade["type"] == "sell":
                last_trade["profit"] = self.leverage * (
                        last_trade["openPrice"] - last_trade["closePrice"] - self.MINUS_SPREAD) * self.lot

            last_trade["margin"] = last_trade["margin"] + last_trade["profit"] * (
                last_trade["margin"] / 10000 / self.exponential_division_factor if self.exponential_trading else 1)

        return trade

    def closeCalculation(self, now, pred, trade_kind):
        if trade_kind == 'buy':
            if pred == SELL_POSITION:
                return 1
            elif pred == BUY_POSITION:
                return 0
        elif trade_kind == 'sell':
            if pred == BUY_POSITION:
                return 1
            elif pred == SELL_POSITION:
                return 0
        if pred == CLOSE_POSITION:# or pred == DO_NOTHING_POSITION:
            return 2
        return 0
        raise BaseException('do nothing')

        # for statistic base test
        # TODO: integrage statistics results in output of model
        if pred == SECOND_LAST_WAVE_REMOVE_CODE:
            return 0
        elif pred == LAST_WAVE_REMOVE_CODE:
            return 2
        return 0

    def trades(self):
        trades = dict()
        tagOpen = 0
        in_trade = False
        numberOfTrade = self.base_trade_counting
        previousPredict = list()
        margin = self.margin
        margins = list()
        previousPredict.append(self.yTest[0])
        print('=' * 25, 'start trading', '=' * 25)
        print(f"yTest shape = {self.yTest.shape}")

        # predicts = np.argmax(self.model.predict(self.xTest), axis=1)
        predicts = self.model.predict(self.xTest)
        for i in tqdm(range(0, len(self.yTest))):
            predict = predicts[i]

            previousPredict.append(predict)

            if tagOpen == 0 and i < len(self.yTest) - 1:
                status = self.openingAgen(predict, i, margin)
                if status != None:
                    trades["step" + str(numberOfTrade)] = status
                    tagOpen = 1
                margins.append(int(margin))
                in_trade = True

            elif in_trade and tagOpen == 1:
                closePosition = self.closingAgent(previousPredict[-2:], i, trades["step" + str(numberOfTrade)])

                if closePosition["close"] >= 1:
                    margin = trades["step" + str(numberOfTrade)]["margin"]
                    in_trade = False

                    if trades["step" + str(numberOfTrade)]["margin"] > 0 and i < len(self.yTest) - 1:
                        numberOfTrade += 1
                        tagOpen = 0
                        status = self.openingAgen(predict, i, margin)
                        if status != None:
                            trades["step" + str(numberOfTrade)] = status
                            tagOpen = 1
                            in_trade = True

                margins.append(margin)
        dirPath = self.savefiles(trades, margins)


        return (trades, margins, None, dirPath)

    def savefiles(self, statement, margins):
        timeFormat = '%Y_%m_%d_%H_%M_%S'
        dirName = 'backtest_res_' + time.strftime(timeFormat)
        dirPath = f'{self.save_path}/{dirName}'
        if self.in_tune_mode:
            return dirPath  # print(dirPath)
        while 1:
            try:
                os.mkdir(dirPath)
                break
            except BaseException as e:
                dirPath = dirPath + '*'

        with open(f'{dirPath}/trades.json', 'w') as jFile:
            json.dump(statement, jFile)
        self.plotMargin(margins, f'{dirPath}/margins.png', save=True)

        return dirPath

    def print(self, dictionary):
        for level1_key, level1_values in dictionary.items():
            print('*' * 20, level1_key, '*' * 20)
            for key, values in level1_values.items():
                print(f'\t {key} = {values}')

    def plotMargin(self, margins, path=None, save=False):
        if self.in_tune_mode:
            return
        plt.plot(range(len(margins)), margins) if self.ax == None else self.ax.plot(range(len(margins)), margins)
        if save:
            plt.savefig(path) if self.ax == None else self.ax.figure.savefig(path)
            # print('Saved!!!!')
        plt.show() if self.ax == None else None

    def statementResults(self, trades_):
        numberOfBuy = 0
        numberOfSell = 0
        numberOfPositveSell = 0
        numberOfPositiveBuy = 0
        profitInBuyTrades = 0
        profitInSellTrades = 0
        netProfit = 0
        totallProfit = 0
        totallLoss = 0
        numberOfLossBuy = 0
        lossInBuyTrades = 0
        numberOfLossSell = 0
        lossInSellTrades = 0
        netBuy = 0
        netSell = 0
        listOfMargin = list()

        average_win = 0
        average_loss = 0

        max_draw_down = 0
        pre_max_margin = 0
        pre_min_margin = 10e20
        list_of_draw_downs = [0]
        list_of_consecutive_loss = [0]
        list_of_candle_base_consecutive_loss = [0]
        current_consecutive_loss = 0
        current_candle_base_consecutive_loss = 0

        for i in range(0, len(trades_)):
            current_step_name = 'step' + str(i + self.base_trade_counting)
            ####################################### positive profit section ####################
            listOfMargin.append(trades_["step" + str(i + self.base_trade_counting)]["margin"])
            if listOfMargin[-1] > pre_max_margin:
                list_of_draw_downs.append(max_draw_down * 100)
                pre_max_margin = listOfMargin[-1]
                pre_min_margin = listOfMargin[-1]
                max_draw_down = 0
            elif listOfMargin[-1] < pre_min_margin:
                pre_min_margin = listOfMargin[-1]
                max_draw_down = 1 - (pre_min_margin / pre_max_margin)

            if trades_[current_step_name]["profit"] > 0:
                totallProfit += trades_[current_step_name]["profit"]
                list_of_consecutive_loss.append(current_consecutive_loss)
                list_of_candle_base_consecutive_loss.append(current_candle_base_consecutive_loss)
                current_consecutive_loss = 0
                current_candle_base_consecutive_loss = 0

            ####################################### loss section #################################
            if trades_[current_step_name]["profit"] < 0:
                totallLoss += trades_[current_step_name]["profit"]
                current_consecutive_loss += 1
                current_candle_base_consecutive_loss += trades_[current_step_name]["closeState"] - \
                                                        trades_[current_step_name]["openStep"]

            ################################ buy section ############################################
            if trades_[current_step_name]["type"] == "buy":
                numberOfBuy += 1
                if trades_[current_step_name]["profit"] > 0:
                    numberOfPositiveBuy += 1
                    profitInBuyTrades += trades_[current_step_name]["profit"]

                if trades_[current_step_name]["profit"] < 0:
                    numberOfLossBuy += 1
                    lossInBuyTrades += trades_[current_step_name]["profit"]

            ############################################### sell section #####################################
            if trades_[current_step_name]["type"] == "sell":
                numberOfSell += 1

                if trades_[current_step_name]["profit"] > 0:
                    numberOfPositveSell += 1
                    profitInSellTrades += trades_[current_step_name]["profit"]

                if trades_[current_step_name]["profit"] < 0:
                    numberOfLossSell += 1
                    lossInSellTrades += trades_[current_step_name]["profit"]

            ################################## total section ###################################################3
            netProfit += trades_[current_step_name]["profit"]
        netBuy = profitInBuyTrades + lossInBuyTrades
        netSell = profitInSellTrades + lossInSellTrades

        winRate = ((numberOfPositiveBuy + numberOfPositveSell) / (len(trades_) + .00001)) * 100
        lossRate = ((numberOfLossSell + numberOfLossBuy) / (len(trades_) + .00001)) * 100

        average_win = (profitInBuyTrades + profitInSellTrades) / (numberOfPositiveBuy + numberOfPositveSell + .001)
        average_loss = (lossInBuyTrades + lossInSellTrades) / (numberOfLossBuy + numberOfLossSell + .001)

        finalResult = dict()

        finalResult["numberOfTrades"] = len(trades_)
        finalResult["numberOfBuy"] = numberOfBuy
        finalResult["numberOfSell"] = numberOfSell
        finalResult["numberOfPositveSell"] = numberOfPositveSell
        finalResult["numberOfPositiveBuy"] = numberOfPositiveBuy
        finalResult["profitInBuyTrades"] = profitInBuyTrades
        finalResult["profitInSellTrades"] = profitInSellTrades
        finalResult["netProfit"] = netProfit
        finalResult["totallProfit"] = totallProfit
        finalResult["totallLoss"] = totallLoss
        finalResult["numberOfLossBuy"] = numberOfLossBuy
        finalResult["lossInBuyTrades"] = lossInBuyTrades
        finalResult["numberOfLossSell"] = numberOfLossSell
        finalResult["lossInSellTrades"] = lossInSellTrades
        finalResult["netBuy"] = netBuy
        finalResult["netSell"] = netSell
        finalResult["winRate"] = winRate
        finalResult["lossRate"] = lossRate

        finalResult['pay_of_ratios'] = abs(average_win / (average_loss + .001))
        finalResult['average_win'] = average_win
        finalResult['average_loss'] = average_loss
        finalResult['profit_factor'] = abs(
            (profitInBuyTrades + profitInSellTrades) / (lossInBuyTrades + lossInSellTrades + .001))
        finalResult['expectancy'] = netProfit / (len(trades_) + .00001)
        finalResult['max_draw_down'] = max(list_of_draw_downs)
        finalResult['list_of_draw_downs'] = sorted(list_of_draw_downs[1:])[-20:][::-1]
        finalResult['max_consecutive_loss'] = max(list_of_consecutive_loss + [1])
        finalResult['list_of_consecutive_loss'] = sorted(list_of_consecutive_loss)[-5:][::-1]
        finalResult['list_of_candle_base_consecutive_loss'] = sorted(list_of_candle_base_consecutive_loss)[-5:][::-1]

        return (finalResult)

    def final(self):
        self.trades_detail, margin, r, _ = self.trades()
        finalResult = self.statementResults(self.trades_detail)
        return (finalResult)
