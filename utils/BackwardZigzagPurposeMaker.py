from .IterativeZigzag import IterativeZigzag
from .GlobalVariable import *
from .my_zigzag import peak_valley_pivots

import numpy as np
import matplotlib.pyplot as plt

class BackwardZigzagPurposeMaker:
    def __init__(self, plot_result=False) -> None:
        self.plot_result = plot_result

    def create_backward_flag(self, close):
        candidate_pivots = list(IterativeZigzag(close, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD).transform())
        final_pivots = peak_valley_pivots(close, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD)
        final_pivots_position = np.where(final_pivots!=0)[0]
        res = np.zeros(len(close))

        first_reversed_pos = []
        place_holder_value = final_pivots[0]

        last_final_pivote = final_pivots[0]
        last_candidate_pivot = candidate_pivots[0]
        for i in range(1, len(close)):
            if candidate_pivots[i] == - last_final_pivote:
                res[i] = 1
            if candidate_pivots[i] == 0 and last_candidate_pivot == last_final_pivote:
                res[i] =1
            if candidate_pivots[i] == 0  and last_candidate_pivot == -last_final_pivote and final_pivots[i] == last_candidate_pivot:
                res[i] = 0
            last_final_pivote = final_pivots[i] if final_pivots[i]!=0 else last_final_pivote
            last_candidate_pivot = candidate_pivots[i] if candidate_pivots[i]!=0 else last_candidate_pivot

        if self.plot_result:
            plot_x = [i for i in range(len(close)) if res[i]!=0]
            plot_y = [close[i] for i in plot_x]
            l = np.array(candidate_pivots)
            plt.plot(close)
            plt.scatter(plot_x, plot_y, s=35, color='red', marker='^')
            plt.scatter(np.where(l!=0)[0], close[np.where(l!=0)[0]], s=45, color='blue', marker='*')
            plt.scatter(final_pivots_position, close[final_pivots_position], s=45, color='black', marker='*')
            plt.plot(final_pivots_position, close[final_pivots_position], color='black')
            plt.show()

        # for i in range(1, len(candidate_pivots)):
        #     if candidate_pivots[i] == -place_holder_value:
        #         place_holder_value = -place_holder_value
        #         first_reversed_pos.append(i)

        # for i in range(len(first_reversed_pos)):
        #     res[final_pivots_position[i]: first_reversed_pos[i]] = 1
        return np.array(res)

    def transform(self, x, close, cols):
        res = self.create_backward_flag(close)
        res = res.reshape(-1, 1)

        if 'purpose' not in cols:
            x = np.append(x, res, axis=1) 
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = res

        return x