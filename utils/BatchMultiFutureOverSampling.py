import copy

import numpy as np


class BatchMultiFutureOverSampling:
    def __init__(self):
        pass

    @staticmethod
    def transform(x, y, *data, num_of_future=1):
        base_item = copy.deepcopy(x)
        base_y = copy.deepcopy(y)
        for i in range(1, num_of_future):
            x = np.concatenate([x, base_item])
            y = np.concatenate([y, np.concatenate([base_y[i:], np.array([base_y[0]]*i)])])
        res = []

        for item in data:
            base_item = copy.deepcopy(item)
            for i in range(num_of_future - 1):
                item = np.concatenate([item, base_item])
            res.append(item)
        return x, y, res
