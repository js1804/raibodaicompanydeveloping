import copy
import numpy as np


class BatchMultiFuturePurposeMaker:
    def __init__(self, num_of_future):
        self.num_of_future = num_of_future

    def transform(self, x, columns):
        label = x[:, columns.index('purpose')]
        shape = x.shape
        labels = []

        for i in range(1, self.num_of_future):
            labels.append(np.concatenate([label[i:], np.array([0]*i)]))

        all_x = copy.deepcopy(x)
        for item in labels:
            x_copy = copy.deepcopy(x)
            x_copy[:, columns.index('purpose')] = item
            all_x = np.concatenate([all_x, x_copy], axis=1)
        x = all_x
        x = x.reshape(shape[0]*self.num_of_future, -1)
        return x
