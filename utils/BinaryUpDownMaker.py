import copy
import numpy as np


class BinaryUpDownMaker():
    def __init__(self) -> None:
        pass
    
    def transform(self, x, purpose_col_name='close', cols=None):
        y_col = copy.deepcopy(x[:, cols.index(purpose_col_name)])
        y_col[y_col > 0 ] = 1
        y_col[y_col <= 0 ] = 0
        if purpose_col_name != 'purpose':
            x = np.append(x, y_col.reshape(-1,1), axis=1)
            cols.append('purpose')
        else:
            x[:, cols.index(purpose_col_name)] = y_col

        return x

    def __call__(self, x, purpose_col_name='close', cols_name=None):
        return self.transform(x, purpose_col_name, cols_name)
