from .DataHandler import DataHandler
from .GlobalVariable import BUY_POSITION, SELL_POSITION, DO_NOTHING_POSITION, OPEN_POSITION, CLOSE_POSITION

import tensorflow as tf
import numpy as np
import pandas as pd
import datetime


class BioTimeFramePredict:
    def __init__(self, high_time_model_path, low_time_model_path, high_time_handler: DataHandler,
                 low_time_handler: DataHandler, high_time, low_time):
        self.high_time = high_time
        self.low_time = low_time
        self.high_time_handler = high_time_handler
        self.low_time_handler = low_time_handler
        self.high_time_model_path = high_time_model_path
        self.low_time_model_path = low_time_model_path
        self.high_time_model = tf.keras.models.load_model(high_time_model_path)
        self.low_time_model = tf.keras.models.load_model(low_time_model_path)

        _, _, self.high_time_x, self.high_time_y = self.high_time_handler.make_train_test_data(is_for_mlp=True)
        _, _, self.low_time_x, self.low_time_y = self.low_time_handler.make_train_test_data(is_for_mlp=True)
        self.high_time_y, self.low_time_y = np.argmax(self.high_time_y, axis=1), np.argmax(self.low_time_y, axis=1)

        self.high_time_predicts = np.argmax(self.high_time_model.predict(self.high_time_x), axis=1)
        self.low_time_predicts = np.argmax(self.low_time_model.predict(self.low_time_x), axis=1)
        # self.low_time_predicts = self.low_time_model.predict(self.low_time_x)
        # low_pred = np.ones([len(self.low_time_predicts)])
        # low_pred[:] = DO_NOTHING_POSITION
        # low_pred[np.logical_and(np.max(self.low_time_predicts, axis=1) > .56, np.argmax(self.low_time_predicts, axis=1)==1) ] = 1
        # low_pred[np.logical_and(np.max(self.low_time_predicts, axis=1) > .56, np.argmax(self.low_time_predicts, axis=1)==0) ] = 0
        # self.low_time_predicts = low_pred

        self.high_time_close = self.high_time_handler.close_[-len(self.high_time_x['deep_input']) - 1:]
        self.low_time_close = self.low_time_handler.close_[-len(self.low_time_x['deep_input']) - 1:]
        self.high_time_index = self.high_time_handler.data_index[-len(self.high_time_x['deep_input']) - 1:]
        self.low_time_index = self.low_time_handler.data_index[-len(self.low_time_x['deep_input']) - 1:]
        self.setup_length_compatible_low()
        self.setup_length_compatible_high()

        self.high_time_idx = 0
        self.low_time_idx = 0
        self.high_time_last_predict = self.high_time_predicts[self.high_time_idx]
        self.low_time_last_predict = self.low_time_predicts[self.low_time_idx]

    def setup_length_compatible_low(self):
        low_st = 0
        low_end = len(self.low_time_index) - 1
        while self.high_time_index[1] > self.low_time_index[low_st]:
            low_st += 1
        while self.high_time_index[-1] + (self.high_time_index[1] - self.high_time_index[0]) < self.low_time_index[
            low_end]:
            low_end -= 1
        self.low_time_index = self.low_time_index[low_st:low_end]
        self.low_time_x = {key: val[low_st:low_end] for key, val in self.low_time_x.items()}
        self.low_time_close = self.low_time_close[low_st:low_end]
        self.low_time_predicts = self.low_time_predicts[low_st: low_end]
        self.low_time_y = self.low_time_y[low_st:low_end]

    def setup_length_compatible_high(self):
        high_st = 1
        high_end = len(self.high_time_index) - 2
        while self.high_time_index[high_st] < self.low_time_index[0]:
            high_st += 1
        while self.high_time_index[high_end] > self.low_time_index[-1]:
            high_end -= 1
        high_st -= 1
        high_end += 1
        self.high_time_index = self.high_time_index[high_st:high_end]
        self.high_time_x = {key: val[high_st:high_end] for key, val in self.high_time_x.items()}
        self.high_time_close = self.high_time_close[high_st:high_end]
        self.high_time_predicts = self.high_time_predicts[high_st:high_end]
        self.high_time_y = self.high_time_y[high_st:high_end]

    def check_time_consistency(self):
        try:
            if self.high_time_index[self.high_time_idx] + 2 * (self.high_time_index[1] - self.high_time_index[0]) <= \
                    self.low_time_index[self.low_time_idx]:
                self.high_time_idx += 1
                self.high_time_last_predict = self.high_time_predicts[self.high_time_idx]
                self.high_time_idx = self.high_time_idx - 1 if self.high_time_idx == len(self.high_time_index) - 1 \
                    else self.high_time_idx
        except BaseException as e:
            print(e)

    def statistics_analisys(self):
        expanded_high = pd.DataFrame(
            np.concatenate(
                [self.high_time_predicts.reshape(-1, 1),
                 self.high_time_y.reshape(-1, 1)
                 ], axis=1),
            columns=['pred', 'true'], index=self.high_time_index)
        expanded_high.append(expanded_high.iloc[-1])
        idx = expanded_high.index[:-1].tolist() + [expanded_high.index[-1] + datetime.timedelta(hours=4)]
        expanded_high.index = idx
        expanded_high = expanded_high.resample('5min').first().fillna(method='ffill')
        expanded_high = expanded_high.filter(items=self.low_time_index - datetime.timedelta(hours=4), axis=0)

        expanded_high = expanded_high.values
        basic_low = np.array([self.low_time_predicts, self.low_time_y]).T

        # h4 is corect and m5 is corect
        both_corect = len(expanded_high[np.logical_and(expanded_high[:, 0] == expanded_high[:, 1],
                                                       basic_low[:, 0] == basic_low[:, 1])]) / len(expanded_high)
        both_fault = len(expanded_high[np.logical_and(expanded_high[:, 0] != expanded_high[:, 1],
                                                      basic_low[:, 0] != basic_low[:, 1])]) / len(expanded_high)
        len(expanded_high[
                np.logical_and(
                    expanded_high[:, 0] == expanded_high[:, 1],
                    basic_low[:, 0] == basic_low[:, 1],
                    basic_low[:, 0] != expanded_high[:, 0]
                )
            ]) / len(expanded_high)
        print(both_corect, both_fault)

    def predict(self):
        # self.statistics_analisys()
        preds = []
        for self.low_time_idx in range(len(self.low_time_x['deep_input'])):
            pred = self.low_time_predicts[self.low_time_idx]
            self.check_time_consistency()
            if self.high_time_last_predict == pred:
                preds.append(pred)
            else:
                preds.append(DO_NOTHING_POSITION)
        preds = np.array(preds).reshape(-1, 1)
        return preds
