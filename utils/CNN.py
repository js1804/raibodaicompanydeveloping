from .DLBase import DeepLearningBase
from .GlobalVariable import CLASSIFICATION_MODE
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense

class CNNModel(DeepLearningBase):
    def __init__(self, layer_sizes, filter_sizes, kernel_sizes, strides, padding, activation, optimizer, LR, regularize, drop_out, loss,\
                    epochs, batch_size, verbose=False, window_size=0, pool_type='AveragePooling2D', pool_size=2, pool_stride=1, \
                        pool_padding='valid', feature_count=0, convolution_type='Conv2D', prediction_mode=CLASSIFICATION_MODE, save_model=False,
                        add_pre_zigzag_analizer=False, pre_zigzag_name='zigzagmodel', pre_zigzag_number=0) -> None:
        super().__init__(layer_sizes, activation, optimizer, LR, regularize, drop_out, loss, epochs, batch_size, verbose, feature_count, save_model_after_train=save_model, prediction_mode=prediction_mode,add_pre_zigzag_analizer=add_pre_zigzag_analizer, pre_zigzag_name=pre_zigzag_name, pre_zigzag_number=pre_zigzag_number)
        self.kernel_sizes = kernel_sizes
        self.window_size = window_size
        self.convolution_type = convolution_type
        self.pool_type = getattr(tf.keras.layers, pool_type) 
        self.convNd = getattr(tf.keras.layers, convolution_type)
        self.strides = strides
        self.padding = padding
        self.filter_sizes = filter_sizes
        self.pool_size=pool_size
        self.pool_stride=pool_stride
        self.pool_padding=pool_padding
        self.data_format='channels_first'
        self.model_type = self.convolution_type

    def build_model(self):
        regu = tf.keras.regularizers.L2(.0001) if self.regularize else None
        model = Sequential()

        model.add(self.convNd(self.filter_sizes[0], self.kernel_sizes[0], activation=self.activation,
                            input_shape=[1, self.window_size, self.feature_count],
                            strides=self.strides[0], padding=self.padding[0], data_format=self.data_format))

        model.add(tf.keras.layers.Dropout(self.drop_out))

        for i, item in enumerate(self.filter_sizes[1:]):
            print(i, len(self.filter_sizes))
            model.add(self.convNd(item, self.kernel_sizes[i+1], activation=self.activation,
                                strides=self.strides[i+1], padding=self.padding[i+1], data_format=self.data_format))
            model.add(self.pool_type(pool_size=self.pool_size, strides=self.pool_stride, padding=self.pool_padding, data_format=self.data_format))
            model.add(tf.keras.layers.Dropout(self.drop_out))
        model.add(tf.keras.layers.Flatten())
        for i, item in enumerate(self.layer_sizes[:-1]):
            model.add(Dense(item, activation=self.activation, kernel_regularizer=regu))
            model.add(tf.keras.layers.Dropout(self.drop_out))

        self.add_last_layer_activation(model)
        model.compile(loss=self.loss, optimizer=self.opt, metrics=['accuracy'])
        print(model.summary()) if self.verbose else None
        self.model = model

        self.change_data_shape_for_convolution()

    def change_data_shape_for_convolution(self):
        self.x = self.x.reshape(-1, 1, *self.x.shape[1:])
        self.x_test = self.x_test.reshape(-1, 1, *self.x_test.shape[1:])
