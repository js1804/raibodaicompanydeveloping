import copy

from .MyExceptions import SwitchMismatch

import numpy as np
from sklearn.utils import shuffle


class ClassOverSampling:
    def __init__(self, class_to_over_sample, encoder, nth_power_mode, equalize_mode):
        self.class_to_over_sample = class_to_over_sample
        self.nth_power_mode = nth_power_mode
        self.equalize_mode = equalize_mode
        self.encoder = encoder

    def equalize_data_(self, data, labels):
        purpose_col_data = np.argmax(labels, axis=1)
        number_of_class = max(purpose_col_data) + 1
        max_element_len = max([len(purpose_col_data[purpose_col_data == i]) for i in range(number_of_class)])
        added_label = np.array([])
        for i in range(number_of_class):
            if purpose_col_data[purpose_col_data == i].shape[0] == 0:
                continue
            class_loc = np.where(purpose_col_data == i)[0]
            expansion_factor = int((max_element_len * 1.5) // purpose_col_data[purpose_col_data == i].shape[0] - 1)

            for k, v in data.items():
                nv = copy.deepcopy(v)
                for _ in range(expansion_factor):
                    nv = np.concatenate([nv, v[class_loc]], axis=0)
                data[k] = nv
            for _ in range(expansion_factor):
                added_label = np.concatenate([added_label, [i] * len(class_loc)], axis=0)

        labels = np.concatenate([labels, self.encoder.transform(added_label.reshape(-1, 1))], axis=0) \
                                                                                if len(added_label) > 0 else labels
        l = list(data.values())
        l = shuffle(*shuffle(*l, labels))
        for i, k in enumerate(data.keys()):
            data[k] = l[i]
        labels = l[-1]
        return data, labels

    def over_sample_with_duplication_(self, data, cols):
        pass

    def transform(self, data, labels):
        if self.nth_power_mode > 0:
            return self.over_sample_with_duplication_(data, labels)
        elif self.equalize_mode:
            return self.equalize_data_(data, labels)
        raise SwitchMismatch('mode of over sampling does\'t selected!!!')
