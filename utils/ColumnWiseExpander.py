import numpy as np


class ColumnWiseExpander():
    def __init__(self, repeatation=0) -> None:
        self.repeatation = repeatation

    def transform(self, x):
        new_cols = None
        for i in range(self.repeatation):
            cols_to_append = np.append(x[i:], np.zeros([i, x.shape[1]]), axis=0)
            new_cols = np.append(new_cols, cols_to_append, axis=1) if new_cols != None else cols_to_append
        x = np.append(x, new_cols, axis=1)

    def __call__(self, x):
        return self.transform(x)