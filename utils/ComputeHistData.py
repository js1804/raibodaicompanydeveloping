from .GlobalVariable import STATISTIC_BUCKET_LENGTH, STATISTIC_NUMBER_OF_BUCKET
import numpy as np
import pickle
from tqdm import tqdm


class ComputeHistData:
    def __init__(self, num_bins=STATISTIC_NUMBER_OF_BUCKET, bin_edge=STATISTIC_BUCKET_LENGTH):
        self.num_bins = num_bins
        self.bin_edge = bin_edge

    def make_bin_edge_list(self):
        return [-i*self.bin_edge for i in range(self.num_bins//2)][::-1] + \
               [i*self.bin_edge for i in range(1, self.num_bins//2)]

    def compute(self, data, zigzag_type, column_names, pivot_positions=slice(None, None), file_name_post_fix=''):
        res_dict = {'names': {item: i for i, item in enumerate(column_names)}}
        for col_idx in tqdm(range(data.shape[1])):
            col = data[pivot_positions, col_idx]
            count, bins = np.histogram(col, self.make_bin_edge_list())
            res_dict[col_idx] = {k: v for k, v in zip(np.where(count > 1)[0], count[count > 1])}
        with open(f'bin_edge_analysis_{zigzag_type}_{file_name_post_fix}.pkl', 'wb') as f:
            pickle.dump(res_dict, f)
