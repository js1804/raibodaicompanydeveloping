from .MLBase import MLBase
from .GlobalVariable import CLASSIFICATION_MODE, REGRESSIONER_MODE, PATH_TO_SAVE_MODEL_AFTER_TRAIN, \
    LSTM_PRE_ZIGZAG_SIZES, MAIN_MODEL_INPUT_NAME, PRE_ZIGZAG_MODEL_NAMES, FUTURE_STEP
from .PostProcessorsBase import PostProcessorsBase
from .MultiFutureStatisticsAnalizer import MultiFutureStatisticsAnalizer
from .SectionBasePrepareBackTest import SectionBasePrepareBackTest

import copy
import gc
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from tensorflow import keras
import time
from keras.layers import Dense


class DeepLearningBase(MLBase):
    def __init__(self, layer_sizes, activation, optimizer, LR, regularize, drop_out, loss, epochs, batch_size,
                 verbose=False, feature_count=0, delete_after_train=False, save_model_after_train=False, 
                 prediction_mode=True, in_tune_mode=False, yReal=None,
                 path_to_save_model=PATH_TO_SAVE_MODEL_AFTER_TRAIN, block_size=1,
                 add_pre_zigzag_analizer=False, pre_zigzag_names=PRE_ZIGZAG_MODEL_NAMES,
                 pre_zigzag_number=0, post_processor=PostProcessorsBase):
        self.layer_sizes = copy.deepcopy(layer_sizes)
        self.activation = activation
        self.optimizer = optimizer
        self.LR = LR
        self.verbose = verbose
        self.regularize =regularize
        self.drop_out = drop_out
        self.loss = loss
        self.epochs = epochs
        self.batch_size = batch_size
        self.feature_count = feature_count
        self.save_model_after_train = save_model_after_train
        self.prediction_mode = prediction_mode
        self.set_tensorflow_optimizer()
        self.delete_after_train = delete_after_train
        self.in_tune_mode = in_tune_mode
        self.yReal = yReal
        self.save_time = None
        self.path_to_save_model = path_to_save_model
        self.pre_zigzag_names = pre_zigzag_names
        self.pre_zigzag_number = pre_zigzag_number
        self.add_pre_zigzag_analizer = add_pre_zigzag_analizer
        # self.make_pre_zigzag_model_analizer()
        self.layer_sizes[0] = self.layer_sizes[0]*self.feature_count if self.model_type.startswith('MLP') \
                                                                     else self.layer_sizes[0]
        self.best_epoch = 0
        self.accuracy_list = []
        self.train_accuracy_list = []
        self.validation_accuracy_list = []
        self.train_loss_list = []
        self.early_stop_trades_info = []
        self.y_pred_list = []
        self.post_processor = post_processor
        self.precisions, self.recalls, self.counts = [], [], []
        self.block_size = block_size
        self.multi_future_data = []

    def build_model(self, post_processor=None):
        self.make_pre_zigzag_model_analizer()
        self._build_model()
        self.post_processor = self.post_processor(model=self) if post_processor is None else post_processor(model=self)

    def make_model_input_layer(self, main_input, submodels_input=[]):
        if self.add_pre_zigzag_analizer:
            return submodels_input + [main_input] + [item.in_data for item in self.zigzag_pre_analizer_model_maker]
        else:
            return submodels_input + [main_input]

    def make_model_concatinator_layer(self, main_input, submodels=[]):
        axis = 2 if self.model_type.startswith('LSTM') else 1 if self.model_type.startswith('MLP') else -1
        if len(submodels) != 0:
            first_concat = tf.keras.layers.Concatenate(axis=axis)(submodels + [main_input])
            if self.add_pre_zigzag_analizer:
                return tf.keras.layers.Concatenate(axis=1)([first_concat] + \
                                                           [item for item in self.zigzag_pre_analizer_model])
            else:
                return first_concat
        else:
            if self.add_pre_zigzag_analizer:
                return tf.keras.layers.Concatenate(axis=1)([main_input] + \
                                                           [item for item in self.zigzag_pre_analizer_model])
            else:
                return main_input

    def set_tensorflow_optimizer(self):
        if self.optimizer == 'SGD':
            self.opt = tf.keras.optimizers.SGD(learning_rate=self.LR)
        elif self.optimizer == 'Adam':
            self.opt = tf.keras.optimizers.Adam(learning_rate=self.LR)

    def calculate_precision_recall(self, epoch):
        self.prepare_test_prediction_data()
        self.accuracy_list.append(self.y_true[self.y_true == self.y_pred].shape[0] / self.y_true.shape[0])
        recalls, precisions, counts = [], [], []
        # self.multi_future_data.append(MultiFutureStatisticsAnalizer().analize(self.y_pred_power, self.y_true, self.block_size))
        for i in range(np.max(self.y_true) + 1):
            real_now = self.y_pred[self.y_true == i]
            now_recal = real_now[real_now == i].shape[0] / (len(real_now) + .00001)
            now_precision = self.y_pred[np.logical_and(self.y_pred == i, self.y_true == i)].shape[0] / (
                            self.y_pred[self.y_pred == i].shape[0] + .00001)

            precisions.append(now_precision)
            recalls.append(now_recal)
            counts.append(len(real_now))

        self.recalls.append(recalls)
        self.precisions.append(precisions)
        self.counts.append(counts)
        print('accuracy:', self.accuracy_list[-1], 'recalls:', recalls, 'precisions:', precisions, 'counts', counts)

    def get_best_margin(self, best_model, best_margin, epoch):
        best_trade = None
        self.calculate_precision_recall(epoch)
        self.early_stop_trades_info.append([])
        for i in range(FUTURE_STEP+1):
            spbt = SectionBasePrepareBackTest(self.test_indices, self.yReal, self.post_processor)
            spbt.final(self.x_test, True, i, True, '', None, 1)
            trade_result = spbt.unify_results()
            self.early_stop_trades_info[-1].append(trade_result)
            print('margin:', trade_result['netProfit'])
            if trade_result['netProfit'] > best_margin:
                best_margin = trade_result['netProfit']
                best_model.set_weights(self.model.get_weights())
                self.best_epoch = epoch
                best_trade = trade_result
        print(best_trade) if self.verbose else None
        return best_margin, best_model

    def train_proc(self, **kwargs):
        best_margin = -10e8
        best_model = tf.keras.models.clone_model(self.model)

        for i in range(self.epochs):
            print(f'epoch {i+1}...')
            self.history = self.model.fit(self.x, self.y, epochs=1, batch_size=self.batch_size, verbose=self.verbose,
                                          validation_split=0.2)
            self.train_accuracy_list.extend(self.history.history['accuracy'])
            self.validation_accuracy_list.extend(self.history.history['val_accuracy'])
            self.train_loss_list.extend(self.history.history['loss'])
            best_margin, best_model = self.get_best_margin(best_model, best_margin, i+1)
            tf.keras.backend.clear_session()
            gc.collect()
            print(self.best_epoch) if self.verbose else None
        print(self.best_epoch)
        self.precisions = np.array(self.precisions)
        self.recalls = np.array(self.recalls)
        self.counts = np.array(self.counts)
        self.model.set_weights(best_model.get_weights())

    def add_last_layer_activation(self, model):
        if self.prediction_mode == CLASSIFICATION_MODE:
            model.add(Dense(self.layer_sizes[-1], activation='softmax'))
        elif self.prediction_mode == REGRESSIONER_MODE:
            model.add(Dense(self.layer_sizes[-1]))

    def plot_acc_loss(self, show=False):
        fis, ax = plt.subplots(1, 1)
        try:
            if self.prediction_mode == CLASSIFICATION_MODE:
                plt.figure(figsize=(8, 6))
                ax.plot(self.train_accuracy_list)
                ax.plot(self.validation_accuracy_list)
                ax.plot(self.accuracy_list)
                for i in range(np.array(self.precisions).shape[1]):
                    ax.plot(self.precisions[:, i])
                for i in range(np.array(self.recalls).shape[1]):
                    ax.plot(self.recalls[:, i])
                ax.set_title('model accuracy')
                ax.legend(['train', 'valid', 'test']
                        + [f'prec_{i}th' for i in range(self.precisions.shape[1])]
                        + [f'rec_{i}th' for i in range(self.recalls.shape[1])], loc='upper right')
                ax.show() if show else None
            else:
                ax.figure(figsize=(8, 6))
                ax.plot(self.y_pred)
                ax.plot(self.y_true)
                ax.title('model accuracy')
                ax.ylabel('value')
                ax.xlabel('datas')
                ax.legend(['prediction', 'real'], loc='upper right')
                ax.show()
        except BaseException as e:
            print(e)
        return ax

    def predict_with_threshold(self, data, next_up_thresh=.8, next_down_thresh=.4):
        base = np.zeros(self.y_true.shape[0])
        raw_pred = self.predict(data)
        next_up = raw_pred[:, 1] > next_up_thresh
        next_down = raw_pred[:, 0] > next_down_thresh
        base[next_up] = 1
        base[next_down] = 0
        base[np.logical_not(np.logical_or(next_down, next_up))] = -1
        self.thresh_pred = base
        return np.array(base, dtype=np.int16)

    def prepare_test_prediction_data_classification_mode(self):
        print('classification')
        self.y_pred_power = self.model.predict(self.x_test)
        self.y_pred = np.argmax(self.y_pred_power, axis=1)
        self.y_true = np.argmax(self.y_test, axis=1)

    def prepare_test_prediction_data_regression_mode(self):
        print('regresion')
        self.y_pred = self.model.predict(self.x_test)
        self.y_true = self.y_test

    def predict(self, data):
        return self.model.predict(data)

    def prepare_test_prediction_data(self):
        self.prepare_test_prediction_data_classification_mode() if \
            self.prediction_mode == CLASSIFICATION_MODE else self.prepare_test_prediction_data_regression_mode()

    def save_model(self, path=None):
        if self.save_model_after_train or path != None:
            time_format = '%Y_%m_%d_%H_%M_%S'
            self.save_time = time.strftime(time_format)
            self.path_to_save_model = path if path != None else self.path_to_save_model
            self.model.save(self.path_to_save_model + f'{self.save_time}_{self.model_type}.h5') if self.save_model_after_train else None

    def start_process(self, post_processor=None, **kwargs):
        keras.backend.clear_session()
        
        self.build_model(post_processor)
        self.train_proc(**kwargs)
        self.prepare_test_prediction_data()
        self.plot_acc_loss() if self.save_model_after_train else None
        self.print_prf()
        self.save_model()
        if self.delete_after_train:
            del self.model

    def load_model(self, path):
        self.model = keras.models.load_model(path)
        self.post_processor = self.post_processor(model=self)
