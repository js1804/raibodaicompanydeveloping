from .Discretizer import Discretizer
from .Scaler import Scaler
from .BinaryUpDownMaker import BinaryUpDownMaker
from .SmootherData import SmootherData
from .Regressioner import Regressioner
from .GlobalVariable import CLASSIFICATION_MODE, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD, MAIN_MODEL_INPUT_NAME, \
    PIVOTE_MODE, FILTER_COLUMNS, WANTED_COLS, NUMBER_OF_FUTURE_IN_OVER_SAMPLING, WAVE_LIST_TO_CHECK_BUCKET_BASE, \
    LAST_WAVE_REMOVE_CODE, SECOND_LAST_WAVE_REMOVE_CODE, REVERSE_BASE_LEARNING, FUTURE_STEP, REVERSE_BASE_MODE, \
    NUMBER_OF_TRAIN_TEST_SECTIONS
from .Zigzag_test_period import ZigZagCalculator
from .ZigZagAnalyzer import ZigZogAnalyzer
from .MultiLevelUpDownMaker import MultiLevelUpDownMaker
from .ZigZagWaveFounder import ZigZagDownTimeFrame
from .ZigZagPurposeMaker import ZigZagPurposeMaker
from .IterativeZigzag import IterativeZigzag
from .SpreadPurposeMaker import SpreadPurposeMaker
from .ForwardBackwardZigzagPurposeMaker import ForwardBackwardZigzagPurposeMaker
from .BackwardZigzagPurposeMaker import BackwardZigzagPurposeMaker
from .GetCandlesSinceW0 import GetCandlesSinceW0
from .GetAndUpdateZigZagSinceW0 import GetAndUpdateZigZagsinceW0
from .OpenCloseBasePurposeMaker1D import OpenCloseBasePurposeMaker1D
from .OpenCloseBasePurposeMaker2D import OpenCloseBasePurposeMaker2D
from .BatchMultiFuturePurposeMaker import BatchMultiFuturePurposeMaker
from .OpenCloseBaseLinSpacePurposeMaker1d import OpenCloseBaseLinSpacePurposeMaker1d
from .ClassOverSampling import ClassOverSampling
from .TrainTestSplit import TrainTestSplit
from .BatchMultiFutureOverSampling import BatchMultiFutureOverSampling
from .Normalizer import Normalizer
from .ComputeHistData import ComputeHistData
from .FilterOnHistData import FilterOnHistData
from .StructureBaseComputeHistData import StructureBaseComputeHistData
from .StructureBaseFilterOnHistData import StructureBaseFilterOnHistData
from .StatisticsAnalysis import StatisticsAnalysis
from .StatisticTradeStrategy import StatisticTradeStrategy
from .ReverseBasePurposeMaker import ReverseBasePurposeMaker
from .ZigzagSubwaveSeperator import ZigzagSubwaveSeperator
from .SectionBaseTrainTestSplit import SectionBaseTrainTestSplit

import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder
import copy
from functools import reduce
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from functools import partial
import pickle

ONLY_PIVOT = 1
FULL_FEATURE = 2
ONLY_PIVOT_AND_FULLFEATURE_TOGETHER = 3


class DataHandler:
    NOT_PCT_CHANGE_MULTIPLIER = .00001

    def __init__(self, dataframe_path, kbins, encode, strategy, window_size, discretizer='binary', 
                 smooth_shift_size=0, smooth_window_size=0, purpose_colname='close',
                 smooth_make_loss_profit_equal=False, prediction_mode=CLASSIFICATION_MODE,
                 add_zig_zag_indicators=False, pct_change_on_all=True, pct_and_not_pct_together=False,
                 save_dataFrame=True, load_from_saved_dataFrame=False, multi_label=False, forward_pred=[],
                 multi_level_res=False, multi_level_intervals=.3, number_of_class=2, zig_zag_mode=(True, True, True),
                 add_zigzag_subwave=False, verbose=False, zig_zag_up_thresh=ZIGZAG_UP_THRESHOLD,
                 zig_zag_down_thresh=ZIGZAG_DOWN_THRESHOLD, zigzag_based_learning=False, future=FUTURE_STEP,
                 in_demo_mode=False, spread_base_learning=False, back_for_base_learning=False,
                 back_zigzag_base_learning=False, include_pivots_data=False, pivot_mode=PIVOTE_MODE,
                 inclue_and_update_pivots_data=False, use_submodels=False, open_close_1d_base_learning=False,
                 open_close_2d_base_learning=False, number_of_future=1, multi_future=False, post_processor=None,
                 open_close_1d_base_learning_lin=False, filter_col=FILTER_COLUMNS, filter_cols_names=WANTED_COLS,
                 bucket_base_check_list=WAVE_LIST_TO_CHECK_BUCKET_BASE, reverse_base_learning=REVERSE_BASE_LEARNING):
        self.dataframe_path = dataframe_path
        self.kbins = kbins
        self.encode = encode
        self.strategy = strategy
        self.window_size = window_size
        self.prediction_mode = prediction_mode
        self.encoder = OneHotEncoder(sparse=False)
        self.smooth_shift_size = smooth_shift_size
        self.smooth_window_size = smooth_window_size
        self.purpose_colname = purpose_colname if not zigzag_based_learning else 'zigzag_base'
        self.smooth_make_loss_profit_equal = smooth_make_loss_profit_equal
        self.add_zig_zag_indicators = add_zig_zag_indicators
        self.pct_change_on_all = pct_change_on_all
        self.pct_and_not_pct_together = pct_and_not_pct_together
        self.forward_pred = forward_pred
        self.multi_label = multi_label
        self.multi_level_res = multi_level_res
        self.number_of_class = number_of_class
        self.multi_level_intervals = multi_level_intervals
        self.zig_zag_mode = zig_zag_mode
        self.add_zigzag_subwave = add_zigzag_subwave
        self.verbose = verbose
        self.zig_zag_up_thresh = zig_zag_up_thresh
        self.zig_zag_down_thresh = zig_zag_down_thresh
        self.future_step = future
        self.in_demo_mode = in_demo_mode
        self.spread_base_learning = spread_base_learning
        self.back_for_base_learning  = back_for_base_learning
        self.back_zigzag_base_learning = back_zigzag_base_learning
        self.include_pivots_data = include_pivots_data
        self.pivot_mode = pivot_mode
        self.inclue_and_update_pivots_data = inclue_and_update_pivots_data
        self.use_sub_models = use_submodels
        self.open_close_2d_base_learning = open_close_2d_base_learning
        self.open_close_1d_base_learning = open_close_1d_base_learning
        self.zigzag_based_learning = zigzag_based_learning
        self.zigzag_wave_candles_info = {}
        self.number_of_future = number_of_future if multi_future else 1
        self.multiple_future_base_learning = multi_future
        self.post_processor = post_processor
        self.open_close_1d_base_learning_lin = open_close_1d_base_learning_lin
        self.filter_cols = filter_col
        self.filter_col_names = filter_cols_names
        self.bucket_base_check_list = bucket_base_check_list
        self.reverse_base_learning = reverse_base_learning
        self.x_slice = slice(0, 1 - self.future_step - self.future_step if self.future_step + self.future_step > 1 else None)
        self.y_slice = slice(self.window_size + self.future_step - 1, None if self.number_of_future == 1 else
                                                                  1 - self.number_of_future)
        self.train_test_idx = []

        self.set_updown_maker()
        self.read_data_file()
        if not load_from_saved_dataFrame:
            self.final_pivots = ZigZagCalculator(self.zig_zag_up_thresh, self.zig_zag_down_thresh)\
                                                (self.dataframe, 'close')
            self.condidate_pivots = list(IterativeZigzag(self.dataframe.close, self.zig_zag_up_thresh,
                                                         self.zig_zag_down_thresh).transform())
            self.post_processor = partial(self.post_processor, candidate_pivots=None, first_reverse_point=None,
                                            closes=None, all_pivots=None, test_indices=None)

            self.add_smooth_col_to_data()
            self.check_and_add_zigzag_feature()
            if self.filter_cols:
                self.dataframe = self.dataframe[self.filter_col_names]
            if self.pct_change_on_all:
                self.calc_percentage_change(duplicates=self.pct_and_not_pct_together)
            else:
                self.minimize_data() 
            # self.dataframe *= 100
            self.final_dataframe = copy.deepcopy(self.dataframe)
            # self.pivot_filters = self.create_filter_on_post_bucket()
            # self.remove_codes = self.set_remove_code()
            # self.post_processor = partial(self.post_processor,
            #                               remove_codes=self.remove_codes[-7868:-1])
            Normalizer().transform(self.dataframe)
            # self.filter_rows_base_on_bucket()

            self.dataframe.to_csv('precessed_df.csv') if save_dataFrame else None
        else:
            self.dataframe = pd.read_csv('precessed_df.csv', index_col='time')
            # self.dataframe = pd.read_csv(path_to_load)
            self.make_datetime_index()

        print(self.dataframe)# if self.verbose else None
        # exit()
        self.feature_count = len(self.dataframe.columns)
        self.change_dataframe_to_numpyArray()

        self.up_down_maker() if self.prediction_mode == CLASSIFICATION_MODE else self.regressionor()
        self.batch_multi_future_up_down() if self.multiple_future_base_learning else None

        if discretizer == 'discretizer':
            self.kbins_discretizer()

    def set_updown_maker(self):
        self.up_down_maker = self.binary_up_down if not self.multi_level_res else self.multi_level_up_down
        self.up_down_maker = self.zigzag_up_down if self.zigzag_based_learning else self.up_down_maker
        self.up_down_maker = self.spread_up_down if self.spread_base_learning else self.up_down_maker
        self.up_down_maker = self.backward_forward_zigzag_flags if self.back_for_base_learning else self.up_down_maker
        self.up_down_maker = self.backward_zigzag_flag if self.back_zigzag_base_learning else self.up_down_maker
        self.up_down_maker = self.open_close_1d_up_down_flag if self.open_close_1d_base_learning else self.up_down_maker
        self.up_down_maker = self.open_close_2d_up_down_flag if self.open_close_2d_base_learning else self.up_down_maker
        self.up_down_maker = self.open_close_1d_lin_space_up_down_flag if self.open_close_1d_base_learning_lin \
                                                                       else self.up_down_maker
        self.up_down_maker = self.reverse_base_purpose_maker if self.reverse_base_learning else self.up_down_maker

    def read_data_file(self,):
        self.dataframe = pd.read_csv(self.dataframe_path) if isinstance(self.dataframe_path, str) \
                                                          else self.dataframe_path
        self.make_datetime_index()
        self.close_ = self.dataframe.close

    def make_datetime_index(self, time_format='%Y-%m-%d %H:%M:%S'):
        # time_format = '%d.%m.%Y %H:%M:%S.000'
        # del self.dataframe['volume']
        if self.dataframe.index.name == 'time':
            self.dataframe.set_index(pd.to_datetime(self.dataframe.index, format=time_format))
            return
        self.dataframe['time'] = pd.to_datetime(self.dataframe['time'], format=time_format)
        self.dataframe.set_index('time', inplace=True)

    def change_dataframe_to_numpyArray(self):
        self.cols = list(self.dataframe.columns)
        self.data_index = self.dataframe.index
        self.dataframe = np.array(self.dataframe)

    def minimize_data(self, skip_col_names=('smooth', 'zigzag')):
        cols = self.dataframe.columns
        for item in cols:
            if reduce(lambda a,b: a or b, map(lambda x: x in item, skip_col_names)):
                continue
            if item == 'close':
                self.dataframe[item + '_not__pct'] = self.dataframe[item] * self.NOT_PCT_CHANGE_MULTIPLIER

            self.dataframe[item] *= self.NOT_PCT_CHANGE_MULTIPLIER

    def calc_percentage_change(self, skip_col_names=('smooth', 'zigzag', 'purpose'), duplicates=False):
        cols = self.dataframe.columns
        for item in cols:
            if reduce(lambda a, b: a or b, map(lambda x: x in item, skip_col_names)):
                continue
            if duplicates:
                self.dataframe[item + '_not__pct'] = self.dataframe[item] * self.NOT_PCT_CHANGE_MULTIPLIER
            self.dataframe[item] /= self.dataframe[item].shift(1)
            self.dataframe[item] -= 1
        self.dataframe.fillna(0, inplace=True)

    def add_smooth_col_to_data(self):
        for window in self.smooth_window_size:
            for shift in self.smooth_shift_size:
                self.dataframe = SmootherData(shift, window, True, self.smooth_make_loss_profit_equal)(self.dataframe)
                # self.dataframe = SmootherData(shift, window, False)(self.dataframe)

    def scaler(self):
        self.dataframe = Scaler()(self.dataframe)
    
    def kbins_discretizer(self):
        self.dataframe = Discretizer(self.kbins, self.encode, self.strategy)(self.dataframe, self.cols)

    def binary_up_down(self):
        self.dataframe = BinaryUpDownMaker()(self.dataframe, self.purpose_colname, self.cols)

    def multi_level_up_down(self):
        self.dataframe = MultiLevelUpDownMaker(self.number_of_class, self.multi_level_intervals)(self.dataframe,
                                                                                                 self.purpose_colname,
                                                                                                 self.cols)

    def regressionor(self):
        self.dataframe = Regressioner()(self.dataframe, self.purpose_colname, self.cols)

    def check_and_add_zigzag_feature(self):
        pre_zigzag_data = copy.deepcopy(self.dataframe)
        self.add_zigzag_indicator_function() if self.add_zig_zag_indicators else None
        if self.inclue_and_update_pivots_data:
            self.add_between_wave_data_with_update(pre_zigzag_data)
        if self.include_pivots_data:
            self.add_between_wave_data()
            self.add_zigzag_subwave_function() if self.add_zigzag_subwave else None

    def add_zigzag_indicator_function(self):
        # pivots = ZigZagCalculator(self.zig_zag_up_thresh, self.zig_zag_down_thresh)(self.dataframe, 'close')
        pivots = IterativeZigzag(self.dataframe.close, self.zig_zag_up_thresh, self.zig_zag_down_thresh).transform()
        self.zigzag_analizer = ZigZogAnalyzer(*self.zig_zag_mode)
        self.dataframe = self.zigzag_analizer.transform(self.dataframe, pivots)

    def add_zigzag_subwave_function(self):
        self.dataframe = ZigZagDownTimeFrame().transform(self.dataframe)

    def add_between_wave_data(self):
        if self.include_pivots_data:
            if self.pivot_mode == ONLY_PIVOT or self.pivot_mode == ONLY_PIVOT_AND_FULLFEATURE_TOGETHER:
                self.zigzag_wave_candles_info['zigzagmodel_normal_pivotonly'] = GetCandlesSinceW0(True)\
                                                    .transform(self.dataframe, self.zigzag_analizer.list_of_tss)
            if self.pivot_mode == FULL_FEATURE or self.pivot_mode == ONLY_PIVOT_AND_FULLFEATURE_TOGETHER:
                self.zigzag_wave_candles_info['zigzagmodel_normal_full'] = GetCandlesSinceW0(False)\
                                                    .transform(self.dataframe, self.zigzag_analizer.list_of_tss)

    def add_between_wave_data_with_update(self, pre_zigzag_data):
        if self.inclue_and_update_pivots_data:
            if self.pivot_mode == ONLY_PIVOT or self.pivot_mode == ONLY_PIVOT_AND_FULLFEATURE_TOGETHER:
                self.zigzag_wave_candles_info['zigzagmodel_withupdate_pivotonly'] = \
                        GetAndUpdateZigZagsinceW0(True)\
                        .transform(pre_zigzag_data, self.zig_zag_up_thresh, self.zig_zag_down_thresh, self.zig_zag_mode)
            if self.pivot_mode == FULL_FEATURE or self.pivot_mode == ONLY_PIVOT_AND_FULLFEATURE_TOGETHER:
                self.zigzag_wave_candles_info['zigzagmodel_withupdate_full'] = \
                        GetAndUpdateZigZagsinceW0(False) \
                        .transform(pre_zigzag_data, self.zig_zag_up_thresh, self.zig_zag_down_thresh, self.zig_zag_mode)

    def zigzag_up_down(self):
        self.dataframe = ZigZagPurposeMaker()(self.dataframe, self.cols, self.close_)
        # self.dataframe = ZigZagPurposeMaker()(self.dataframe, self.cols,
        #                                       self.close_.iloc[self.zigzag_analizer.list_of_true_ratios_idxs])

    def filter_rows_base_on_bucket(self):
        columns = list(filter(lambda x: 'zigzag_price' in x, self.dataframe.columns))

        for i, item in enumerate(self.bucket_base_check_list):
            pivot_filter = self.pivot_filters[i]
            filter_col_names = FilterOnHistData.col_names_to_check(columns, [item])
            np_array_col_to_change = self.dataframe[filter_col_names].values
            np_array_col_to_change[pivot_filter] = LAST_WAVE_REMOVE_CODE if i == 0 else SECOND_LAST_WAVE_REMOVE_CODE
            self.dataframe[filter_col_names] = np_array_col_to_change

    def set_remove_code(self):
        res = np.zeros(len(self.dataframe))
        for i, item in enumerate(self.pivot_filters):
            res[item] = LAST_WAVE_REMOVE_CODE if i == 0 else SECOND_LAST_WAVE_REMOVE_CODE
        return res

    def spread_up_down(self):
        self.dataframe = SpreadPurposeMaker(.3)(self.dataframe, self.close_.values, self.cols)

    def backward_forward_zigzag_flags(self):
        self.dataframe = ForwardBackwardZigzagPurposeMaker().transform(self.dataframe, self.close_.values, self.cols)
    
    def backward_zigzag_flag(self):
        self.dataframe = BackwardZigzagPurposeMaker().transform(self.dataframe, self.close_, self.cols)

    def reverse_base_purpose_maker(self):
        self.dataframe = ReverseBasePurposeMaker().transform(self.dataframe, self.cols, self.close_, REVERSE_BASE_MODE)

    def open_close_1d_up_down_flag(self):
        self.dataframe = OpenCloseBasePurposeMaker1D(multiplier=10, base='price',
                                                     zigzag_up_thresh=self.zig_zag_up_thresh,
                                                     zigzag_down_thresh=self.zig_zag_down_thresh)\
                                                    .transform(self.dataframe, self.close_.values, self.cols)

    def open_close_2d_up_down_flag(self):
        self.dataframe = OpenCloseBasePurposeMaker2D(time_sigma_multiplier=2, price_sigma_multiplier=2,
                                                     zigzag_up_thresh=self.zig_zag_up_thresh,
                                                     zigzag_down_thresh=self.zig_zag_down_thresh)\
                                                    .transform(self.dataframe, self.close_, self.cols)

    def open_close_1d_lin_space_up_down_flag(self):
        self.dataframe = OpenCloseBaseLinSpacePurposeMaker1d(base='price', zigzag_up_thresh=self.zig_zag_up_thresh,
                                                             zigzag_down_thresh=self.zig_zag_down_thresh,
                                                             fractions=[1, 2, 1], open_dnt_close_mode=True)\
                                                            .transform(self.dataframe, self.close_.values, self.cols)

    def batch_multi_future_up_down(self):
        self.dataframe = BatchMultiFuturePurposeMaker(self.number_of_future).transform(self.dataframe, self.cols)

    def class_over_sampling_preprocess(self):
        self.dataframe = ClassOverSampling(None, 0, True).transform(self.dataframe, self.cols)

    def batch_multi_future_train_test_split_checker(self, x_t, y_t):
        for k, v in x_t.items():
            x_t[k] = v[::self.number_of_future]
        y_t = y_t[::self.number_of_future]
        return x_t, y_t

    def get_zigzag_candle_info(self, rang:slice):
        if self.inclue_and_update_pivots_data or self.include_pivots_data:
            # self.zigzag_wave_candles_info = self.zigzag_wave_candles_info[rang]
            self.zigzag_wave_candles_info = self.slice_pre_zigzag(self.zigzag_wave_candles_info, rang)
        else:
            self.zigzag_wave_candles_info['none'] = np.zeros(len(self.dataframe) - self.window_size
                                                             - self.number_of_future + 1)
            
    def set_test_idx(self):
        test_idx = np.array(SectionBaseTrainTestSplit.get_test_idx(self.train_test_idx))
        test_idx = np.concatenate([test_idx, (test_idx[:, 1]-test_idx[:, 0]).reshape(-1, 1)], axis=1)
        test_idx[:, 0] += self.window_size - 1
        test_idx[:, 1] += self.window_size - 1 + self.future_step
        self.test_idxs = test_idx
        
    def make_windows(self):
        if self.in_demo_mode:
            self.make_windows_demo()
            self.get_zigzag_candle_info(slice(self.window_size, None))
        else:
            self.make_windows_train()
            self.get_zigzag_candle_info(slice(self.window_size-1, -1))

    def make_windows_demo(self):
        self.train_cols_count = len(self.cols[:-1])
        self.all_x = np.lib.stride_tricks.sliding_window_view(self.dataframe[:, list(range(self.train_cols_count))],
                                                            [self.window_size, self.train_cols_count])[1:]
        self.all_x = self.all_x.reshape(-1, self.window_size, self.train_cols_count)

    def make_windows_train(self):
        self.train_cols_count = len(self.cols[:-1])
        self.all_x = np.lib.stride_tricks.sliding_window_view(self.dataframe[:, list(range(self.train_cols_count))],
                                                              [self.window_size, self.train_cols_count])[self.x_slice]
        self.all_x = self.all_x.reshape(-1, self.window_size, self.train_cols_count)
    
    def split_x_and_zigzagpivot(self, x, data_last_column_index):
        if self.inclue_and_update_pivots_data:
            return x[:, :data_last_column_index], x[:, data_last_column_index:]
        return x, []
    
    def set_preprocessor_parameter(self, y_t):
        self.post_processor = partial(self.post_processor, candidate_pivots=np.array(self.condidate_pivots),
                                      closes=self.close_.values, all_pivots=self.zigzag_analizer.list_of_tss,
                                      first_reverse_point=self.zigzag_analizer.first_reversed_point,
                                      test_indices=self.test_idxs)

    @staticmethod
    def slice_pre_zigzag(data: dict, rang: slice):
        data = {key: val[rang] for key, val in data.items()}
        return data

    def make_train_test_data(self, is_for_mlp=False, x_data=None, train_test_spl=True, future_step=1):
        self.future_step = future_step
        x = copy.deepcopy(self.all_x if x_data is None else x_data)
        x_prezigzag = copy.deepcopy(self.zigzag_wave_candles_info)
        if is_for_mlp:
            x = x.reshape(-1, self.window_size*self.feature_count)
        x_prezigzag = self.slice_pre_zigzag(x_prezigzag, self.x_slice)

        y = self.dataframe[self.y_slice, self.cols.index('purpose')].reshape(-1, 1)

        y = np.concatenate([np.array([[i] for i in range(0, int(np.max(y))+1)]), y])
        y = self.encoder.fit_transform(y) if self.prediction_mode == CLASSIFICATION_MODE else y
        y = y[y.shape[1]:]
        if not train_test_spl:
            x_t = copy.deepcopy(x)
            y_t = copy.deepcopy(y)
            x_t_prezigzag = copy.deepcopy(x_prezigzag)
            x, x_prezigzag, y = shuffle(x, x_prezigzag, np.array(y))

        else:
            x_t_prezigzag = {}
            res, self.train_test_idx = SectionBaseTrainTestSplit()(x, y, *x_prezigzag.values(), 
                                                                   train_size=.75, block_size=self.number_of_future, 
                                                                   num_of_sections=NUMBER_OF_TRAIN_TEST_SECTIONS)
            # res, self.train_test_idx = TrainTestSplit()(x, y, *x_prezigzag.values(), 
            #                                                        train_size=.75, block_size=self.number_of_future,)
            # self.set_test_idx()

            x, x_t, y, y_t = res[:4]
            for i, item in enumerate(x_prezigzag.keys()):
                x_prezigzag[item] = res[i*2 + 4]
                x_t_prezigzag[item] = res[i*2 + 5]
            self.set_preprocessor_parameter(y_t)
        
            x, y, res = BatchMultiFutureOverSampling.transform(x, y, *x_prezigzag.values(),
                                                               num_of_future=NUMBER_OF_FUTURE_IN_OVER_SAMPLING)
            res = shuffle(x, y, *res)
            x, y = res[:2]
            for i, item in enumerate(x_prezigzag.keys()):
                x_prezigzag[item] = res[i+2]

        if not self.inclue_and_update_pivots_data and not self.include_pivots_data:
            x_prezigzag, x_t_prezigzag = {}, {}

        x = {MAIN_MODEL_INPUT_NAME: x}
        x.update(x_prezigzag)
        x_t = {MAIN_MODEL_INPUT_NAME: x_t}
        x_t.update(x_t_prezigzag)
        # x_t, y_t = self.batch_multi_future_train_test_split_checker(x_t, y_t)
        x, y = ClassOverSampling(0, self.encoder, 0, True).transform(x, y)
        # r = np.argmax(y, axis=1)
        # x['deep_input'] = x['deep_input'][r!=0]
        # y = y[r!=0]
        # del r
        self.set_test_idx()
        return x, y, x_t, np.array(y_t), self.test_idxs

    def make_couple_col(self, *args, data=None):
        if len(data) == 0:
            return []
        return reduce(lambda x,y: np.append(x, y, axis=2),
                      map(lambda col_name: data[:,:, self.cols.index(col_name)].reshape(*list(data.shape[:-1]) + [1]),
                          args))
    
    def make_couple_col_mlp(self, *args, data=None):
        ndata = data.reshape(-1, self.window_size, self.feature_count)
        return reduce(lambda x, y: np.append(x, y, axis=1),
                      map(lambda col_name: ndata[:,:, self.cols.index(col_name)], args))

    def check_main_model_input_data(self, data, func:callable, cols):
        for item in data:
            if item.startswith('wide'):
                continue
            if item == 'deep_input':
                data[item] = data[item] if cols == ['all'] else func(*cols, data=data[item])
            elif item.startswith('zigzag'):
                data[item] = data[item] if cols == ['all'] else self.make_couple_col(*cols, data=data[item])
        return data

    def make_train_test_data_dictionary(self, couples, is_for_mlp=False, train_test_spl=True, future_step=1):
        main_model_cols, submodel_cols = couples['main'], couples['submodel']
        x, y, x_test, y_test, _ = self.make_train_test_data(is_for_mlp=is_for_mlp, train_test_spl=train_test_spl,
                                                         future_step=future_step)

        func = self.make_couple_col if is_for_mlp == False else self.make_couple_col_mlp
        for i, item in enumerate(submodel_cols):
            x[f'wide_{i}th'] = func(*item, data=x[MAIN_MODEL_INPUT_NAME])
            x_test[f'wide_{i}th'] = func(*item, data=x_test[MAIN_MODEL_INPUT_NAME])

        x = self.check_main_model_input_data(x, func, main_model_cols)
        x_test = self.check_main_model_input_data(x_test, func, main_model_cols)

        return x, y, x_test, y_test, self.test_idxs
