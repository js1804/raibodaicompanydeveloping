import copy
from functools import reduce
import numpy as np

from .DataHandler import DataHandler
from .GlobalVariable import CLASSIFICATION_MODE, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD, MAIN_MODEL_INPUT_NAME, PRE_ZIGZAG_MODEL_NAMES


class DataHandlerManager:
    def __init__(self, dataframe_paths, **kwargs):
        self.data_handlers = [DataHandler(dataframe_path, **kwargs) for dataframe_path in dataframe_paths]
        for item in self.data_handlers:
            item.make_windows()
        self.datas = []
        self.feature_count = self.data_handlers[0].feature_count
        self.close_ = self.data_handlers[0].close_
        self.final_pivots = self.data_handlers[0].final_pivots
        self.condidate_pivots = self.data_handlers[0].condidate_pivots
        self.zig_zag_up_thresh = self.data_handlers[0].zig_zag_up_thresh
        self.smooth_shift_size = self.data_handlers[0].smooth_shift_size
        self.smooth_window_size = self.data_handlers[0].smooth_window_size
        self.cols = self.data_handlers[0].cols
        self.purpose_colname = self.data_handlers[0].purpose_colname
        self.future_step = self.data_handlers[0].future_step
        self.zig_zag_mode = self.data_handlers[0].zig_zag_mode
        self.post_processor = self.data_handlers[0].post_processor

    def concat_data(self):
        xs = {item: self.datas[0][0][item] for item in self.datas[0][0].keys()}
        y = copy.deepcopy(self.datas[0][1])
        x_t, y_t = self.datas[0][2], self.datas[0][3]
        for data_touples in self.datas[1:]:
            xs = {item: np.concatenate([xs[item], data_touples[0][item]], axis=0) for item in self.datas[0][0].keys()}
            y = np.concatenate([y, data_touples[1]], axis=0)
        # xs['zigzagmodel'] = [] if len(xs['zigzagmodel']) == 0 else xs['zigzagmodel']
        return xs, y, x_t, y_t, self.datas[0][-1]

    def make_train_test_data_dictionary(self, couples, is_for_mlp=False, train_test_spl=True, future_step=1):
        for item in self.data_handlers:
            self.datas.append(item.make_train_test_data_dictionary(couples=couples, is_for_mlp=is_for_mlp,
                                                            train_test_spl=train_test_spl, future_step=future_step))
        return self.concat_data()

    def make_train_test_data(self, is_for_mlp=False, train_test_spl=True, future_step=1):
        for item in self.data_handlers:
            self.datas.append(item.make_train_test_data(is_for_mlp=is_for_mlp,
                                                            train_test_spl=train_test_spl, future_step=future_step))
        self.post_processor = self.data_handlers[0].post_processor

        return self.concat_data()