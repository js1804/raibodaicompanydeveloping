import numpy as np
from sklearn.preprocessing import KBinsDiscretizer

class Discretizer():
    def __init__(self, kbins, encode, strategy='quantile'):
        self.kbdiscretizer = KBinsDiscretizer(kbins, encode= encode, strategy=strategy)
        self.kbins = kbins

    def transform(self, x, cols):
        close_col = x[:, cols.index('close')].reshape(-1, 1)

        self.kbdiscretizer.fit(close_col)
        new_x = np.hstack([self.kbdiscretizer.transform(
            x[:, cols.index(i)].reshape(-1, 1)).astype(np.float32) for i in cols if i!='purpose'])
        if 'purpose' in cols:
            new_x = np.append(new_x, x[:, cols.index('purpose')].reshape(-1,1), axis=1)
        x = new_x
        return x
    
    def __call__(self, x, cols):
        return self.transform(x, cols)
