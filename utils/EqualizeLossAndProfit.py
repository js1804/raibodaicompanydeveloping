import numpy as np
import pandas as pd

class EqualizeLossAndProfit():
    def __init__(self, shift_size):
        self.shift_size = shift_size
    
    def transform(self, x, col):
        new_col = np.array(x[col] / x[col].shift(self.shift_size))
        new_col[:self.shift_size] = 1
        profit_indices = new_col >= 1
        losses = new_col[new_col < 1]
        losses = (1 / losses - 1) * -1
        new_col[new_col < 1] = losses
        new_col[profit_indices] -= 1
        return pd.DataFrame(new_col, index=x.index)

    def __call__(self, x, col):
        return self.transform(x, col)

