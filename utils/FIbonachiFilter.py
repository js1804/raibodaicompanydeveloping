import numpy as np
import pandas as pd
from functools import reduce
import copy
from tqdm import tqdm

FIBONACHI_POSITIVE_LOCATIONS = [.236, .382, .5, .618, .786, .886, 1, 1.13, 1.27, 1.618, 2, 2.618, 3.618, 4.236]
FIBONACHI_NEGATIVE_LOCATIONS = [-i for i in FIBONACHI_POSITIVE_LOCATIONS][::-1]
FIBONACHI_CODES = list(range(-len(FIBONACHI_POSITIVE_LOCATIONS), len(FIBONACHI_POSITIVE_LOCATIONS)))


class FibonachiFilter:
    def __init__(self, filter_width=.01):
        self.filter_width = filter_width

    def transform(self, data, data_names):
        final_truth = []
        for col_idx in range(data.shape[1]):
            col = data[:, col_idx]
            truth_list = []
            for fib_val in FIBONACHI_POSITIVE_LOCATIONS:
                truth_list.append(np.logical_and(col >= fib_val - fib_val*self.filter_width,
                                                 col <= fib_val + fib_val*self.filter_width))
            for fib_val in FIBONACHI_NEGATIVE_LOCATIONS:
                truth_list.append(np.logical_and(col >= fib_val + fib_val*self.filter_width,
                                                 col <= fib_val - fib_val*self.filter_width))
            final_truth.append(np.logical_not(reduce(lambda x, y: np.logical_or(x, y), truth_list)))
        # data[:, col_idx][np.logical_not(reduce(lambda x, y: np.logical_or(x, y), truth_list))] = 0
        # for i in tqdm(range(data.shape[1])):
        #     tmp = copy.deepcopy(data[:, i])
        #     tmp[final_truth[i]] = 0
        #     plt.hist(data[:, i], [-k * .01 for k in range(300)][::-1] + [k * .01 for k in range(1, 300)])
        #     plt.hist(tmp, [-k * .01 for k in range(300)][::-1] + [k * .01 for k in range(1, 300)])
        #     plt.savefig(f'hist_{i}.png')
        #     plt.close()

        return data

    def code_transform(self, data, data_names):
        ndata = copy.deepcopy(data)
        for col_idx in range(ndata.shape[1]):
            col = ndata[:, col_idx]
            truth_list = []
            i = 0
            for i, fib_val in enumerate(FIBONACHI_NEGATIVE_LOCATIONS):
                truth_list.append(np.logical_and(col >= fib_val + fib_val * self.filter_width,
                                                 col <= fib_val - fib_val * self.filter_width))
                col[np.logical_and(col >= fib_val + fib_val * self.filter_width,
                                   col <= fib_val - fib_val * self.filter_width)] = FIBONACHI_CODES[i]

            for j, fib_val in enumerate(FIBONACHI_POSITIVE_LOCATIONS):
                truth_list.append(np.logical_and(col >= fib_val - fib_val * self.filter_width,
                                                 col <= fib_val + fib_val * self.filter_width))
                col[np.logical_and(col >= fib_val - fib_val * self.filter_width,
                                   col <= fib_val + fib_val * self.filter_width)] = FIBONACHI_CODES[j + i + 1]

            col[np.logical_not(reduce(lambda x, y: np.logical_or(x, y), truth_list))] = max(FIBONACHI_CODES) + 1
        return ndata

    def code_transform_(self, data, data_names):
        ndata = copy.deepcopy(data)
        fibs = [-np.inf] + FIBONACHI_NEGATIVE_LOCATIONS + FIBONACHI_POSITIVE_LOCATIONS + [np.inf]
        for i in range(len(fibs) - 1):
            ndata[np.logical_and(ndata >= fibs[i], ndata < fibs[i + 1])] = i - 100
        ndata = ndata.astype(np.int)
        ndata += 100
        return ndata
