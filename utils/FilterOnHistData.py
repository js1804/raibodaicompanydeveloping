from .GlobalVariable import STATISTIC_BUCKET_LENGTH, STATISTIC_NUMBER_OF_BUCKET
import copy
import numpy as np
import pickle


class FilterOnHistData:
    def __init__(self, num_bins=STATISTIC_NUMBER_OF_BUCKET, bin_edge=STATISTIC_BUCKET_LENGTH):
        self.num_bins = num_bins
        self.bin_edge = bin_edge
        self.all_res = []

    def make_bin_edge_list(self):
        return [-i*self.bin_edge for i in range(self.num_bins//2)][::-1] + \
               [i*self.bin_edge for i in range(1, self.num_bins//2)]

    def send_to_sail(self, col_data):
        bins = self.make_bin_edge_list()
        for i, (st, sp) in enumerate(zip(bins[:-1], bins[1:])):
            col_data[np.logical_and(col_data >= st, col_data < sp)] = i
        return col_data

    def compute(self, data, type_name='price', wave_to_check=[4], true_second_wave=None, file_name_post_fix=''):
        with open(f'bin_edge_analysis_{type_name}_{file_name_post_fix}.pkl', 'rb') as f:
            bins_data = pickle.load(f)
        res = np.zeros(len(data))
        alias = self.make_relevant_pairs(list(bins_data['names'].keys()))
        idx_to_name = {val: key for key, val in bins_data['names'].items()}
        for col_idx in (range(data.shape[1])):
            if idx_to_name[col_idx] not in alias:
                continue
            if idx_to_name[col_idx] not in self.col_names_to_check(list(bins_data['names'].keys()), wave_to_check):
                continue
            col = copy.deepcopy(data[:, col_idx])
            col = self.send_to_sail(col)
            col_to_check_with__id = bins_data['names'][alias[idx_to_name[col_idx]][0]]
            has_been_seen_buckets = list(bins_data[col_to_check_with__id].keys())
            res[np.logical_not(res)] = np.logical_or(res[np.logical_not(res)],
                                                     np.logical_not(np.in1d(col[np.logical_not(res)],
                                                                            has_been_seen_buckets)))
            # self.all_res.append(np.logical_not(np.in1d(col, has_been_seen_buckets)))

        if true_second_wave is not None:
            res[true_second_wave] = 0
        with open('existance_check.pkl', 'wb') as f:
            pickle.dump(res, f)
        return res

    @staticmethod
    def col_names_to_check(names_dict, name_of_wave):
        res = []
        for i, item in enumerate(names_dict):
            mid_numb = list(map(lambda x: int(x), filter(lambda x: x.isdigit(), item.split('_'))))[:-1]
            for wave in name_of_wave:
                if len(mid_numb) < 3:
                    continue
                if mid_numb[0] == wave:  # and mid_numb[2] != wave:# and mid_numb[0] - mid_numb[1] > 1:
                    res.append(item)
                    break
        return list(set(res))

    @staticmethod
    def make_relevant_pairs(names_dict, exclusive=True):
        res = {}
        for i, item in enumerate(names_dict[:-1]):
            mid = item.split('_')
            mid_numb = list(map(lambda x: int(x), filter(lambda x: x.isdigit(), mid)))[:-1]
            res[item] = []
            for j, jtem in enumerate(names_dict[(i + 1) if exclusive else i:-1]):
                smid = jtem.split('_')
                smid_numb = list(map(lambda x: int(x), filter(lambda x: x.isdigit(), smid)))[:-1]
                first_dif = smid_numb[0] - mid_numb[0]
                flag = True
                for m in range(1, 4):
                    if smid_numb[m] - mid_numb[m] != first_dif:
                        flag = False
                        break
                if flag:
                    res[item].append(jtem)
            if len(res[item]) == 0:
                del res[item]
        return res

    @staticmethod
    def ratio_base_pair_sorter(point_prices):
        touples = [[i, abs(point_prices[i + 1] - point_prices[i])] for i in range(len(point_prices) - 1)]
        return ''.join(list(map(lambda x: str(x[0]), sorted(touples, key=lambda t: t[1]))))

    @staticmethod
    def structure_base_pair_sorter(point_prices):
        touples = [[i, item] for i, item in enumerate(point_prices)]
        return ''.join(list(map(lambda x: str(x[0]), sorted(touples, key=lambda t: t[1]))))

    @staticmethod
    def both_base_pair_sorter(point_prices):
        return FilterOnHistData.structure_base_pair_sorter(point_prices) + '*' + \
            FilterOnHistData.ratio_base_pair_sorter(point_prices)
