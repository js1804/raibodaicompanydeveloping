from .GlobalVariable import *
from .IterativeZigzag import IterativeZigzag
from .my_zigzag import peak_valley_pivots
from .ZigZagPurposeMaker import ZigZagPurposeMaker
from .BackwardZigzagPurposeMaker import BackwardZigzagPurposeMaker

import numpy as np

class ForwardBackwardZigzagPurposeMaker:
    def __init__(self) -> None:
        pass

    def transform(self, x, close, cols):
        back_flags = BackwardZigzagPurposeMaker().create_backward_flag(close)
        forward_flags = ZigZagPurposeMaker().transform(np.zeros([len(close), 1]), [], close)[:, 1]

        res = []
        for i in range(len(close)):
            if back_flags[i] == 1 and forward_flags[i] == 1:
                res.append(3)
            elif back_flags[i] == 1 and forward_flags[i] == 0:
                res.append(2)
            elif back_flags[i] == 0 and forward_flags[i] == 1:
                res.append(1)
            else:
                res.append(0)
        res = np.array(res).reshape(-1, 1)
        if 'purpose' not in cols:
            x = np.append(x, res, axis=1) 
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = res

        return x
        

