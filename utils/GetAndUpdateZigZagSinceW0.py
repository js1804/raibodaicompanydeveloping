from .Zigzag_test_period import ZigZagCalculator
from .ZigZagAnalyzer import ZigZogAnalyzer
from .IterativeZigzag import IterativeZigzag

import numpy as np
import copy
import tensorflow as tf


class GetAndUpdateZigZagsinceW0:
    def __init__(self, only_pivot=True) -> None:
        self.only_pivot = only_pivot

    def transform(self, dataframe, zig_zag_up_thresh, zig_zag_down_thresh, zigzag_mode):
        base_df = copy.deepcopy(dataframe)
        
        final_pivots = ZigZagCalculator(zig_zag_up_thresh, zig_zag_down_thresh)(dataframe, 'close')
        zigzag_with_future = ZigZogAnalyzer(*zigzag_mode).transform(base_df, final_pivots)

        iterative_pivot = IterativeZigzag(base_df.close.values, zig_zag_up_thresh, zig_zag_down_thresh).transform()
        zigzager = ZigZogAnalyzer(*zigzag_mode)
        correct_zigzag = zigzager.transform(base_df, iterative_pivot)
        list_of_wave_positions = zigzager.list_of_tss

        cols = zigzag_with_future.columns

        temp_future = zigzag_with_future.values.reshape(-1, len(cols))
        temp_future[np.isnan(temp_future)] = 0
        temp_correct = correct_zigzag.values.reshape(-1, len(cols))
        temp_correct[np.isnan(temp_correct)] = 0

        if self.only_pivot:
            return self.pivots_only_calculation(list_of_wave_positions, temp_future, temp_correct)
        else:
            return self.since_w0_calculation(list_of_wave_positions, temp_future, temp_correct)

    def since_w0_calculation(self, list_of_wave_positions, temp_future, temp_correct):
        res = [temp_correct[:1]]
        last_seen_w4 = 0
        for [w0, w1, w2, w3, w4, w5] in list_of_wave_positions:
            if last_seen_w4 != w4:
                if w4 < last_seen_w4:
                    res.append(temp_future[w0: w5])
                else:
                    res.append(np.append(temp_future[w0:w4], temp_correct[w4:w5], axis=0))
                last_seen_w4 = w4
            else:
                res.append(np.append(temp_future[w0:w4], temp_correct[w4:w5], axis=0))
        res = tf.keras.preprocessing.sequence.pad_sequences(res[:1] + res[2:], padding='pre')
        return res

    def pivots_only_calculation(self, list_of_wave_positions, temp_future, temp_correct):
        res = []
        for [w0, w1, w2, w3, w4, w5] in list_of_wave_positions:
            item = [w0, w1, w2, w3, w4, w5]
            res.append(temp_future[item[:-1]])
        return np.array(res)
