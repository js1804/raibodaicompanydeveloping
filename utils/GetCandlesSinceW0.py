
import numpy as np
import tensorflow as tf


class GetCandlesSinceW0:
    def __init__(self, only_pivot=True) -> None:
        self.only_pivot = only_pivot

    def get_only_pivots(self, dataframe, list_of_wave_positions):
        res = []
        cols = dataframe.columns
        temp = dataframe.values.reshape(-1, len(cols))
        temp[np.isnan(temp)] = 0
        for item in list_of_wave_positions:
            res.append(temp[item[:-1]])
        return np.array(res)

    def get_throughfull(self, dataframe, list_of_wave_positions):
        res = []
        cols = dataframe.columns
        temp = dataframe.values.reshape(-1, len(cols))
        temp[np.isnan(temp)] = 0
        for item in list_of_wave_positions:
            res.append(temp[item[0]: item[-1]])
        res = tf.keras.preprocessing.sequence.pad_sequences(res, padding='pre')

        return res

    def transform(self, dataframe, list_of_wave_position):
        if self.only_pivot:
            return self.get_only_pivots(dataframe, list_of_wave_position)
        else:
            return self.get_throughfull(dataframe, list_of_wave_position)
