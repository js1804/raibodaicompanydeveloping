GRAPHIC_CARD_NUMBER = 0
FOLDER_TO_SAVE_RESULTS = 'genetic'
MAIN_MODEL_INPUT_NAME = 'deep_input'
NUMBER_OF_OUTPUT_CLASSES = 2
WINDOW_SIZE = 3
LR = .001
DROP_OUT = .2
EPOCHS = 20
BATCH_SIZE = 16
VERBOSE = True
ACTIVATION_FUNCTION = 'ReLU'
OPTIMIZER = 'Adam'
CLASSIFICATION_MODE = 1
REGRESSIONER_MODE = 0
PREDICTION_MODE = CLASSIFICATION_MODE
FUTURE_STEP = 1
# LOSS_FUNCTION = 'categorical_crossentropy' #mean_squared_error
LOSS_FUNCTION = 'categorical_crossentropy' #mean_squared_error
# best for h4: {'shift': 3, 'smooth': 3, 'future': 4, 'i': 3}
# best for m30: {'shift': 4, 'smooth': 5, 'future': 6, 'i': 5,}
# best for m15: {shift': 5, 'smooth': 7, 'future': 9, 'i': 8}
SMOOTH_SHIFT_SIZE = [1, 3]
SMOOTH_WINDOW_SIZE = [3]
DATAFRAME_PATH = 'dataset_4h.csv'
COUPLES = [['close', 'open'],['close', 'high'],['close', 'low'],['high', 'low'],['high', 'open'],]
PATH_TO_SAVE_MODEL_AFTER_TRAIN = 'saved_models/'
REPETITION_OF_RUN = 2
SHOW_FIGS = True
SAVE_MODEL_WEIGHTS = True
NUMBER_OF_GRAPHIC_CARD = 1
NUMBER_OF_RUN_PER_CARD = 1
TIMER_FOR_MULTIPLE_RUN = 10000
TIMER_FOR_SINGLE_RUN = 3600
NUMBER_OF_TRAIN_TEST_SECTIONS = 10
#best for 4h --> .01
#best for 30min --> .004
#best for 15min --> .004
#best for 5min --> .002
ZIGZAG_UP_THRESHOLD = .01
ZIGZAG_DOWN_THRESHOLD = -.01
MINIMUM_ZIGZAG_WAVE_COUNT = 10 
BUY_POSITION = 1
SELL_POSITION = 0
DO_NOTHING_POSITION = 2
CLOSE_POSITION = 3
OPEN_POSITION = 4
ADD_ZIGZAG_SUBWAVE = False
MULTI_LEVEL_INTERVALS = .2
ZIG_ZAG_MODE = (True, False, False) # (price, time, pace)
ZIGZAG_BASED_LEARNING = True
SPREAD_BASE_LEARNING = False
BACK_FOR_BASE_LEARNING = False
BACK_ZIGZAG_BASE_LEARNING = False
OPEN_CLOSE_1D_BASE_LEARNING = False
OPEN_CLOSE_2D_BASE_LEARNING = False
OPEN_CLOSE_1D_BASE_LEARNING_LIN = False
MULTIPLE_FUTURE_BASE_LEARNING = False
REVERSE_BASE_LEARNING = False
REVERSE_BASE_MODE = 'w5_prime' #choose between candidate_pivots and w5_prime
REVERSE_BASE_POST_PROCESSOR_MODE = 'w5_prime' #choose between candidate_pivots and w5_prime
NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE = 1
NUMBER_OF_FUTURE_IN_OVER_SAMPLING = 1
PIVOTE_MODE = 3 #use 1 for pivot only, 2 for full mode and 3 for pivot only and full together
INCLUDE_PIVOTS_DATA = False
INCLUDE_AND_UPDATE_PIVOTS_DATA = False
LSTM_PRE_ZIGZAG_SIZES = [30, 50,WINDOW_SIZE*3]
SUBMODEL_LSTM_LAYERS_SHAPE = [2,30,1]
MAIN_LSTM_LAYERS_SHAPE = [WINDOW_SIZE*3, 300, 200, NUMBER_OF_OUTPUT_CLASSES]
SUBMODEL_MLP_LAYERS_SHAPE = [2,30,1]
MAIN_MLP_LAYERS_SHAPE = [WINDOW_SIZE, 300, 200, NUMBER_OF_OUTPUT_CLASSES]
USE_SUBMODELS = False
PRE_ZIGZAG_UPDATE = ['zigzagmodel_withupdate_pivotonly', 'zigzagmodel_withupdate_full']
PRE_ZIGZAG_NORMAL = ['zigzagmodel_normal_pivotonly', 'zigzagmodel_normal_full']
PRE_ZIGZAG_MODEL_NAMES = ['zigzagmodel_withupdate_pivotonly' , 'zigzagmodel_normal_pivotonly']
MIN_COVERAGE = .98
PATH_LIST_OF_ZIGZAG_SEQUENCE_NAME = r'D:\E\projects\test_borse\wave_structure.pkl'
FILTER_COLUMNS = False
STATISTIC_BUCKET_LENGTH = .05
STATISTIC_NUMBER_OF_BUCKET = 20000
WAVE_LIST_TO_CHECK_BUCKET_BASE = [4]
LAST_WAVE_REMOVE_CODE = -1
SECOND_LAST_WAVE_REMOVE_CODE = -2
NUMBER_OF_PIVOTS = 6
NUMBER_OF_RATIOS = 105
SHIFT_SMOOTH_COLS_NAME = ['high_pct_change_1_smoother_3','low_pct_change_1_smoother_3','open_pct_change_1_smoother_3',
                           'close_pct_change_1_smoother_3','high_pct_change_3_smoother_3','low_pct_change_3_smoother_3',
                           'open_pct_change_3_smoother_3','close_pct_change_3_smoother_3']
BASIC_ZIGZAG_FEATURE_NAMES = [
    'close', 'open', 'high', 'low',

    'high_pct_change_1_smoother_3', 'low_pct_change_1_smoother_3', 'open_pct_change_1_smoother_3',
    'close_pct_change_1_smoother_3', 'high_pct_change_3_smoother_3', 'low_pct_change_3_smoother_3',
    'open_pct_change_3_smoother_3', 'close_pct_change_3_smoother_3',

    'zigzag_price__waves_5__4_d_waves_4__3_0',
    'zigzag_price__waves_5__4_d_waves_3__2_0',
    'zigzag_price__waves_5__4_d_waves_1__0_0',
    'zigzag_price__waves_4__3_d_waves_3__2_0',
    'zigzag_price__waves_4__3_d_waves_2__1_0',
    'zigzag_price__waves_3__2_d_waves_2__1_0',
    'zigzag_price__waves_3__2_d_waves_1__0_0',
    'zigzag_price__waves_2__1_d_waves_1__0_0',

    'zigzag_time__waves_5__4_d_waves_4__3_0',
    'zigzag_time__waves_5__4_d_waves_3__2_0',
    'zigzag_time__waves_5__4_d_waves_1__0_0',
    'zigzag_time__waves_4__3_d_waves_3__2_0',
    'zigzag_time__waves_4__3_d_waves_2__1_0',
    'zigzag_time__waves_3__2_d_waves_2__1_0',
    'zigzag_time__waves_3__2_d_waves_1__0_0',
    'zigzag_time__waves_2__1_d_waves_1__0_0'
]
COMPLETE_ZIGZAG_FEATURE_NAMES = ['zigzag_price__waves_2__1_d_waves_1__0_0',
                        'zigzag_price__waves_3__2_d_waves_1__0_0',
                        'zigzag_price__waves_3__2_d_waves_2__1_0',
                        # 'zigzag_price__waves_4__3_d_waves_1__0_0',
                        'zigzag_price__waves_4__3_d_waves_2__1_0',
                        # 'zigzag_price__waves_4__3_d_waves_3__0_0',
                        'zigzag_price__waves_4__3_d_waves_3__2_0',
                        # 'zigzag_price__waves_5__2_d_waves_1__0_0',
                        # 'zigzag_price__waves_5__2_d_waves_2__1_0',
                        'zigzag_price__waves_5__4_d_waves_1__0_0',
                        'zigzag_price__waves_5__4_d_waves_2__1_0',
                        # 'zigzag_price__waves_5__4_d_waves_3__0_0',
                        'zigzag_price__waves_5__4_d_waves_3__2_0',
                        'zigzag_price__waves_5__4_d_waves_4__3_0',
                        'zigzag_time__waves_2__1_d_waves_1__0_0',
                        'zigzag_time__waves_3__1_d_waves_2__0_0',
                        'zigzag_time__waves_3__2_d_waves_1__0_0',
                        'zigzag_time__waves_3__2_d_waves_2__0_0',
                        'zigzag_time__waves_3__2_d_waves_2__1_0',
                        'zigzag_time__waves_4__2_d_waves_2__0_0',
                        'zigzag_time__waves_4__2_d_waves_3__1_0',
                        'zigzag_time__waves_4__3_d_waves_2__1_0',
                        'zigzag_time__waves_4__3_d_waves_3__0_0',
                        'zigzag_time__waves_4__3_d_waves_3__1_0',
                        'zigzag_time__waves_4__3_d_waves_3__2_0',
                        'zigzag_time__waves_5__1_d_waves_2__0_0',
                        'zigzag_time__waves_5__1_d_waves_4__0_0',
                        'zigzag_time__waves_5__2_d_waves_1__0_0',
                        'zigzag_time__waves_5__3_d_waves_3__1_0',
                        'zigzag_time__waves_5__3_d_waves_3__2_0',
                        'zigzag_time__waves_5__3_d_waves_4__2_0',
                        'zigzag_time__waves_5__4_d_waves_1__0_0',
                        'zigzag_time__waves_5__4_d_waves_2__1_0',
                        'zigzag_time__waves_5__4_d_waves_3__0_0',
                        'zigzag_time__waves_5__4_d_waves_3__2_0',
                        'zigzag_time__waves_5__4_d_waves_4__0_0',
                        'zigzag_time__waves_5__4_d_waves_4__1_0',
                        'zigzag_time__waves_5__4_d_waves_4__2_0',
                        'zigzag_time__waves_5__4_d_waves_4__3_0']
WANTED_COLS = SHIFT_SMOOTH_COLS_NAME
# def fix_gpu_memory():
#     pass
