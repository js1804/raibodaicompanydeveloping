import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

PEAK, VALLEY = 1, -1

class IterativeZigzag:
    def __init__(self, x, up_thrsh, down_thresh) -> None:
        self.x = x
        self.up_thresh = up_thrsh
        self.down_thresh = down_thresh

    def transform(self):
        return IterativeZigzag.peak_valley_pivots(self.x, self.up_thresh, self.down_thresh)

    def plot(self):
        pivots = np.array([item for item in self.transform()])
        plt.figure(figsize=(15, 9))
        plt.xlim(0, len(self.x))
        plt.ylim(self.x.min()*0.99, self.x.max()*1.01)
        plt.plot(np.arange(len(self.x)), self.x, 'k:', alpha=0.5)
        plt.plot(np.arange(len(self.x))[pivots != 0], self.x[pivots != 0], 'k-')
        plt.scatter(np.arange(len(self.x))[pivots == 1], self.x[pivots == 1], color='g')
        plt.scatter(np.arange(len(self.x))[pivots == -1], self.x[pivots == -1], color='r')
        plt.show()
    
    @staticmethod
    def _identify_initial_pivot(X, up_thresh, down_thresh):
        x_0 = X[0]
        max_x = x_0
        max_t = 0
        min_x = x_0
        min_t = 0
        up_thresh += 1
        down_thresh += 1

        for t in range(1, len(X)):
            x_t = X[t]

            if x_t / min_x >= up_thresh:
                return VALLEY if min_t == 0 else PEAK

            if x_t / max_x <= down_thresh:
                return PEAK if max_t == 0 else VALLEY

            if x_t > max_x:
                max_x = x_t
                max_t = t

            if x_t < min_x:
                min_x = x_t
                min_t = t

        t_n = len(X)-1
        return VALLEY if x_0 < X[t_n] else PEAK

    @staticmethod
    def peak_valley_pivots(X, up_thresh, down_thresh):
        if down_thresh > 0:
            raise ValueError('The down_thresh must be negative.')

        initial_pivot = IterativeZigzag._identify_initial_pivot(X, up_thresh, down_thresh)

        t_n = len(X)
        yield initial_pivot

        up_thresh += 1
        down_thresh += 1

        trend = -initial_pivot
        last_pivot_x = X[0]
        for t in range(1, len(X)):
            x = X[t]
            r = x / last_pivot_x

            if trend == -1:
                if r >= up_thresh:
                    trend = 1
                    last_pivot_x = x
                    yield trend
                elif x < last_pivot_x:
                    last_pivot_x = x
                    yield trend
                else:
                    yield 0
            else:
                if r <= down_thresh:
                    # yield trend
                    trend = -1
                    last_pivot_x = x
                    yield trend
                elif x > last_pivot_x:
                    last_pivot_x = x
                    yield trend
                else:
                    yield 0

if __name__ == '__main__':
    df = pd.read_csv(r"D:\E\projects\test_borse\dataset_4h.csv")
    IterativeZigzag(df.close, .01, -.01).plot()