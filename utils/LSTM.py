from .DLBase import DeepLearningBase
from .GlobalVariable import CLASSIFICATION_MODE, NUMBER_OF_OUTPUT_CLASSES, LSTM_PRE_ZIGZAG_SIZES, PRE_ZIGZAG_MODEL_NAMES
from .LSTMBaseZigzagPreWaveAnalizerModel import  LSTMBaseZigzagPreWaveAnalizerModel
from keras.models import Sequential
from keras.layers import Dense, LSTM
import tensorflow as tf


class LSTMModel(DeepLearningBase):
    def __init__(self, layer_sizes, activation, optimizer, LR, regularize, drop_out, loss, epochs, batch_size,
                verbose=False, window_size=0, feature_count=0, dif_shape_lstms = None, feature_count_diff=None,
                prediction_mode=CLASSIFICATION_MODE, save_model=False, yReal=None, in_tune_mode=False,
                add_pre_zigzag_analizer=False, pre_zigzag_names=PRE_ZIGZAG_MODEL_NAMES, pre_zigzag_number=0,
                post_processor=None, block_size=None):
        self.window_size = window_size
        self.dif_shape_lstms = dif_shape_lstms
        self.feature_count_diff = feature_count_diff
        self._build_model = self.build_ds_model if self.dif_shape_lstms else self.build_sequential_model
        self.model_type = 'LSTM' if dif_shape_lstms==None else 'LSTM_DS'
        super().__init__(layer_sizes, activation, optimizer, LR, regularize, drop_out, loss, epochs,
                         batch_size, verbose, feature_count, save_model_after_train=save_model,
                         prediction_mode=prediction_mode, in_tune_mode=in_tune_mode, yReal=yReal,
                         add_pre_zigzag_analizer=add_pre_zigzag_analizer, pre_zigzag_names=pre_zigzag_names,
                         pre_zigzag_number=pre_zigzag_number, post_processor=post_processor, block_size=block_size)


    def make_pre_zigzag_model_analizer(self, feature_count=None):
        output_shape = sum([item[-1] for item in self.dif_shape_lstms]) + self.feature_count if self.model_type == 'LSTM_DS' else self.feature_count
        if self.add_pre_zigzag_analizer:
            self.zigzag_pre_analizer_model_maker = []
            self.zigzag_pre_analizer_model = []
            for item in self.pre_zigzag_names:
                self.zigzag_pre_analizer_model_maker.append(LSTMBaseZigzagPreWaveAnalizerModel(LSTM_PRE_ZIGZAG_SIZES,
                                                           self.drop_out, in_shape=self.feature_count,
                                                           out_shape=output_shape, name=item,
                                                           number=self.pre_zigzag_number, is_for_mlp=False))
                self.zigzag_pre_analizer_model.append(self.zigzag_pre_analizer_model_maker[-1].build_zigzag_model())


    def build_sequential_model(self):    
        deep_layer_input = tf.keras.layers.Input(shape=[self.window_size, self.feature_count], name='deep_input')
        model = self.make_model_concatinator_layer(deep_layer_input)

        model = LSTM(self.layer_sizes[0], activation='tanh', dropout=self.drop_out, return_sequences=True)(model)

        for i, item in enumerate(self.layer_sizes[1:-1]):
            model = LSTM(item, activation='tanh', dropout=self.drop_out, return_sequences=True if i + 1< len(self.layer_sizes)-2 else False)(model)
            model = tf.keras.layers.Dropout(self.drop_out, name=f'deep_drop{i}')(model)
        
        final_input_layer = self.make_model_input_layer(deep_layer_input)

        model = Dense(self.layer_sizes[-1], activation='softmax')(model)
        model = tf.keras.Model(inputs=final_input_layer, outputs=model)
        
        model.compile(loss=self.loss, optimizer=self.opt, metrics=['accuracy'])
        print(model.summary()) if self.verbose else None
        self.model = model

    def make_each_ds_block(self, lay, i):
        in_X = tf.keras.layers.Input(shape=(self.window_size, self.feature_count_diff[i]), name=f'wide_{i}th')
        # X = LSTM(lay[0], input_shape=[self.window_size, self.feature_count_diff[i]], return_sequences=True)
        for j, item in enumerate(lay):
            X = LSTM(item, return_sequences=True)(X if j>0 else in_X)
            X = tf.keras.layers.Dropout(self.drop_out, name=f'wide_{i}th_dropout_{j}')(X)
        return in_X, X

    def build_ds_model(self, hp=None):
        ds_models = []
        input_layers = []
        deep_layer_input = tf.keras.layers.Input(shape=[self.window_size, self.feature_count], name='deep_input')

        for i, lay in enumerate(self.dif_shape_lstms):
            input_layer, wide_model = self.make_each_ds_block(lay, i)
            ds_models.append(wide_model)
            input_layers.append(input_layer)

        final_input_layer = self.make_model_input_layer(deep_layer_input, input_layers)
        
        remider_lstms = self.make_model_concatinator_layer(deep_layer_input, ds_models)
        if hp != None:
            self.layer_sizes = [hp.Int(f'LSTM_Layer_{i}', 5, 1000) for i in range(hp.Int('LSTM_Layers_count', 1, 7))] + [NUMBER_OF_OUTPUT_CLASSES]

        for i, item in enumerate(self.layer_sizes[:-1]):
            remider_lstms = LSTM(item, activation='tanh', dropout=self.drop_out, return_sequences=True if i < len(self.layer_sizes) - 2 else False)(remider_lstms)
            remider_lstms = tf.keras.layers.Dropout(self.drop_out, name=f'deep_drop{i}')(remider_lstms)

        remider_lstms = Dense(self.layer_sizes[-1], activation='softmax' if self.prediction_mode==CLASSIFICATION_MODE else None)(remider_lstms)

        model = tf.keras.Model(inputs=final_input_layer, outputs=remider_lstms)
        model.compile(loss=self.loss, optimizer=self.opt, metrics=['accuracy'])
        print(model.summary()) if self.verbose else None
        self.model = model
        return model
