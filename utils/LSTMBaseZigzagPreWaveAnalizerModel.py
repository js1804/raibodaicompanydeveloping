
from keras.models import Sequential
from keras.layers import LSTM
import tensorflow as tf
import copy

class LSTMBaseZigzagPreWaveAnalizerModel:
    def __init__(self, layer_sizes, drop_out, in_shape, out_shape, name='zigzagmodel', number=0, is_for_mlp=False):
        self.layer_sizes = layer_sizes
        self.drop_out = drop_out
        self.name = name
        self.number = number
        self.is_for_mlp = is_for_mlp
        self.in_shape = in_shape
        self.out_shape = out_shape

    def build_zigzag_model(self):
        self.in_data = tf.keras.layers.Input(shape=[None, self.in_shape], name=f'{self.name}')
        # model = copy.deepcopy(self.in_data)

        model = LSTM(self.layer_sizes[0], return_sequences=True, name=f'{self.name}_lstm_0')(self.in_data)
        model = tf.keras.layers.Dropout(self.drop_out)(model)

        for i, item in enumerate(self.layer_sizes[:-1]):
            model = LSTM(item, return_sequences=True, name=f'{self.name}_lstm_{i+1}')(model)
            model = tf.keras.layers.Dropout(self.drop_out)(model)
        
        model = LSTM(self.out_shape, return_sequences=False, name=f'{self.name}_lstm_final')(model)
        model = tf.keras.layers.Reshape([1, -1])(model) if not self.is_for_mlp else model
        return model

    def __str__(self):
        return f'{self.name}'
    
