from .GlobalVariable import REGRESSIONER_MODE
from sklearn.metrics import precision_recall_fscore_support

class MLBase():
    def set_data(self, x, y, x_test, y_test, test_indices):
        self.x = x
        self.y = y
        self.x_test = x_test
        self.y_test = y_test
        self.test_indices = test_indices

    def print_prf(self):
        if self.prediction_mode==REGRESSIONER_MODE:
            return
        print('accuracy:', self.y_true[self.y_true==self.y_pred].shape[0] / self.y_true.shape[0])
        print('average type: macro')
        print(precision_recall_fscore_support(self.y_true, self.y_pred, average='macro'))
        print('average type: micro')
        print(precision_recall_fscore_support(self.y_true, self.y_pred, average='micro'))    
        print('average type: weighted')
        print(precision_recall_fscore_support(self.y_true, self.y_pred, average='weighted'))
        print('*'*30, '\n')    

    def find_last_model_by_type(self):
        pass