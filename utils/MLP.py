from .DLBase import DeepLearningBase
from .LSTMBaseZigzagPreWaveAnalizerModel import  LSTMBaseZigzagPreWaveAnalizerModel
from .GlobalVariable import CLASSIFICATION_MODE, NUMBER_OF_OUTPUT_CLASSES, LSTM_PRE_ZIGZAG_SIZES, PRE_ZIGZAG_MODEL_NAMES
import tensorflow as tf
from keras.layers import Dense
from keras.models import Sequential


class MLP(DeepLearningBase):
    def __init__(self, layer_sizes, activation, optimizer, LR, regularize, drop_out,
                 loss, epochs, batch_size, verbose=False, feature_count=0,
                 prediction_mode=CLASSIFICATION_MODE, save_model=False,
                 in_tune_mode=False, yReal=None, dif_shape_mlp=None,
                 feature_count_diff=None, window_size=None, block_size=None,
                 add_pre_zigzag_analizer=False, pre_zigzag_names=PRE_ZIGZAG_MODEL_NAMES,
                 pre_zigzag_number=0, post_processor=None):
        self.model_type = 'MLP' if dif_shape_mlp is None else 'MLP_DS'
        self.dif_shape_mlp = dif_shape_mlp
        self.feature_count_diff = feature_count_diff
        self.window_size = window_size
        self._build_model = self.build_ds_model if self.dif_shape_mlp else self.build_sequential_model
        super().__init__(layer_sizes, activation, optimizer, LR, regularize, 
                         drop_out, loss, epochs, batch_size, verbose, feature_count,
                         save_model_after_train=save_model, prediction_mode=prediction_mode,
                         in_tune_mode=in_tune_mode, yReal=yReal, block_size=block_size,
                         add_pre_zigzag_analizer=add_pre_zigzag_analizer, pre_zigzag_names=pre_zigzag_names,
                         pre_zigzag_number=pre_zigzag_number, post_processor=post_processor)

    def make_pre_zigzag_model_analizer(self, feature_count=None):
        if self.add_pre_zigzag_analizer:
            self.zigzag_pre_analizer_model_maker = []
            self.zigzag_pre_analizer_model = []
            for item in self.pre_zigzag_names:
                self.zigzag_pre_analizer_model_maker.append(LSTMBaseZigzagPreWaveAnalizerModel(LSTM_PRE_ZIGZAG_SIZES,
                                                           self.drop_out, in_shape=self.feature_count,
                                                           out_shape=self.feature_count, name=item,
                                                           number=self.pre_zigzag_number, is_for_mlp=True))
                self.zigzag_pre_analizer_model.append(self.zigzag_pre_analizer_model_maker[-1].build_zigzag_model())


    def build_sequential_model(self):
        regu = tf.keras.regularizers.L2(.0001) if self.regularize else None

        deep_layer_input = tf.keras.layers.Input(shape=[self.layer_sizes[0]], name='deep_input')

        model = self.make_model_concatinator_layer(deep_layer_input)
        
        for i, item in enumerate(self.layer_sizes[:-1]):
            model = Dense(item, activation='relu', kernel_regularizer=regu)(model)
            model = tf.keras.layers.Dropout(self.drop_out, name=f'deep_drop{i}')(model)
        
        # self.add_last_layer_activation(model)
        final_input_layer = self.make_model_input_layer(deep_layer_input)
        model = Dense(self.layer_sizes[-1], activation='softmax')(model)
        model = tf.keras.Model(inputs=final_input_layer, outputs=model)

        model.compile(loss=self.loss, optimizer=self.opt, metrics=['accuracy'])
        print(model.summary()) if self.verbose else None
        self.model = model

    def make_each_ds_block(self, lay, i):
        in_X = tf.keras.layers.Input(shape=(lay[0]*self.window_size), name=f'wide_{i}th')

        for j, item in enumerate(lay):
            X = Dense(item, activation='relu')(X if j>0 else in_X)
            X = tf.keras.layers.Dropout(self.drop_out, name=f'wide_{i}th_drop_out_{j}')(X)
        return in_X, X

    def build_ds_model(self):
        ds_models = []
        input_layers = []
        regu = None#tf.keras.regularizers.L2(.0001) if self.regularize else None
        deep_layer_input = tf.keras.layers.Input(shape=[self.layer_sizes[0]], name='deep_input')

        for i, lay in enumerate(self.dif_shape_mlp):
            input_layer, wide_model = self.make_each_ds_block(lay, i)
            ds_models.append(wide_model)
            input_layers.append(input_layer)

        final_input_layer = self.make_model_input_layer(deep_layer_input, input_layers)

        remider_mlp = self.make_model_concatinator_layer(deep_layer_input, ds_models)

        for i, item in enumerate(self.layer_sizes[:-1]):
            remider_mlp = Dense(item, activation='relu', kernel_regularizer=regu)(remider_mlp)
            remider_mlp = tf.keras.layers.Dropout(self.drop_out, name=f'deep_drop{i}')(remider_mlp)
        
        # self.add_last_layer_activation(remider_mlp)
        remider_mlp = Dense(self.layer_sizes[-1], activation='softmax')(remider_mlp)

        model = tf.keras.Model(inputs=final_input_layer, outputs=remider_mlp)
        model.compile(loss=self.loss, optimizer=self.opt, metrics=['accuracy'])
        print(model.summary()) if self.verbose else None
        self.model = model
        return model
