import numpy as np


class MultiFutureIndexSelector:
    def __init__(self) -> None:
        pass

    @staticmethod
    def get_index1(data_len, num_of_future):
        wanted_powers = np.zeros([data_len//num_of_future, num_of_future], dtype=np.int32)
        wanted_powers[:num_of_future, 0] = np.arange(num_of_future)
        wanted_powers[num_of_future:, 0] = np.arange(2*num_of_future-1, data_len,
                                                          num_of_future)[:-num_of_future + 1]
        for i in range(1, num_of_future):
            wanted_powers[:, i] = wanted_powers[:, i-1] + num_of_future - 1
        for i in range(num_of_future - 1):
            wanted_powers[i, i+1:] = wanted_powers[i, i]
        return wanted_powers

    @staticmethod
    def get_index(data_len, num_of_future):
        al = np.arange(data_len - num_of_future, -1, -num_of_future)[::-1]
        wanted_powers = np.zeros([len(al), num_of_future], dtype=np.int32)
        wanted_powers[:, 0] = al
        for i in range(1, num_of_future):
            wanted_powers[:, i] = wanted_powers[:, i-1] - (num_of_future - 1)
        for i in range(len(wanted_powers)):
            has_under_zero = False
            for j in range(1, num_of_future):
                if wanted_powers[i, j] < 0:
                    wanted_powers[i, j] = wanted_powers[i, j-1]
                    has_under_zero = True
            if not has_under_zero:
                break
        wanted_powers = wanted_powers[:, ::-1]
        return wanted_powers

