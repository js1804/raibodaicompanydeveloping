from .PostProcessorsBase import PostProcessorsBase
from .MultiFutureIndexSelector import MultiFutureIndexSelector

import numpy as np

class MultiFuturePostProcessor(PostProcessorsBase):
    def __init__(self, model, num_of_future, window_size, **kwargs):
        super().__init__(model, **kwargs)
        self.num_of_future = num_of_future
        self.window_size = window_size

    def predict(self, x_in):
        power = self.model.model.predict(x_in)
        wanted_powers = MultiFutureIndexSelector.get_index(len(x_in['deep_input']), self.num_of_future)
        res = np.sum(power[wanted_powers], axis=1)
        # res = np.argmax(res, axis=1)
        return res
        