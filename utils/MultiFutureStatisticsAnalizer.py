from .MultiFutureIndexSelector import MultiFutureIndexSelector

import numpy as np
import itertools


class MultiFutureStatisticsAnalizer:
    def __init__(self) -> None:
        pass

    @staticmethod
    def calculate_one_state_prf(preds_power, label):
        num_of_label = preds_power.shape[1]
        precisions, recalls, counts, f1s = [], [], [], []
        for i in range(num_of_label):
            preds = np.argmax(preds_power, axis=1)
            real_now = preds[label == i]
            now_recal = real_now[real_now == i].shape[0] / (len(real_now) + .00001)
            now_precision = preds[np.logical_and(preds == i, label == i)].shape[0] / (
                            preds[preds == i].shape[0] + .00001)

            precisions.append(now_precision)
            recalls.append(now_recal)
            counts.append(len(real_now))
            f1s.append(2*now_precision*now_recal / (now_recal + now_precision + .00001))
        return [precisions, recalls, counts, f1s]

    @staticmethod
    def analize(preds_power, y, num_of_future):
        relative_idx = MultiFutureIndexSelector.get_index(len(preds_power), num_of_future)
        indices = list(itertools.product([0, 1], repeat=num_of_future))[1:]
        indices = [[i for i,item in enumerate(jtem) if item==1] for jtem in indices]
        res = {}
        for item in indices:
            power = np.sum(preds_power[relative_idx[:, item]], axis=1)
            res[str(item)] = MultiFutureStatisticsAnalizer.calculate_one_state_prf(power, y[::num_of_future])
        print(res)
        return res
