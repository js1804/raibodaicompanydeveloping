import copy
import numpy as np


class MultiLevelUpDownMaker():
    def __init__(self, number_of_levels=5, intervals=.5) -> None:
        self.number_of_levels = number_of_levels
        self.intervals = intervals
    
    def transform(self, x, purpose_col_name='close', cols=None):
        y_col = copy.deepcopy(x[:, cols.index(purpose_col_name)])
        pivots = [i*self.intervals - self.number_of_levels/2*self.intervals + self.intervals for i in range(self.number_of_levels-1)]

        indices = []

        indices.append(y_col < pivots[0])
        for level in range(1, len(pivots)):
            indices.append(np.logical_and(y_col > pivots[level-1], y_col <= pivots[level]))
        indices.append(y_col > pivots[-1])

        for i, item in enumerate(indices):
            y_col[item] = i

        y_col = np.array(y_col, dtype=np.int32)

        if purpose_col_name != 'purpose':
            x = np.append(x, y_col.reshape(-1,1), axis=1)
            cols.append('purpose')
        else:
            x[:, cols.index(purpose_col_name)] = y_col

        return x

    def __call__(self, x, purpose_col_name='close', cols_name=None):
        return self.transform(x, purpose_col_name, cols_name)
