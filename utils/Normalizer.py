import numpy as np
import pandas as pd


class Normalizer:
    def __init__(self, normalizing_window=1000):
        self.normalizing_window = normalizing_window

    def zigzag_normalizer(self, data):
        tmp = data.abs().rolling(self.normalizing_window).max()
        data /= np.array(tmp.fillna(method='bfill'))
        return data

    def transform(self, data: pd.DataFrame):
        zigzag_column_names = list(filter(lambda x: 'zigzag' in x, data.columns))
        data[zigzag_column_names] = self.zigzag_normalizer(data[zigzag_column_names])
        return data