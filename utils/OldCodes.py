class zigzag:
    @staticmethod
    def make_list_of_vars_old(waves):
        vars_5, vars_4, vars_3, vars_2, vars_1 = [], [], [], [], []
        vars_5.append(waves[5] - waves[4])
        vars_5.append(waves[5] - waves[3])
        vars_5.append(waves[5] - waves[2])
        vars_5.append(waves[5] - waves[1])
        vars_5.append(waves[5] - waves[0])
        vars_4.append(waves[4] - waves[3])
        vars_4.append(waves[4] - waves[2])
        vars_4.append(waves[4] - waves[1])
        vars_4.append(waves[4] - waves[0])
        vars_3.append(waves[3] - waves[2])
        vars_3.append(waves[3] - waves[1])
        vars_3.append(waves[3] - waves[0])
        vars_2.append(waves[2] - waves[1])
        vars_2.append(waves[2] - waves[0])
        vars_1.append(waves[1] - waves[0])

        return [vars_5, vars_4, vars_3, vars_2, vars_1]


    @staticmethod
    def make_list_of_names_old(prefix, postfix=0):
        vars_5, vars_4, vars_3, vars_2, vars_1 = [], [], [], [], []
        vars_5.append('waves_5__4')
        vars_5.append('waves_5__3')
        vars_5.append('waves_5__2')
        vars_5.append('waves_5__1')
        vars_5.append('waves_5__0')
        vars_4.append('waves_4__3')
        vars_4.append('waves_4__2')
        vars_4.append('waves_4__1')
        vars_4.append('waves_4__0')
        vars_3.append('waves_3__2')
        vars_3.append('waves_3__1')
        vars_3.append('waves_3__0')
        vars_2.append('waves_2__1')
        vars_2.append('waves_2__0')
        vars_1.append('waves_1__0')

        varss = [vars_5, vars_4, vars_3, vars_2, vars_1]
        res = []
        for i in range(5):
            for j in range(i + 1, 5):
                for item in varss[i]:
                    for jtem in varss[j]:
                        res.append(item + '_d_' + jtem)
        res.append('waves_5_d_waves_4')
        res = [prefix + '__' + item + f'_{postfix}' for item in res]
        return res


    @staticmethod
    def calc_price_ratios_old(ws):
        x = []
        varss = ZigZogAnalyzer.make_list_of_vars(ws)

        for i in range(5):
            for j in range(i + 1, 5):
                for item in varss[i]:
                    for jtem in varss[j]:
                        x.append(item / jtem)

        x.append(ws[5] / ws[4])

        return x


    @staticmethod
    def calc_time_ratios_old(ts):
        x = []
        varss = ZigZogAnalyzer.make_list_of_vars(ts)
        for i in range(5):
            for j in range(i + 1, 5):
                for item in varss[i]:
                    for jtem in varss[j]:
                        x.append(item / jtem)

        x.append(ts[5] / ts[4])

        return x


    @staticmethod
    def calc_pace_ratios(ws, ts):
        x = []
        w_5__w_4 = ws[5] - ws[4]
        w_4__w_3 = ws[4] - ws[3]
        w_3__w_2 = ws[3] - ws[2]
        w_2__w_1 = ws[2] - ws[1]
        w_1__w_0 = ws[1] - ws[0]

        t_5__t_4 = ts[5] - ts[4]
        t_4__t_3 = ts[4] - ts[3]
        t_3__t_2 = ts[3] - ts[2]
        t_2__t_1 = ts[2] - ts[1]
        t_1__t_0 = ts[1] - ts[0]

        x.append(((w_5__w_4) / (w_4__w_3)) / ((t_5__t_4) / (t_4__t_3)))
        x.append(((w_4__w_3) / (w_3__w_2)) / ((t_4__t_3) / (t_3__t_2)))
        x.append(((w_3__w_2) / (w_1__w_0)) / ((t_3__t_2) / (t_1__t_0)))
        x.append(((w_2__w_1) / (w_1__w_0)) / ((t_2__t_1) / (t_1__t_0)))
        x.append(((w_5__w_4) / (w_3__w_2)) / ((t_5__t_4) / (t_3__t_2)))
        x.append(((w_4__w_3) / (w_2__w_1)) / ((t_4__t_3) / (t_2__t_1)))
        x.append(((w_3__w_2) / (w_2__w_1)) / ((t_3__t_2) / (t_2__t_1)))
        x.append(((w_5__w_4) / (w_1__w_0)) / ((t_5__t_4) / (t_1__t_0)))
        x.append((ws[5] / ws[4]) / t_5__t_4)
        return x

    def create_filter_on_post_bucket(self):
        columns = list(filter(lambda x: 'zigzag_price' in x, self.final_dataframe.columns))
        # ComputeHistData().compute(self.dataframe[columns].values, 'price', columns, self.zigzag_analizer.final_pivots,
        #                           'proved_pivot')
        # ComputeHistData().compute(self.dataframe[columns].values, 'price', columns,
        #                           np.setdiff1d(self.zigzag_analizer.list_of_true_ratios_idxs,
        #                                        self.zigzag_analizer.final_pivots),
        #                           'not_proved_pivot')
        # StructureBaseComputeHistData().compute(self.final_dataframe[columns].values, 'price', columns,
        #                                        self.zigzag_analizer.list_of_tss, self.close_.values,
        #                                        FilterOnHistData.both_base_pair_sorter,
        #                                        self.zigzag_analizer.final_pivots,
        #                                        5,
        #                                        'proved_pivot')
        # StructureBaseComputeHistData().compute(self.final_dataframe[columns].values, 'price', columns,
        #                                        self.zigzag_analizer.list_of_tss, self.close_.values,
        #                                        FilterOnHistData.both_base_pair_sorter,
        #                                        np.setdiff1d(self.zigzag_analizer.list_of_true_ratios_idxs,
        #                                                     self.zigzag_analizer.final_pivots),
        #                                        4,
        #                                        'not_proved_pivot')
        # exit()
        pivot_filters = []
        for i, item in enumerate(self.bucket_base_check_list):
            print(len(self.zigzag_analizer.final_pivots))
            pivot_only_filterer = FilterOnHistData()
            pivot_filter = pivot_only_filterer.compute(self.dataframe[columns].values, 'price', [item],
                                                       self.zigzag_analizer.list_of_true_ratios_idxs if item==4 else None,
                                                       file_name_post_fix='proved_pivot')
            # second_pivot_only_filterer = FilterOnHistData()
            # second_pivot_filter = second_pivot_only_filterer.compute(self.final_dataframe[columns].values, 'price', [item],
            #                                                          self.zigzag_analizer.list_of_true_ratios_idxs if item == 4 else None,
            #                                                          file_name_post_fix='not_proved_pivot')
            #
            # StatisticsAnalysis.dual_w4_predicts(self.zigzag_analizer.list_of_tss,
            #                                     np.where(self.final_pivots != 0)[0],
            #                                     self.zigzag_analizer.first_reversed_point,
            #                                     pivot_filter,
            #                                     second_pivot_filter)
            # pivot_filter = StructureBaseFilterOnHistData().compute(self.final_dataframe[columns].values,
            #                                                        self.zigzag_analizer.list_of_tss,
            #                                                        self.close_.values,
            #                                                        FilterOnHistData.both_base_pair_sorter,
            #                                                        'price', [item],
            #                                                        self.zigzag_analizer.list_of_true_ratios_idxs
            #                                                                     if item == 4 else None,
            #                                                        file_name_post_fix='proved_pivot')
            # with open('existance_check.pkl', 'rb') as f:
            #     pivot_filter = pickle.load(f)
            # FilterOnHistData.structure_base_analysis(self.close_, np.where(self.final_pivots!=0)[0], pivot_filter,
            #                                          self.zigzag_analizer.first_reversed_point,
            #                                          sorter=FilterOnHistData.structure_base_pair_sorter)
            # FilterOnHistData.calculate_miss_acc_w4_base(self.zigzag_analizer.list_of_tss,
            #                                             np.where(self.final_pivots != 0)[0],
            #                                             self.zigzag_analizer.first_reversed_point, pivot_filter)
            #
            # FilterOnHistData.structure_base_analysis_candle_base(self.close_, self.zigzag_analizer.list_of_tss,
            #                                                      pivot_filter, np.where(self.final_pivots != 0)[0],
            #                                                      sorter=FilterOnHistData.both_base_pair_sorter)
            StatisticsAnalysis.overall_statistic_calculation(np.where(self.final_pivots != 0)[0], pivot_filter,
                                                             first_reversed_point=self.zigzag_analizer.first_reversed_point,
                                                             all_pivots=self.zigzag_analizer.list_of_tss)
            StatisticsAnalysis.plot(self.close_, np.where(self.final_pivots != 0)[0], pivot_filter,
                                    self.zigzag_analizer.first_reversed_point)
            pivot_filters.append(pivot_filter.astype(np.bool))
            print(len(self.zigzag_analizer.list_of_true_ratios_idxs))
        exit()
        return pivot_filters

    def predict(self, x_in):
        print('in predict')
        self.reset()
        preds = np.argmax(self.model.predict(x_in), axis=1)        
        res = []

        stop_loss = 1300
        direction = 0
        sl_direction = direction
        in_trade = False
        list_of_sl = []
        last_buy_sell_decition = 0

        for i, item in enumerate(preds):
            direction = self.candidate_pivots[i] if self.candidate_pivots[i] != 0 else direction
            close_flag = False
            if in_trade and self.hit_stop_loss(self.closes[i], sl_direction, stop_loss):
                # res.append(CLOSE_POSITION)
                # in_trade = False
                # self.filterer.send(i)
                close_flag = True
            
            if self.filterer.send(i):   
                if item == IS_REVERSE:
                    sl_direction = -direction
                    stop_loss = self.sl_calculator(self.closes, direction, 0, self.all_pivots[i])
                    in_trade = True

                elif item == NO_REVERSE:
                    sl_direction = direction
                    stop_loss = self.sl_calculator(self.closes, direction, ZIGZAG_UP_THRESHOLD, self.all_pivots[i])
                    in_trade = True
                dec = self.make_decision(direction, item)
                if close_flag:
                    res.append(CLOSE_POSITION if dec == last_buy_sell_decition  else dec)
                    if res[-1] == CLOSE_POSITION:
                        in_trade = False
                    close_flag = False
                else:
                    res.append(dec)
                last_buy_sell_decition = dec if dec != DO_NOTHING_POSITION else last_buy_sell_decition
            elif close_flag:
                res.append(CLOSE_POSITION)
                in_trade = False
            else:
                res.append(DO_NOTHING_POSITION)
                preds[i] = 4

            list_of_sl.append(stop_loss)

        res = np.array(res)

        plt.scatter(np.where(preds==0)[0], self.closes[np.where(preds==0)[0]], color='orange', marker='*')
        plt.scatter(np.where(preds==1)[0], self.closes[np.where(preds==1)[0]], color='black', marker='*')
        plt.scatter(np.where(preds==2)[0], self.closes[np.where(preds==2)[0]], color='red', marker='*')
        plt.scatter(np.where(preds==4)[0], self.closes[np.where(preds==4)[0]], color='green', marker='*')

        plt.scatter(self.first_reverse_point, self.closes[self.first_reverse_point]-2, color='purple', marker='v')
        plt.plot(self.closes)
        plt.plot(list_of_sl)
        # plt.show()
        return res