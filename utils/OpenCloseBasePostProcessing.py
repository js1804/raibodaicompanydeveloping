from .GlobalVariable import OPEN_POSITION, DO_NOTHING_POSITION, CLOSE_POSITION, BUY_POSITION, SELL_POSITION
from .PostProcessorsBase import PostProcessorsBase

import numpy as np
from sklearn.preprocessing import OneHotEncoder


class OpenCloseBasePostProcessing(PostProcessorsBase):
    def __init__(self, model, candidate_pivots, open_position=OPEN_POSITION, do_nothing_position=DO_NOTHING_POSITION,
                 close_position=CLOSE_POSITION, buy_position=BUY_POSITION, sell_position=SELL_POSITION):
        super().__init__(model)
        self.open_position = open_position
        self.do_nothing_position = do_nothing_position
        self.close_position = close_position
        self.candidate_pivots = candidate_pivots
        self.buy_position = buy_position
        self.sell_position = sell_position

    @staticmethod
    def get_last_previous_pivot(pivots):
        for item in pivots[::-1]:
            if item != 0:
                return item
        raise BaseException('pivot not found')

    def predict(self, x_in):
        pred = np.argmax(self.model.predict(x_in), axis=1)

        res = []
        for i, item in enumerate(pred):
            if item == self.open_position:
                res.append(OpenCloseBasePostProcessing.get_last_previous_pivot(self.candidate_pivots[:i+1]))
                res[-1] = self.buy_position if res[-1] == -1 else self.sell_position
            elif item == self.do_nothing_position:
                res.append(DO_NOTHING_POSITION)
            elif item == self.close_position:
                res.append(CLOSE_POSITION)
            else:
                res.append(item)
        res = np.concatenate([np.array([i for i in range(5)]), np.array(res)], axis=0).reshape(-1, 1)
        res = OneHotEncoder(sparse=False).fit_transform(res)[5:]
        return res
