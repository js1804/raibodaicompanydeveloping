from .GlobalVariable import OPEN_POSITION,ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD, BUY_POSITION, SELL_POSITION, DO_NOTHING_POSITION, CLOSE_POSITION
from .my_zigzag import peak_valley_pivots

import numpy as np


class OpenCloseBasePurposeMaker1D:
    def __init__(self, multiplier=1, base='price', zigzag_up_thresh=ZIGZAG_UP_THRESHOLD,
                        zigzag_down_thresh=ZIGZAG_DOWN_THRESHOLD, open_thresh=.5, close_thresh=.5,
                        open_dnt_close_mode=True):
        self.zigzag_up_thresh = zigzag_up_thresh
        self.zigzag_down_thresh = zigzag_down_thresh

        self.multiplier = 1/multiplier
        self.open_thresh = open_thresh
        self.close_thresh = close_thresh
        self.base = base
        self.open_dnt_close_mode = open_dnt_close_mode

    def get_data(self, close, pivots, pivot):
        if self.base == 'price':
            price_st = close[pivots[pivot]]
            price_end = close[pivots[pivot + 1]]
            yield price_st, price_end, abs(price_end - price_st)
        else:
            yield pivots[pivot], pivots[pivot+1], pivots[pivot + 1] - pivots[pivot]

    def choose_open_label(self, pivots, pivot, pivots_positions):
        if self.open_dnt_close_mode:
            return OPEN_POSITION
        else:
            if pivots[pivots_positions[pivot]] == -1:
                return (BUY_POSITION)
            else:
                return (SELL_POSITION)

    @staticmethod
    def seperate_price_time(mode, cur_pos, data):
        if mode=='price':
            return data[cur_pos]
        return cur_pos

    def choose_label(self, i, close, st, end, length, pivots, pivots_positions, pivot):
        cur = OpenCloseBasePurposeMaker1D.seperate_price_time(self.base, i, close)
        if self.calculate_and_check_on_thresh(cur, st, length, self.open_thresh,
                                              0, self.multiplier):
            return self.choose_open_label(pivots, pivot, pivots_positions)
        elif self.calculate_and_check_on_thresh(cur, end, length, self.close_thresh,
                                                0, self.multiplier):
            return (CLOSE_POSITION)
        else:
            return (DO_NOTHING_POSITION)

    def transform(self, x, close, cols):
        pivots = peak_valley_pivots(close, self.zigzag_up_thresh, self.zigzag_down_thresh)
        pivots_positions = np.where(pivots != 0)[0]
        pivots_positions = np.concatenate([pivots_positions, [len(pivots)-1]], axis=0)
        res = []

        for pivot in range(len(pivots_positions) - 1):
            gen = self.get_data(close, pivots_positions, pivot)
            st, end, length = next(gen)
            for i in range(pivots_positions[pivot], pivots_positions[pivot+1]):
                res.append(self.choose_label(i, close, st, end, length, pivots, pivots_positions, pivot))
        res.append(self.choose_label(len(pivots)-1, close, st, end, length, pivots, pivots_positions, pivot))
        res = np.array(res).reshape(-1, 1)

        if 'purpose' not in cols:
            x = np.append(x, res, axis=1)
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = res
        return x

    @staticmethod
    def calculate_and_check_on_thresh(cur_price, base_price, price_length, thresh, mx, sx):
        price = (cur_price - base_price) / price_length
        val = OpenCloseBasePurposeMaker1D.gaus1d(price, mx, sx)
        if val >= thresh:
            return True
        return False

    @staticmethod
    def gaus1d(x, mx=0, sx=1):
        return 1. / (np.sqrt(2. * np.pi)* sx ) * np.exp(-1/2 * (((x - mx) /(sx)) **2))
