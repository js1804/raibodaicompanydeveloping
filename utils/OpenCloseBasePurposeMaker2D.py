from .GlobalVariable import OPEN_POSITION, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD, BUY_POSITION, SELL_POSITION, DO_NOTHING_POSITION, CLOSE_POSITION
from .my_zigzag import peak_valley_pivots

import numpy as np


class OpenCloseBasePurposeMaker2D:
    def __init__(self, time_sigma_multiplier=1, price_sigma_multiplier=1, zigzag_up_thresh=ZIGZAG_UP_THRESHOLD,
                        zigzag_down_thresh=ZIGZAG_DOWN_THRESHOLD, open_thresh=.5, close_thresh=.5,
                        open_dnt_close_mode=True):
        self.zigzag_up_thresh = zigzag_up_thresh
        self.zigzag_down_thresh = zigzag_down_thresh

        self.time_sigma_multiplier = 1/time_sigma_multiplier
        self.price_sigma_multiplier = 1/price_sigma_multiplier
        self.open_thresh = open_thresh
        self.close_thresh = close_thresh
        self.open_dnt_close_mode = open_dnt_close_mode

    def choose_open_label(self, pivots, pivot, pivots_positions):
        if self.open_dnt_close_mode:
            return OPEN_POSITION
        else:
            if pivots[pivots_positions[pivot]] == -1:
                return (BUY_POSITION)
            else:
                return (SELL_POSITION)

    def choose_label(self, i, close, price_st, price_end, price_length, pivot, pivots, time_length, pivots_positions):
        if self.calculate_and_check_on_thresh(close[i], i, price_st, pivots_positions[pivot],
                                              price_length, time_length, self.open_thresh,
                                              0, 0, self.price_sigma_multiplier, self.time_sigma_multiplier):
            return self.choose_open_label(pivots, pivot, pivots_positions)

        elif self.calculate_and_check_on_thresh(close[i], i, price_end, pivots_positions[pivot + 1],
                                                price_length, time_length, self.close_thresh,
                                                0, 0, self.price_sigma_multiplier, self.time_sigma_multiplier):
            return CLOSE_POSITION
        else:
            return DO_NOTHING_POSITION

    def transform(self, x, close, cols):
        pivots = peak_valley_pivots(close, self.zigzag_up_thresh, self.zigzag_down_thresh)
        pivots_positions = np.where(pivots != 0)[0]
        pivots_positions = np.concatenate([pivots_positions, [len(pivots)-1]], axis=0)
        res = []

        for pivot in range(len(pivots_positions) - 1):
            price_st = close[pivots_positions[pivot]]
            price_end = close[pivots_positions[pivot+1]]
            time_length = pivots_positions[pivot+1] - pivots_positions[pivot]
            price_length = abs(price_end-price_st)

            for i in range(pivots_positions[pivot], pivots_positions[pivot+1]):
                res.append(self.choose_label(i, close, price_st, price_end, price_length,
                                             pivot, pivots, time_length, pivots_positions))

        res.append(self.choose_label(len(pivots)-1, close, price_st, price_end, price_length,
                                                pivot, pivots, time_length, pivots_positions))
        res = np.array(res).reshape(-1, 1)

        if 'purpose' not in cols:
            x = np.append(x, res, axis=1)
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = res
        return x

    @staticmethod
    def calculate_and_check_on_thresh(cur_price, cur_time, base_price, base_time,
                                      price_length, time_length, thresh, mx, my, sx, sy):
        price = abs(cur_price - base_price) / price_length
        time = abs(cur_time - base_time) / time_length
        val = OpenCloseBasePurposeMaker2D.gaus2d(price, time, mx, my, sx, sy)
        if val >= thresh:
            return True
        return False

    @staticmethod
    def gaus2d(x, y, mx=0, my=0, sx=1, sy=1):
        return 1. / (2. * np.pi * sx * sy) * np.exp(-((x - mx)**2. / (2. * sx**2.) + (y - my)**2. / (2. * sy**2.)))
