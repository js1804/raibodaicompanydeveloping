from .GlobalVariable import TIMER_FOR_SINGLE_RUN, TIMER_FOR_MULTIPLE_RUN
from .Runners import run_model
from .WatchDogTimer import Watchdog
from .FixVram import fix_gpu_memory

import multiprocessing
import tensorflow as tf

fix_gpu_memory()


class OneModelParallelMultipleRun:
    def __init__(self, model_maker, handler, train_test_maker_name, number_of_run, show_figs, save_results, zipper,
                 per_card, number_of_card, sort_output=False, just_best=False, only_trade=False, **handler_kwargs):
        self.number_of_run = number_of_run
        self.model_maker = model_maker
        self.handler = handler
        self.show_figs = show_figs
        self.save_results = save_results
        self.zipper = zipper
        self.number_of_card = number_of_card
        self.per_card = per_card
        self.sort_output = sort_output
        self.handler_kwargs = handler_kwargs
        self.just_best = just_best
        self.train_test_maker_name = train_test_maker_name
        self.only_trade = only_trade

    def start(self):
        res = OneModelParallelMultipleRun.one_model_multiple_run(self.handler, self.train_test_maker_name,
                                                                 self.number_of_run, self.model_maker,
                                                                 self.zipper, self.show_figs, self.save_results,
                                                                 self.number_of_card, self.per_card,
                                                                 **self.handler_kwargs)
        print('finished')
        if self.sort_output:
            res = OneModelParallelMultipleRun.sort_output_res(res)
            return res[-1] if self.just_best else res
        else:
            return res

    @staticmethod
    def sort_output_res(res):
        return sorted(res, key=lambda x: max([item['netProfit'] for item in x]))

    @staticmethod
    def input_wrapper(kwargs):
        return kwargs[0](**kwargs[1])

    @staticmethod
    def one_model_one_run(handler, train_test_maker_name, model_maker, zipper, graphic_card_num, show_figs,
                          save_results, **model_maker_kwargs):
        print(f'graphic_card_num:{graphic_card_num}')
        watch_dog_timer = Watchdog(TIMER_FOR_SINGLE_RUN)
        try:
            with tf.device(f'/gpu:{graphic_card_num}'):
                model = model_maker(handler, run=False)
                analizer_obj = run_model(model, getattr(handler, train_test_maker_name), handle=handler, show_figs=show_figs,
                                         zipper=zipper, save_results=save_results, **model_maker_kwargs)
                watch_dog_timer.stop()
                del analizer_obj.model
                return analizer_obj.trade_info
        except Watchdog:
            print('time out in one model run')
            return OneModelParallelMultipleRun.one_model_one_run(handler, train_test_maker_name, model_maker, zipper,
                                                                 graphic_card_num, show_figs, save_results,
                                                                 **model_maker_kwargs)

    @staticmethod
    def one_model_multiple_run(handler, train_test_maker_name, number_of_run, model_maker, zipper, show_figs,
                               save_results, number_of_graphic_card, number_of_run_per_card, **model_maker_kwargs):
        res = []
        base_input = {'handler': handler,
                      'model_maker': model_maker,
                      'zipper': zipper,
                      'show_figs': show_figs,
                      'save_results': save_results,
                      'train_test_maker_name': train_test_maker_name}
        base_input.update(model_maker_kwargs)

        all_input = [[OneModelParallelMultipleRun.one_model_one_run,
                      OneModelParallelMultipleRun.copy_and_update_dict(
                          base_input, graphic_card_num=i % number_of_graphic_card)
                      ]
                     for i in range(number_of_run)]
        thread_count = number_of_run_per_card * number_of_graphic_card
        watch_dog_timer = Watchdog(TIMER_FOR_MULTIPLE_RUN)
        try:
            with multiprocessing.Pool(thread_count) as pool:
                for i in range(thread_count, number_of_run + 1, thread_count):
                    tmp = pool.map(OneModelParallelMultipleRun.input_wrapper, all_input[i - thread_count:i])
                    res.extend(tmp)
                watch_dog_timer.stop()
        except Watchdog:
            print('time out in multiple run')
            return OneModelParallelMultipleRun.one_model_multiple_run(handler, train_test_maker_name, number_of_run,
                                                                      model_maker, zipper, show_figs, save_results,
                                                                      number_of_graphic_card, number_of_run_per_card,
                                                                      **model_maker_kwargs)
        return res

    @staticmethod
    def copy_and_update_dict(dic, **kwargs):
        ndic = {}
        ndic.update(dic)
        ndic.update(kwargs)
        return ndic
