from .DataHandler import DataHandler
from .Runners import run_lstmDS_model, run_lstm_model
import matplotlib.pyplot as plt
import numpy as np


class PlotThreshold:
    MAX_THRESHOLD = 90

    def __init__(self, handle: DataHandler, couples, model_path) -> None:
        self.handle = handle
        self.couples = couples
        self.path = model_path

        model = run_lstm_model(self.handle, False)
        model.load_model(self.path)
        # x, y, x_t, y_t = self.handle.make_train_test_data_dictionary(self.couples, train_test_spl=False)
        x, y, x_t, y_t = self.handle.make_train_test_data(is_for_mlp=False)
        model.set_data(x, y, x_t, y_t)
        model.prepare_test_prediction_data()
        self.prediction = model.predict(x_t)
        self.y_ture = model.y_true

    def threshold_plot(self):
        prediction = self.prediction

        prediction_coverage_ = []
        res_ = []
        for i in range(50, self.MAX_THRESHOLD):
            cover = 0
            res_.append([])
            for class_ in range(prediction.shape[1]):
                print(f'threshold: {i/100}')
                idx_cutter = prediction[:, class_] > i/100

                if len(prediction[idx_cutter]):
                    acc_ = len(prediction[idx_cutter][self.y_ture[idx_cutter] == class_]) / len(prediction[idx_cutter])
                    predicted_ = len(prediction[idx_cutter])
                    print(f'down prediction acc: {acc_}, number of prediction: {predicted_}')
                    res_[-1].append([acc_, predicted_])
                else:
                    res_[-1].append([0, 0])

                cover += len(prediction[idx_cutter])
            prediction_coverage_.append(cover/len(self.y_ture))
            print('*'*30, '\n')
        
        res_ = np.array(res_)
        prediction_coverage_ = np.array(prediction_coverage_)
        fig = plt.figure()
        ax = fig.gca()
        ax.set_xticks(np.arange(50, 101, 2.5))
        ax.set_yticks(np.arange(0, 101, 5))
        for class_ in range(prediction.shape[1]):
            plt.plot(range(50, self.MAX_THRESHOLD), res_[:, class_, 0]*100,)
        plt.plot(range(50, self.MAX_THRESHOLD), prediction_coverage_*100)
        plt.grid()
            
        plt.legend([f'class_{clss}_acc' for clss in range(prediction.shape[1])] + ['coverage'], loc='lower left')
        plt.xlabel('threshold')
        plt.ylabel('percentage')
        plt.title('threshold')
        fig.canvas.manager.set_window_title('threshold_acc')
        manager = plt.get_current_fig_manager()
        manager.full_screen_toggle()
        plt.savefig('threshold_acc.jpg')

    def bucket_threshold_plot(self):
        prediction = self.prediction

        prediction_coverage_ = []
        res_ = []

        for i in range(50, self.MAX_THRESHOLD):
            cover = 0
            res_.append([])
            for class_ in range(prediction.shape[1]):
                print(f'threshold: {i/100}')
                idx_cutter = np.logical_and(prediction[:, class_] > i/100, prediction[:, class_] < (i+1)/100)

                if len(prediction[idx_cutter]):
                    acc_ = len(prediction[idx_cutter][self.y_ture[idx_cutter] == class_]) / len(prediction[idx_cutter])
                    predicted_ = len(prediction[idx_cutter])
                    print(f'down prediction acc: {acc_}, number of prediction: {predicted_}')
                    res_[-1].append([acc_, predicted_])
                else:
                    res_[-1].append([0, 0])

                cover += len(prediction[idx_cutter])
            prediction_coverage_.append(cover/len(self.y_ture))

        res_ = np.array(res_)
        prediction_coverage_ = np.array(prediction_coverage_)
        fig = plt.figure()
        ax = fig.gca()
        ax.set_xticks(np.arange(50, 101, 1))
        ax.set_yticks(np.arange(0, 101, 5))
        for class_ in range(prediction.shape[1]):
            plt.plot(range(50, self.MAX_THRESHOLD), res_[:, class_, 0]*100,)
        plt.plot(range(50, self.MAX_THRESHOLD), prediction_coverage_*100)
        plt.grid()
        plt.legend([f'class_{clss}_acc' for clss in range(prediction.shape[1])] + ['coverage'], loc='lower left')

        plt.xlabel('threshold')
        plt.ylabel('percentage')
        plt.title('1 percent width bucket threshold acc')
        fig.canvas.manager.set_window_title('bucket_acc')
        manager = plt.get_current_fig_manager()
        manager.full_screen_toggle()
        plt.savefig('bucket_acc.jpg')
        # plt.show()

        fig = plt.figure()
        ax = fig.gca()
        ax.set_xticks(np.arange(50, 101, 1))
        ax.set_yticks(np.arange(0, np.max(res_[:,:,1]), 20))
        for class_ in range(prediction.shape[1]):
            plt.plot(range(50, self.MAX_THRESHOLD), res_[:, class_, 1]*100)
        plt.plot(range(50, self.MAX_THRESHOLD), np.cumsum(prediction_coverage_)*100)
        plt.grid()
            
        plt.legend([f'class_{clss}_predicted' for clss in range(prediction.shape[1])] + ['cumulative coverage'], loc='upper left')

        plt.xlabel('threshold')
        plt.ylabel('count')
        plt.title('bucket number of predictions')
        fig.canvas.manager.set_window_title('bucket_counts')
        manager = plt.get_current_fig_manager()
        manager.full_screen_toggle()
        plt.savefig('bucket_counts.jpg')
        # plt.show()
