class PostProcessorsBase:
    def __init__(self, model, **kwargs):
        self.model = model
        all_attr = set([attribute for attribute in dir(self.model) if not attribute.startswith('__')])
        my_model_attr = set([attribute for attribute in dir(self.model.model) if not attribute.startswith('__')])
        attributes = list(all_attr.difference(my_model_attr))
        current_attr = dir(self)
        for item in attributes:
            if item in current_attr:
                continue
            setattr(self, item, getattr(self.model, item))
