import numpy as np


class PurposeMakerBase:
    @staticmethod
    def append_label_to_data(x, cols, labels):
        if 'purpose' not in cols:
            x = np.append(x, labels, axis=1)
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = labels
        return x

    def transform(self, x, cols, close):
        pass