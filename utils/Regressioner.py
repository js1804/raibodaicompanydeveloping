import numpy as np

class Regressioner:
    def __init__(self) -> None:
        pass

    def transform(self, x, purpose_col_name='purpose', cols=None):
        if purpose_col_name != 'purpose':
            x = np.append(x, x[:, cols.index(purpose_col_name)].reshape(-1,1), axis=1)
            cols.append('purpose')
        return x
        
    def __call__(self, x, purpose_col_name, cols):
        return self.transform(x, purpose_col_name, cols)