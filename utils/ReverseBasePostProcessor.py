from .PostProcessorsBase import PostProcessorsBase
from .ReverseBasePurposeMaker import IS_REVERSE, NO_REVERSE, NO_DECISION_CODE
from .GlobalVariable import BUY_POSITION, SELL_POSITION, DO_NOTHING_POSITION, CLOSE_POSITION, \
                            ZIGZAG_UP_THRESHOLD, REVERSE_BASE_POST_PROCESSOR_MODE
from .ZigzagSubwaveSeperator import ZigzagSubwaveSeperator

import numpy as np
import matplotlib.pyplot as plt


class ReverseBasePostProcessor(PostProcessorsBase):
    def __init__(self, model, candidate_pivots, closes, all_pivots, first_reverse_point, 
                 test_indices, 
                 mode=REVERSE_BASE_POST_PROCESSOR_MODE,**kwargs):
        super().__init__(model, **kwargs)
        self.candidate_pivots = np.array(candidate_pivots)
        self.closes = closes
        self.all_pivots = all_pivots
        self.first_reverse_point = first_reverse_point
        self.test_indices = test_indices
        self.current_section = -1
        self.cur_sec_length = 0

        self.mode = mode

    def reset(self):
        cur_sec_ind = self.test_indices[self.current_section]
        self.filterer = getattr(self, self.mode + '_filterer')(self.candidate_pivots[cur_sec_ind[0]: cur_sec_ind[0] + cur_sec_ind[2]])
        next(self.filterer)
        self.sl_calculator = getattr(self, self.mode + '_set_stop_loss')
        

    def predict(self, x_in):
        print('in predict')
        self.current_section += 1
        self.reset()
        preds = np.argmax(self.model.predict(x_in), axis=1)        
        res = []

        stop_loss = 1300
        direction = 0
        sl_direction = direction
        in_trade = False
        list_of_sl = []

        for i, item in enumerate(preds):
            cur_candle_index = self.test_indices[self.current_section][0] + i
            direction = self.candidate_pivots[cur_candle_index] if self.candidate_pivots[cur_candle_index] != 0 else direction

            if in_trade and self.hit_stop_loss(self.closes[cur_candle_index], sl_direction, stop_loss):
                res.append(CLOSE_POSITION)
                in_trade = False
                preds[i] = 4 if not self.filterer.send(i) else preds[i]
            
            elif self.filterer.send(i):   
                if item == IS_REVERSE:
                    sl_direction = -direction
                    stop_loss = self.sl_calculator(self.closes, direction, 0, self.all_pivots[cur_candle_index])
                    in_trade = True

                elif item == NO_REVERSE:
                    sl_direction = direction
                    stop_loss = self.sl_calculator(self.closes, direction, ZIGZAG_UP_THRESHOLD, self.all_pivots[cur_candle_index])
                    in_trade = True

                res.append(self.make_decision(direction, item))
            else:
                res.append(DO_NOTHING_POSITION)
                preds[i] = 4

            list_of_sl.append(stop_loss)

        res = np.array(res)
        plt.scatter(np.where(preds==0)[0] + self.cur_sec_length, self.closes[np.where(preds==0)[0] + self.test_indices[self.current_section][0]], color='orange', marker='*')
        plt.scatter(np.where(preds==1)[0] + self.cur_sec_length, self.closes[np.where(preds==1)[0] + self.test_indices[self.current_section][0]], color='black', marker='*')
        plt.scatter(np.where(preds==2)[0] + self.cur_sec_length, self.closes[np.where(preds==2)[0] + self.test_indices[self.current_section][0]], color='red', marker='*')
        plt.scatter(np.where(preds==4)[0] + self.cur_sec_length, self.closes[np.where(preds==4)[0] + self.test_indices[self.current_section][0]], color='green', marker='*')

        # plt.scatter(self.first_reverse_point, self.closes[self.first_reverse_point]-2, color='purple', marker='v')
        x_li_range = np.arange(self.cur_sec_length, self.cur_sec_length + self.test_indices[self.current_section][2])
        plt.plot(x_li_range, self.closes[self.test_indices[self.current_section][0]: self.test_indices[self.current_section][0] + self.test_indices[self.current_section][2]], color='tab:blue')
        plt.plot(x_li_range, list_of_sl, color='orange')
        # plt.show()
        self.cur_sec_length += self.test_indices[self.current_section][2]

        if self.current_section == len(self.test_indices) - 1:
            self.current_section = -1
            self.cur_sec_length = 0
        return res

    @staticmethod
    def hit_stop_loss(close, direction, sl):
        if direction == 1:
            return True if close < sl else False
        elif direction == -1:
            return True if close > sl else False
        else:
            return False

    @staticmethod
    def w5_prime_set_stop_loss(close, direction, multiplier, pivot_list):
        return close[pivot_list[-2]]*((1+multiplier) if direction == -1 else 1-multiplier)
    
    @staticmethod
    def candidate_pivots_set_stop_loss(close, direction, multiplier, pivot_list):
        return close[pivot_list[-1]]*((1+multiplier) if direction == -1 else 1-multiplier)

    @staticmethod
    def make_decision(direction, pred):
        if pred == IS_REVERSE:
            if direction == 1:
                return SELL_POSITION
            elif direction == -1:
                return BUY_POSITION
            else:
                return DO_NOTHING_POSITION
            
        if pred == NO_REVERSE:
            if direction == 1:
                return BUY_POSITION
            elif direction == -1:
                return SELL_POSITION
            else:
                return DO_NOTHING_POSITION
            
        if pred == NO_DECISION_CODE:
            return DO_NOTHING_POSITION
        
    @staticmethod
    def w5_prime_filterer(candidate_pivots):
        w5_prime_positions = np.array(ZigzagSubwaveSeperator(None, np.where(candidate_pivots!=0)[0],
                                                    None, None).get_all_w5_prime_idx())
        last_w5_prime = 0
        i = 0

        while 1:
            if i >= w5_prime_positions[last_w5_prime][1]:
                last_w5_prime += 1 if last_w5_prime < len(w5_prime_positions) - 1 else 0
            if i >= w5_prime_positions[last_w5_prime][0] and i < w5_prime_positions[last_w5_prime][1]:
                i = (yield True)
            else:
                i = (yield False)
        
    @staticmethod
    def candidate_pivots_filterer(candidate_pivots):
        candid = np.where(candidate_pivots!=0)[0]
        last_candid = 0
        i = 0
        while 1:
            if i > candid[last_candid]:
                last_candid += 1 if last_candid < len(candid) - 1 else 0
            if i == candid[last_candid]:
                i = (yield True)
            else:
                i = (yield False)
