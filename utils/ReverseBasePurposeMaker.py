from .GlobalVariable import ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD
from .IterativeZigzag import IterativeZigzag
from .my_zigzag import peak_valley_pivots
from .ZigzagSubwaveSeperator import ZigzagSubwaveSeperator
from .PurposeMakerBase import PurposeMakerBase

import numpy as np

NO_DECISION_CODE = 0
NO_REVERSE = 1
IS_REVERSE = 2

class ReverseBasePurposeMaker(PurposeMakerBase):
    def __init__(self):
        pass

    @staticmethod
    def transform_on_candidate_pivots(data):
        candidate_pivot = np.array(list(IterativeZigzag(data, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD).transform()))
        final_pivot = peak_valley_pivots(data, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD)
        res = np.zeros(len(data))
        res[np.where(candidate_pivot != 0)[0]] = NO_REVERSE
        res[np.where(final_pivot != 0)[0]] = IS_REVERSE
        return res.reshape(-1, 1)

    @staticmethod
    def transform_on_w5_prime(data):
        candidate_pivot = np.where(np.array(list(IterativeZigzag(data, ZIGZAG_UP_THRESHOLD,
                                                                 ZIGZAG_DOWN_THRESHOLD).transform())) != 0)[0]
        final_pivot = peak_valley_pivots(data, ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD)

        w5_prime_positions = ZigzagSubwaveSeperator(np.where(final_pivot != 0)[0], candidate_pivot, None, final_pivot)\
                                                                                                .get_all_w5_prime_idx()

        res = np.zeros(len(data))
        final_pivot = np.where(final_pivot != 0)[0]
        last_seen_pivot_position = 0

        for item in w5_prime_positions:
            while item[0] - 1 > final_pivot[last_seen_pivot_position] and last_seen_pivot_position < len(final_pivot) - 1:
                last_seen_pivot_position += 1

            if item[0] - 1 == final_pivot[last_seen_pivot_position]:
                res[item[0]:item[1]] = IS_REVERSE
            else:
                res[item[0]:item[1]] = NO_REVERSE
        res = np.array(res)
        return res.reshape(-1, 1)

    def transform(self, x, cols, close, mode='w5_prime'):
        if mode == 'w5_prime':
            labels = self.transform_on_w5_prime(close)
        elif mode == 'candidate_pivots':
            labels = self.transform_on_candidate_pivots(close)
        else:
            raise BaseException('not existance mode...')
        return self.append_label_to_data(x, cols, labels)
