from .AnalizeAndDrawTrainResult import Analyze
from .CNN import CNNModel
from .GlobalVariable import *
from .LSTM import LSTMModel
from .MLP import MLP
from .tunner import KerasTunner

import json
from functools import reduce


def run_model(model, data_setter, *data_setter_args, handle=None, show_figs=True, zipper=None, save_results=True,
              **kwargs):
    model.set_data(*data_setter(*data_setter_args, **kwargs))
    model.yReal = handle.close_.values
    model.start_process(post_processor=handle.post_processor)
    analizer = Analyze(model, handle, show_figs=show_figs, save=save_results, zipper=zipper, **kwargs)
    analizer.run()
    return analizer


def load_model_from_saved_instance(model, data_setter, data_setter_inpput, path='2022_07_22_12_13_20_LSTM_DS'):
    model.load_model(path)
    model.set_data(*data_setter(data_setter_inpput))
    model.prepare_test_prediction_data()
    model.print_prf()
    # model.plot_acc_loss()


def load_ds_mode_from_json():
    with open('utils/ds_config.json', 'r') as f:
        couples = json.load(f)
    couples['submodel'] = reduce(lambda a, b: a + b, couples['submodel'].values())
    couples['main'] = couples['main'][0]
    shape = [len(item) for item in couples['submodel']]
    return couples, shape


def run_mlp_model(handle, run=True, future_step=1, zipper=None, save_model=False):
    model = MLP(MAIN_MLP_LAYERS_SHAPE, ACTIVATION_FUNCTION, OPTIMIZER, LR,
                None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, handle.feature_count,
                prediction_mode=PREDICTION_MODE, save_model=save_model, post_processor=handle.post_processor,
                add_pre_zigzag_analizer=INCLUDE_AND_UPDATE_PIVOTS_DATA or INCLUDE_PIVOTS_DATA,
                block_size=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE)
    run_model(model, handle.make_train_test_data, is_for_mlp=True, zipper=zipper, handle=handle,
              future_step=future_step) if run else None
    return model


def run_mlpDS_model(handle, run=True, future_step=1, zipper=None, save_model=False):
    couples, shape, dif_shape_mlp, feature_count = make_couples(handle)
    model = MLP(MAIN_MLP_LAYERS_SHAPE, ACTIVATION_FUNCTION, OPTIMIZER, LR,
                None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, feature_count,
                prediction_mode=PREDICTION_MODE, save_model=save_model, post_processor=handle.post_processor,
                dif_shape_mlp=dif_shape_mlp, feature_count_diff=shape, window_size=WINDOW_SIZE,
                add_pre_zigzag_analizer=INCLUDE_AND_UPDATE_PIVOTS_DATA or INCLUDE_PIVOTS_DATA,
                block_size=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE)
    run_model(model, handle.make_train_test_data_dictionary, couples=couples, zipper=zipper, handle=handle,
              future_step=future_step, is_for_mlp=True) if run else None
    return model


def run_lstm_model(handle, run=True, zipper=None, future_step=1):
    model = LSTMModel([WINDOW_SIZE * handle.feature_count, 300, 200, NUMBER_OF_OUTPUT_CLASSES], ACTIVATION_FUNCTION,
                      OPTIMIZER, LR,
                      None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, WINDOW_SIZE,
                      handle.feature_count, prediction_mode=PREDICTION_MODE,
                      add_pre_zigzag_analizer=INCLUDE_AND_UPDATE_PIVOTS_DATA or INCLUDE_PIVOTS_DATA,
                      post_processor=handle.post_processor, block_size=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE)
    run_model(model, handle.make_train_test_data, is_for_mlp=False, handle=handle, zipper=zipper,
              future_step=future_step) if run else None
    return model


def run_lstmDS_model(handle, run=True, zipper=None, future_step=1, save_model=True):
    couples, shape, dif_shape_lstm, feature_count = make_couples(handle)
    model = LSTMModel(MAIN_LSTM_LAYERS_SHAPE, ACTIVATION_FUNCTION, OPTIMIZER, LR,
                      None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, WINDOW_SIZE, feature_count,
                      dif_shape_lstms=dif_shape_lstm, feature_count_diff=shape, prediction_mode=PREDICTION_MODE,
                      save_model=save_model, post_processor=handle.post_processor,
                      add_pre_zigzag_analizer=INCLUDE_AND_UPDATE_PIVOTS_DATA or INCLUDE_PIVOTS_DATA,
                      block_size=NUMBER_OF_FUTURE_IN_MULTIPILE_FUTURE)
    run_model(model, handle.make_train_test_data_dictionary, couples, zipper=zipper, handle=handle,
              future_step=future_step) if run else None
    return model


def run_CNN_model(handle, run=True):
    model = CNNModel([320, 400, 150, NUMBER_OF_OUTPUT_CLASSES], [10, 20, 40, 40, 40], [2] * 5, [2] * 5, ['same'] * 5,
                     ACTIVATION_FUNCTION, OPTIMIZER, LR, None, DROP_OUT,
                     LOSS_FUNCTION, EPOCHS, BATCH_SIZE, VERBOSE, WINDOW_SIZE, feature_count=handle.feature_count,
                     prediction_mode=PREDICTION_MODE)
    run_model(model, handle.make_train_test_data, False) if run else None
    return model


def start_tune(handle, max_trial=1000, wait_window=0, future_step=1):
    KerasTunner(handle, ).start(max_trial=max_trial, wait_window=wait_window, future_step=future_step)


def load_tune(handle):
    KerasTunner(handle).load()


def make_couples(handle):
    couples, shape = load_ds_mode_from_json()
    dif_shape_lstm = [[item] + SUBMODEL_MLP_LAYERS_SHAPE for item in shape]
    feature_count = handle.feature_count if couples['main'] == ['all'] else len(couples['main'])
    return couples, shape, dif_shape_lstm, feature_count
