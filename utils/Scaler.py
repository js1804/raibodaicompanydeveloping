class Scaler:
    def __init__(self) -> None:
        pass

    def transform(self, x, cols):
        self.first_close_ = x[0, cols.index('close')]
        x /= self.first_close_
        return x

    def __call__(self, x, cols):
        return self.transform(x, cols)