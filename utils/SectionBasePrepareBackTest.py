from .BackTraderTesting import backTradeTesting

import numpy as np
from functools import reduce
import matplotlib.pyplot as plt
import time
import os
import json


class SectionBasePrepareBackTest:
    def __init__(self, indice, close_price, model) -> None:
        self.indices = indice
        self.close_price = close_price
        self.model = model
        self.reset()

    def reset(self):
        self.trade_actions = []
        self.trade_results = []
        self.y_real = None

    @staticmethod
    def get_dict_slice(dic, slic):
        return {key: val[slic] for key, val in dic.items()}

    def final(self, x_in, reset_state, future=0, in_tune_mode=True, save_path='', ax=None, lot=1, 
              exponential_trading=False, exponential_division_factor=1):
        self.reset() if reset_state else None
        base_idx = 0
        base_margin = 100000
        base_trade_count = 0
        for i, (st, sp, rang) in enumerate(self.indices):
            y_real = self.close_price[st+future: st+future+rang]
            back_test = backTradeTesting(self.model, self.get_dict_slice(x_in, slice(base_idx, base_idx+rang)), 
                                         np.zeros(len(y_real)), base_margin, 100, y_real, 
                                         True, in_tune_mode, save_path, ax, lot, 
                                         exponential_trading, exponential_division_factor, 
                                         base_trade_count, base_idx)
            self.trade_results.append(back_test.final())
            self.trade_actions.append(back_test.trades_detail)
            base_idx += rang
            base_margin += self.trade_results[-1]['netProfit']
            base_trade_count += int(self.trade_results[-1]['numberOfTrades'])
            self.y_real = y_real if self.y_real is None else np.concatenate([self.y_real, y_real])
        return self.trade_results, self.trade_actions

    def unify_results(self):
        final_result = {}
        final_result["numberOfTrades"] = sum(map(lambda x: x['numberOfTrades'], self.trade_results))
        final_result["numberOfBuy"] = sum(map(lambda x: x['numberOfBuy'], self.trade_results))
        final_result["numberOfSell"] = sum(map(lambda x: x['numberOfSell'], self.trade_results))
        final_result["numberOfPositveSell"] = sum(map(lambda x: x['numberOfPositveSell'], self.trade_results))
        final_result["numberOfPositiveBuy"] = sum(map(lambda x: x['numberOfPositiveBuy'], self.trade_results))
        final_result["profitInBuyTrades"] = sum(map(lambda x: x['profitInBuyTrades'], self.trade_results))
        final_result["profitInSellTrades"] = sum(map(lambda x: x['profitInSellTrades'], self.trade_results))
        final_result["netProfit"] = sum(map(lambda x: x['netProfit'], self.trade_results))
        final_result["totallProfit"] = sum(map(lambda x: x['totallProfit'], self.trade_results))
        final_result["totallLoss"] = sum(map(lambda x: x['totallLoss'], self.trade_results))
        final_result["numberOfLossBuy"] = sum(map(lambda x: x['numberOfLossBuy'], self.trade_results))
        final_result["lossInBuyTrades"] = sum(map(lambda x: x['lossInBuyTrades'], self.trade_results))
        final_result["numberOfLossSell"] = sum(map(lambda x: x['numberOfLossSell'], self.trade_results))
        final_result["lossInSellTrades"] = sum(map(lambda x: x['lossInSellTrades'], self.trade_results))
        final_result["netBuy"] = sum(map(lambda x: x['netBuy'], self.trade_results))
        final_result["netSell"] = sum(map(lambda x: x['netSell'], self.trade_results))
        final_result["winRate"] = sum(map(lambda x: x['winRate'], self.trade_results)) / len(self.trade_results)
        final_result["lossRate"] = sum(map(lambda x: x['lossRate'], self.trade_results))/ len(self.trade_results)

        final_result['average_win'] = sum(map(lambda x: x['average_win']*x['numberOfTrades'], self.trade_results))/ final_result['numberOfTrades']
        final_result['average_loss'] = sum(map(lambda x: x['average_loss']*x['numberOfTrades'], self.trade_results))/ final_result['numberOfTrades']
        final_result['pay_of_ratios'] = abs(final_result['average_win'] / (final_result['average_loss'] + .001))

        final_result['max_draw_down'] = max([item['max_draw_down'] for item in self.trade_results])
        final_result['list_of_draw_downs'] = sorted(reduce(lambda a, b: a+b, map(lambda x: x['list_of_draw_downs'], self.trade_results)))[-20:][::-1]
        final_result['max_consecutive_loss'] = max([item['max_consecutive_loss'] for item in self.trade_results])
        final_result['list_of_consecutive_loss'] = sorted(reduce(lambda a, b: a+b, map(lambda x: x['list_of_consecutive_loss'], self.trade_results)))[-5:][::-1]
        final_result['list_of_candle_base_consecutive_loss'] = sorted(reduce(lambda a, b: a+b, map(lambda x: x['list_of_candle_base_consecutive_loss'], self.trade_results)))[-5:][::-1]
        return final_result
    
    def unify_actions(self):
        res = {}
        for item in self.trade_actions:
            res.update(item)
        return res
    
    def plot(self, length, ax, path='', save=False):
        trade_detail = self.unify_actions()
        margins = np.ones(length)
        pre_idx = 0
        for i, item in enumerate(trade_detail.values()):
            margins[pre_idx:item['closeState']] = item['margin']
            pre_idx = item['closeState']
        margins[pre_idx:] = margins[pre_idx-1]

        ax.plot(margins)
        self.margins = margins
        if save:
            ax.figure.savefig(path)
        
    def save(self, save_path, length, ax, show_save_fig):
        timeFormat = '%Y_%m_%d_%H_%M_%S'
        dirName = 'backtest_res_' + time.strftime(timeFormat)
        dir_path = f'{save_path}/{dirName}'
        while 1:
            try:
                os.mkdir(dir_path)
                break
            except BaseException as e:
                dir_path = dir_path + '_'
                print(dir_path)

        with open(f'{dir_path}/trades.json', 'w') as jFile:
            json.dump(self.unify_actions(), jFile)
        if show_save_fig:
            self.plot(length, ax, f'{dir_path}/margins.png', True)
