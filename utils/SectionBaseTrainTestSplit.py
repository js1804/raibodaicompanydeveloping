from .TrainTestSplit import TrainTestSplit

import numpy as np
from functools import reduce


class SectionBaseTrainTestSplit:
    def __init__(self):
        pass

    @staticmethod
    def get_slice(*args, st, sp):
        return [item[st:sp] for item in args]
    
    @staticmethod
    def concat_train_test_to_train_test(datas):
        res = [item for item in datas[0]]
        for section in datas[1:]:
            for i, item in enumerate(section):
                res[i] = np.concatenate([res[i], item])
        return res
    
    @staticmethod
    def get_test_idx(indice):
        return [[item[1], item[2]] for item in indice]
    
    @staticmethod
    def get_train_idx(indice):
        return [[item[0], item[1]] for item in indice]
    
    @staticmethod
    def get_data_on_list_of_indices(data, list_of_idx):
        res = data[slice(list_of_idx[0][0], list_of_idx[0][1])]
        for item in list_of_idx[1:]:
            res = np.concatenate([res, data[slice(item[0], item[1])]])
        return res

    def __call__(self, *args, train_size=.75, block_size=4, num_of_sections=10):
        data_length = len(args[0])
        idx_touple = list(zip(range(0, data_length, data_length//(num_of_sections + 1)), 
                              range(data_length//(num_of_sections + 1), data_length, data_length//(num_of_sections + 1))))
        idx_touple[-1] = (idx_touple[-1][0], data_length)
        data_res, idx_res = [], []
        for st, sp in idx_touple:
            r = TrainTestSplit()(*self.get_slice(*args, st=st, sp=sp), train_size=train_size, block_size=block_size)
            data_res.append(r[0])
            idx_res.append((st, r[1][0][1]+st, sp))
        data_res = self.concat_train_test_to_train_test(data_res)
        return data_res, idx_res

