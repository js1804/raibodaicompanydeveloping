from .PostProcessorsBase import PostProcessorsBase

import numpy as np


class SimplePostProcessor(PostProcessorsBase):
    def __init__(self, model, **kwargs):
        super().__init__(model)
        # self.predict = self.model.predict

    def predict(self, x_in):
        return np.argmax(self.model.predict(x_in), axis=1)
