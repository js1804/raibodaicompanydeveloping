from .EqualizeLossAndProfit import EqualizeLossAndProfit

class SmootherData():
    def __init__(self, shift_size, window_size, pct_change_mode=False, make_loss_profit_equal=False):
        self.window_size = window_size
        self.shift_size = shift_size
        self.pct_change_mode = pct_change_mode
        self.make_loss_profit_equal = make_loss_profit_equal

    def make_normal_pct_change(self, x, col):
        return (x[col] / x[col].shift(self.shift_size) - 1)

    def transform(self, x, cols=None):
        added_col_name = f'_pct_change_{self.shift_size}_smoother_{self.window_size}' if self.pct_change_mode else f'_smoother_{self.window_size}'
        pct_change_calculator = self.make_normal_pct_change if not self.make_loss_profit_equal else EqualizeLossAndProfit(self.shift_size)
        if cols == None and self.pct_change_mode:
            for col in x.columns:
                if 'smoother' in col:
                    continue
                x[col + added_col_name] = pct_change_calculator(x, col).rolling(self.window_size).mean()
        elif cols == None and not self.pct_change_mode:
            for col in x.columns:
                if 'smoother' in col:
                    continue
                x[col + added_col_name] = x[col].rolling(self.window_size).mean()
        elif cols != None and self.pct_change_mode:
            for col in cols:
                if 'smoother' in col:
                    continue
                x[col + added_col_name] = pct_change_calculator(x, col).rolling(self.window_size).mean()
        elif cols != None and not self.pct_change_mode:
            for col in cols:
                if 'smoother' in col:
                    continue
                x[col + added_col_name] = x[col].rolling(self.window_size).mean()
        return x
    
    def __call__(self,x, cols=None):
        return self.transform(x, cols)
