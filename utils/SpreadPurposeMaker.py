from .GlobalVariable import *
import numpy as np
import pandas as pd


class SpreadPurposeMaker:
    def __init__(self, spread=.3) -> None:
        self.spread = spread

    def transform(self, x, close, cols):
        res = np.append(np.array([0]), close[1:] - close[:-1], axis=0)
        buy_indx = res > self.spread
        sell_indx = res < -self.spread
        do_nothing_indx = np.logical_and(res >= -self.spread, res <= self.spread)
        res[buy_indx] = BUY_POSITION
        res[sell_indx] = SELL_POSITION
        res[do_nothing_indx] = DO_NOTHING_POSITION
        res[np.isnan(res)] = DO_NOTHING_POSITION
        res = res.reshape(-1, 1)

        if 'purpose' not in cols:
            x = np.append(x, res, axis=1) 
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = res
        return x

    def __call__(self, x, close, cols):
        return self.transform(x, close, cols)