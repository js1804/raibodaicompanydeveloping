from .FilterOnHistData import FilterOnHistData
from .GlobalVariable import BUY_POSITION, SELL_POSITION, DO_NOTHING_POSITION, ZIGZAG_UP_THRESHOLD, CLOSE_POSITION

import numpy as np
import matplotlib.pyplot as plt


class StatisticTradeStrategy:
    def __init__(self, handler, mode='filter_on_statistic_w4'):
        self.handler = handler
        self.filter_function = getattr(self, mode)

    def filter_on_all_w4(self, data):
        res = np.ones(len(data))
        res[self.handler.condidate_pivots] = 0
        return res

    def filter_on_statistic_w4(self, data):
        columns = [item for item in data.columns if 'price' in item]
        pivot_only_filterer = FilterOnHistData()
        return pivot_only_filterer.compute(data[columns].values, 'price', [4],
                                           self.handler.zigzag_analizer.list_of_true_ratios_idxs,
                                           file_name_post_fix='proved_pivot')

    def predict_(self, data):
        filter = self.filter_function(data)

        in_trade = False
        direction = -1
        pre_direction = direction
        stop_loss = 0
        list_of_stop_loss = []
        max_pre_w4_position = -1
        number_of_position_found = 0
        for close, decision, is_pivot, pivot_list in zip(self.handler.close_.values, filter,
                                                         self.handler.condidate_pivots,
                                                         self.handler.zigzag_analizer.list_of_tss):
            direction = pre_direction
            pre_direction = is_pivot if is_pivot != 0 else direction
            list_of_stop_loss.append(stop_loss)

            if in_trade:
                if self.hit_stop_loss(close, direction, stop_loss):
                    in_trade = False
                    stop_loss = 0
                    yield CLOSE_POSITION
                    continue
                # if max_pre_w4_position < pivot_list[-2] and decision == 1
                if max_pre_w4_position < pivot_list[-2]:
                    max_pre_w4_position = pivot_list[-2]
                    stop_loss = self.set_stop_loss(self.handler.close_.values[max_pre_w4_position], direction)
            else:
                if decision == 1:
                    number_of_position_found += 1
                    in_trade = True
                    max_pre_w4_position = pivot_list[-2]
                    stop_loss = self.set_stop_loss(self.handler.close_.values[max_pre_w4_position], direction)
                    yield self.choose_buy_or_sell(direction)
                    continue
            yield DO_NOTHING_POSITION
        list_of_stop_loss = np.array(list_of_stop_loss)
        x_ = np.where(list_of_stop_loss != 0)[0]
        plt.plot(x_, list_of_stop_loss[list_of_stop_loss != 0])

    @staticmethod
    def choose_buy_or_sell(direction):
        if direction == -1:
            return SELL_POSITION
        return BUY_POSITION

    @staticmethod
    def hit_stop_loss(close, direction, sl_close):
        if direction == 1:
            return True if close < sl_close else False
        else:
            return True if close > sl_close else False

    @staticmethod
    def set_stop_loss(close, direction):
        return close*(1 + ZIGZAG_UP_THRESHOLD) if direction == -1 else close*(1 - ZIGZAG_UP_THRESHOLD)

    def predict(self, data):
        return np.array(list(self.predict_(data)))
