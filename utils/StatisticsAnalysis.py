from .GlobalVariable import NUMBER_OF_PIVOTS, ZIGZAG_UP_THRESHOLD
from .FilterOnHistData import FilterOnHistData

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm


class StatisticsAnalysis:

    @staticmethod
    def plot(close, pivots_position, filters, first_reversed_point=None):
        plt.scatter(np.where(filters == 1)[0], close.iloc[np.where(filters == 1)[0]], color='red', s=15)
        plt.plot(close.values, color='blue')
        plt.plot(pivots_position, close.iloc[pivots_position], color='black')
        if first_reversed_point is not None:
            plt.scatter(first_reversed_point, close.iloc[first_reversed_point], color='orange', marker='^')
        plt.show()

    @staticmethod
    def structure_base_analysis(close, pivots_position, filters, first_reversed_point, sorter):
        res = {}
        for i in range(NUMBER_OF_PIVOTS, min(len(pivots_position), len(first_reversed_point))):
            structure = sorter(close.values[pivots_position[i - NUMBER_OF_PIVOTS: i]])
            if structure not in res:
                res[structure] = [0, 0, 0, 0]
            tmp = filters[pivots_position[i - 1]:pivots_position[i]]
            before_reverse_point = tmp[:first_reversed_point[i - 2] - pivots_position[i - 1]]
            after_reverse_point = tmp[:pivots_position[i] - first_reversed_point[i - 2]]
            res[structure][0] += len(before_reverse_point[before_reverse_point == 1])
            res[structure][1] += first_reversed_point[i - 2] - pivots_position[i - 1]
            res[structure][2] += len(after_reverse_point[after_reverse_point == 1])
            res[structure][3] += len(tmp)
        print(*enumerate(res.items()), sep='\n')
        print(*sorted(res.items(), key=lambda x: x[1][-1]), sep='\n')
        return res

    @staticmethod
    def structure_base_analysis_candle_base(close, pivots_position, filters, final_pivots, sorter):
        base = 5
        res = {}
        sub_res = {}
        cclose = close.values
        for i, item in enumerate(tqdm(pivots_position)):
            sub_structure = sorter(cclose[item])
            if sub_structure not in sub_res:
                sub_res[sub_structure] = [0, 0, 0]
            if sub_structure not in res:
                res[sub_structure] = [0, 0, 0]

            if filters[i] == 1:
                if abs(1 - (cclose[item[-1]] / cclose[item[-2]])) < ZIGZAG_UP_THRESHOLD:
                    sub_res[sub_structure][0] += 1
                else:
                    sub_res[sub_structure][1] += 1
            sub_res[sub_structure][2] += 1
            res[sub_structure][2] += 1
            if filters[i] == 1:
                if abs(1 - (cclose[item[-1]] / cclose[final_pivots[base]])) >= ZIGZAG_UP_THRESHOLD:
                    res[sub_structure][1] += 1
                elif abs(1 - (cclose[item[-1]] / cclose[final_pivots[base]])) < ZIGZAG_UP_THRESHOLD:
                    res[sub_structure][0] += 1
                else:
                    print('problem in:', abs(1 - (cclose[item[-1]] / cclose[final_pivots[base]])), item[-1],
                          final_pivots[base], cclose[item[-1]], cclose[final_pivots[base]])
            if item[-1] >= final_pivots[base + 1]:
                base += 1 if base != len(final_pivots) - 2 else 0
        # print(*sorted(sub_res.items(), key=lambda x: x[1][2]), sep='\n')
        print(*sorted(res.items(), key=lambda x: x[1][2]), sep='\n')

        return res, sub_res

    @staticmethod
    def overall_statistic_calculation(pivots_positions, filters, train_test_size=.75, first_reversed_point=None,
                                      all_pivots=None):
        test_first_idx = int(len(filters) * train_test_size)

        print('number of pivots: ', len(pivots_positions))
        print(f'filter coverage: {len(filters[filters == 1]) / len(filters) * 100}')
        print(
            f'filter coverage on train: {len(filters[:test_first_idx][filters[:test_first_idx] == 1]) / test_first_idx * 100}')
        print(
            f'filter coverage on test: {len(filters[test_first_idx:][filters[test_first_idx:] == 1]) / (len(filters) - test_first_idx) * 100}')
        StatisticsAnalysis.print_reverse_point_base_details(pivots_positions, first_reversed_point, filters,
                                                          test_first_idx, all_pivots)

        p_test = pivots_positions
        p_test = p_test[p_test >= test_first_idx] - test_first_idx
        f_test = filters[test_first_idx:]
        r_test = f_test[p_test]
        print(f'percent of removed pivots test: {len(r_test[r_test == 1]) / len(r_test) * 100}')

        p_test = pivots_positions
        p_test = p_test[p_test < test_first_idx]
        f_test = filters[:test_first_idx]
        r_test = f_test[p_test]
        print(f'percent of removed pivots train: {len(r_test[r_test == 1]) / len(r_test) * 100}')

    @staticmethod
    def print_reverse_point_base_details(pivots, reverse_points, filters, test_first_idx, all_pivots):
        if reverse_points is not None:
            reverse_point_details = StatisticsAnalysis.calculate_miss_acc_wave_base(
                pivots[pivots > test_first_idx],
                reverse_points[reverse_points > test_first_idx],
                filters[pivots[pivots > test_first_idx][0]:])
            w4_base = StatisticsAnalysis.calculate_miss_acc_w4_base(all_pivots,
                                                                  pivots[pivots > test_first_idx],
                                                                  reverse_points[reverse_points > test_first_idx],
                                                                  filters)
            print('test section:')
            print('percent of removed waves before and after reverse point:', reverse_point_details[0],
                  reverse_point_details[1])
            print('percent of removed pivots before and after reverse point:', reverse_point_details[2],
                  reverse_point_details[3])
            print('number of w4 before and after reverse has predicted:', w4_base)

            reverse_point_details = StatisticsAnalysis.calculate_miss_acc_wave_base(
                pivots[pivots < test_first_idx],
                reverse_points[reverse_points < test_first_idx],
                filters[:pivots[pivots < test_first_idx][-1] + 1])
            w4_base = StatisticsAnalysis.calculate_miss_acc_w4_base(all_pivots,
                                                                  pivots[pivots < test_first_idx],
                                                                  reverse_points[reverse_points < test_first_idx],
                                                                  filters)
            print('train section:')
            print('percent of removed waves before and after reverse point:', reverse_point_details[0],
                  reverse_point_details[1])
            print('percent of removed pivots before and after reverse point:', reverse_point_details[2],
                  reverse_point_details[3])
            print('number of w4 before and after reverse has predicted:', w4_base)

            reverse_point_details = StatisticsAnalysis.calculate_miss_acc_wave_base(
                pivots,
                reverse_points,
                filters)
            w4_base = StatisticsAnalysis.calculate_miss_acc_w4_base(all_pivots,
                                                                  pivots,
                                                                  reverse_points,
                                                                  filters)
            print('all section:')
            print('percent of removed waves before and after reverse point:', reverse_point_details[0],
                  reverse_point_details[1])
            print('percent of removed pivots before and after reverse point:', reverse_point_details[2],
                  reverse_point_details[3])
            print('number of w4 before and after reverse has predicted:', w4_base)

    @staticmethod
    def calculate_miss_acc_wave_base(pivots, reverse_points, filters):
        piv = pivots[len(pivots[pivots < reverse_points[0]]) - 1:]
        before_reverse_point, after_reverse_point = 0, 0
        before_reverse_candle_count, after_reverse_candle_count = 0, 0
        w4_base_counting = 0
        for st, sp in zip(piv, reverse_points):
            decision = len(filters[st:sp][filters[st:sp] == 1])
            before_reverse_point += 1 if decision else 0
            before_reverse_candle_count += decision

        piv = pivots[pivots > reverse_points[0]]
        for st, sp in zip(reverse_points[len(reverse_points[reverse_points < piv[0]]) - 1:], piv):
            decision = len(filters[st:sp][filters[st:sp] == 1])
            after_reverse_point += 1 if decision else 0
            after_reverse_candle_count += decision

        return before_reverse_point / len(reverse_points), after_reverse_point / len(reverse_points), \
               before_reverse_candle_count / len(filters), after_reverse_candle_count / len(filters)

    @staticmethod
    def calculate_miss_acc_w4_base(all_pivots, final_pivots, reverse_points, filters):
        if reverse_points is None:
            return
        piv = final_pivots[final_pivots > reverse_points[0]]
        true_counter = 0
        for st, sp in zip(reverse_points[len(reverse_points[reverse_points < piv[0]])-1:], piv):
            pre_max = -1
            for i in range(st, sp):
                if filters[i] == 1:
                    if all_pivots[i][-2] > pre_max:
                        true_counter += 1
                        pre_max = all_pivots[i][-2]

        piv = final_pivots[len(final_pivots[final_pivots < reverse_points[0]]) - 1:]
        false_counter = 0
        for st, sp in zip(piv, reverse_points):
            false_counter += 1 if len(np.where(filters[st:sp] == 1)[0]) else 0
        return false_counter, true_counter

    @staticmethod
    def dual_w4_predicts(all_pivots, final_pivots, reverse_points, pivot_filter, not_pivot_filter):
        res = np.logical_and(pivot_filter, np.logical_not(not_pivot_filter))
        StatisticsAnalysis.print_reverse_point_base_details(final_pivots, reverse_points, res, 50000, all_pivots)

        res = np.logical_and(pivot_filter, not_pivot_filter)
        StatisticsAnalysis.print_reverse_point_base_details(final_pivots, reverse_points, res, 50000, all_pivots)

        res = pivot_filter
        StatisticsAnalysis.print_reverse_point_base_details(final_pivots, reverse_points, res, 50000, all_pivots)

        res = np.logical_or(np.logical_not(pivot_filter), not_pivot_filter)
        StatisticsAnalysis.print_reverse_point_base_details(final_pivots, reverse_points, res, 50000, all_pivots)
