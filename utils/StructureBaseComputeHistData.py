import copy

from .ComputeHistData import ComputeHistData
from .FilterOnHistData import FilterOnHistData
from .GlobalVariable import STATISTIC_BUCKET_LENGTH, STATISTIC_NUMBER_OF_BUCKET

import numpy as np
import pickle
import multiprocessing


class StructureBaseComputeHistData(ComputeHistData):
    def __init__(self, num_bins=STATISTIC_NUMBER_OF_BUCKET, bin_edge=STATISTIC_BUCKET_LENGTH):
        super().__init__(num_bins, bin_edge)

    def compute(self, data, name, column_names, all_pivots, closes, func, final_pivots=slice(None, None), num_worker=6,
                file_name_post_fix=''):
        res_dict = {'names': {item: i for i, item in enumerate(column_names)}}
        structure_idx = self.group_by(all_pivots[final_pivots], closes, func)
        inputs = [[StructureBaseComputeHistData.one_thread_compute,
                   {'data': data[final_pivots][indices], 'indices': indices, 'bin_edges': self.make_bin_edge_list()}]
                  for indices in structure_idx.values()]
        with multiprocessing.Pool(num_worker) as pool:
            res = pool.map(StructureBaseComputeHistData.input_wrapper, inputs)
        # res = process_map(StructureBaseComputeHistData.input_wrapper, inputs, max_workers=6)
        for i, key in enumerate(structure_idx.keys()):
            res_dict[key] = res[i]
        with open(f'bin_edge_analysis_structure_base_{name}_{file_name_post_fix}.pkl', 'wb') as f:
            pickle.dump(res_dict, f)

    @staticmethod
    def group_by(all_pivots, closes, func=FilterOnHistData.structure_base_pair_sorter):
        res = {}
        for i, item in enumerate(all_pivots):
            code = func(closes[item])
            if code not in res:
                res[code] = []
            res[code].append(i)
        for key in res:
            res[key] = np.array(res[key])
        return res

    @staticmethod
    def one_thread_compute(data, indices, bin_edges):
        tmp = {}
        for col_idx in range(data.shape[1]):
            col = data[:, col_idx]
            count, bins = np.histogram(col, bin_edges)
            tmp[col_idx] = {k: v for k, v in zip(np.where(count > 0)[0], count[count > 0])}
        return tmp

    @staticmethod
    def input_wrapper(kwargs):
        return kwargs[0](**kwargs[1])
