from .FilterOnHistData import FilterOnHistData
from .StructureBaseComputeHistData import StructureBaseComputeHistData
from .GlobalVariable import STATISTIC_BUCKET_LENGTH, STATISTIC_NUMBER_OF_BUCKET

import numpy as np
import pickle
import copy
import multiprocessing


class StructureBaseFilterOnHistData(FilterOnHistData):
    def __init__(self, num_bins=STATISTIC_NUMBER_OF_BUCKET, bin_edge=STATISTIC_BUCKET_LENGTH):
        super().__init__(num_bins=num_bins, bin_edge=bin_edge)

    def compute(self, data, all_pivots, closes, func, type_name='price', wave_to_check=[4], true_second_wave=None,
                file_name_post_fix=''):
        with open(f'bin_edge_analysis_structure_base_{type_name}_{file_name_post_fix}.pkl', 'rb') as f:
            bins_data = pickle.load(f)
        res = np.zeros(len(data))
        alias = self.make_relevant_pairs(list(bins_data['names'].keys()), exclusive=False)
        idx_to_name = {val: key for key, val in bins_data['names'].items()}
        structure_indices = StructureBaseComputeHistData.group_by(all_pivots, closes, func)
        inputs = [[StructureBaseFilterOnHistData.one_thread_compute,
                   {'data': data[v], 'idx_to_name': idx_to_name,
                    'alias': alias, 'bins': self.make_bin_edge_list(),
                    'col_names_to_check': self.col_names_to_check(list(bins_data['names'].keys()), wave_to_check),
                    'col_names':bins_data['names'],
                    'bins_data': bins_data[ke]}] for ke, v in structure_indices.items() if ke in bins_data]

        with multiprocessing.Pool(6) as pool:
            partial_res = pool.map(StructureBaseComputeHistData.input_wrapper, inputs)
        count = 0
        for i, ke in enumerate(structure_indices.keys()):
            if ke not in bins_data:
                continue
            res[structure_indices[ke]] = partial_res[count]
            count += 1
        if true_second_wave is not None:
            res[true_second_wave] = 0
        with open('existance_check.pkl', 'wb') as f:
            pickle.dump(res, f)
        return res

    @staticmethod
    def one_thread_compute(data, idx_to_name, alias, col_names_to_check, col_names, bins_data, bins):
        res = np.zeros(len(data))
        for col_idx in range(data.shape[1]):
            if idx_to_name[col_idx] not in alias:
                continue
            if idx_to_name[col_idx] not in col_names_to_check:
                continue
            col = copy.deepcopy(data[:, col_idx])
            col = StructureBaseFilterOnHistData.static_send_to_sail(col, bins)
            col_to_check_with__id = col_names[alias[idx_to_name[col_idx]][0]]
            has_been_seen_buckets = list(bins_data[col_to_check_with__id].keys())
            res[np.logical_not(res)] = np.logical_not(np.in1d(col[np.logical_not(res)],
                                                              has_been_seen_buckets))
        return res

    @staticmethod
    def static_send_to_sail(col_data, bins):
        for i, (st, sp) in enumerate(zip(bins[:-1], bins[1:])):
            col_data[np.logical_and(col_data >= st, col_data < sp)] = i
        return col_data
