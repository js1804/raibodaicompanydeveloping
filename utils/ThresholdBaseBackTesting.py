from .BackTraderTesting import backTradeTesting
from .GlobalVariable import SELL_POSITION, BUY_POSITION, OPEN_POSITION, CLOSE_POSITION, DO_NOTHING_POSITION

import numpy as np
import pandas as pd
from tqdm import tqdm


class ThresholdBaseBackTesting(backTradeTesting):
    def __init__(self, model, xTest, yTest, margin, leverage, yReal, tradeOnYourPrediction=True,
                 in_tune_mode=False, save_path=None, ax=None, lot=1, exponential_trading=False,
                 exponential_division_factor=1, up_threshold=.7, down_threshold=.6):
        super().__init__(model, xTest, yTest, margin, leverage, yReal, tradeOnYourPrediction=tradeOnYourPrediction,
                         in_tune_mode=in_tune_mode, save_path=save_path, ax=ax, lot=lot,
                         exponential_trading=exponential_trading,
                         exponential_division_factor=exponential_division_factor)
        self.up_threshold = up_threshold
        self.down_threshold = down_threshold

    def threshold_calculation(self, pred, up_thresh=None, down_thresh=None):
        up_thresh = self.up_threshold if up_thresh is None else up_thresh
        down_thresh = self.down_threshold if down_thresh is None else down_thresh
        idx = np.argmax(pred)
        if idx == SELL_POSITION:
            if pred[idx] > down_thresh:
                return SELL_POSITION
        elif idx == BUY_POSITION:
            if pred[idx] > up_thresh:
                return BUY_POSITION
        else:
            return DO_NOTHING_POSITION

    def trades(self):
        trades = dict()
        tagOpen = 0
        numberOfTrade = 0
        previousPredict = list()
        margin = self.margin
        margins = list()
        previousPredict.append(self.yTest[0])
        resultToChangeTrdesConcept = pd.DataFrame()
        print('=' * 25, 'start trading', '=' * 25)
        print(f"yTest shape = {self.yTest.shape}")

        predicts = self.model.predict(self.xTest)
        for i in tqdm(range(0, len(self.yTest) - 1)):
            # previousPredict.append(predicts[i])

            if tagOpen == 0:
                predict = self.threshold_calculation(predicts[i])
                status = self.openingAgen(predict, i, margin)
                if status != None:
                    trades["step" + str(numberOfTrade)] = status
                    tagOpen = 1
                margins.append(int(margin))

            else:
                predict = self.threshold_calculation(predicts[i], .7, .7)
                closePosition = self.closingAgent([None, predict], i, trades["step" + str(numberOfTrade)])

                if closePosition["close"] >= 1:
                    margin = trades["step" + str(numberOfTrade)]["margin"]

                    if trades["step" + str(numberOfTrade)]["margin"] > 0:
                        numberOfTrade += 1
                        tagOpen = 0
                        status = self.openingAgen(self.threshold_calculation(predicts[i]), i, margin)
                        if status != None:
                            trades["step" + str(numberOfTrade)] = status
                            tagOpen = 1

                margins.append(margin)
        dirPath = self.savefiles(trades, margins)

        return (trades, margins, resultToChangeTrdesConcept, dirPath)
