from .PostProcessorsBase import PostProcessorsBase
from .GlobalVariable import DO_NOTHING_POSITION

import numpy as np
from sklearn.preprocessing import OneHotEncoder


class ThresholdPostProcessor(PostProcessorsBase):
    def __init__(self, model, up_threshold, down_threshold, **kwargs):
        super().__init__(model, **kwargs)
        self.up_threshold = up_threshold
        self.down_threshold = down_threshold

    def predict(self, x_in):
        powers = self.model.predict(x_in)

        res = np.argmax(powers, axis=1)
        
        res[np.logical_and(np.argmax(powers, axis=1) == 1, np.max(powers, axis=1) < self.up_threshold)] = DO_NOTHING_POSITION
        res[np.logical_and(np.argmax(powers, axis=1) == 0, np.max(powers, axis=1) < self.down_threshold,
                           np.max(powers, axis=1) > .5)] = DO_NOTHING_POSITION

        res = OneHotEncoder(sparse=False).fit_transform(res.reshape(-1, 1))
        return res