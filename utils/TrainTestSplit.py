import numpy as np


class TrainTestSplit:
    def __init__(self) -> None:
        pass

    @staticmethod
    def calc_block_size_idx(length, block_size, train_size):
        bar_pos = int(length * (1-train_size))
        bar_pos = bar_pos - (bar_pos%block_size) 
        return length - bar_pos

    def __call__(self, *args, train_size=.75, block_size=4):
        l = len(args[0])
        bar_pos = self.calc_block_size_idx(len(args[0]), block_size, train_size)
        res = []
        for item in args:
            res.extend([item[:bar_pos], item[bar_pos:]])
        return res, [(0, bar_pos, l)]
        