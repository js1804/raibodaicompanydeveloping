from .MLBase import MLBase
import numpy as np
from xgboost.sklearn import XGBClassifier

class XGBModel(MLBase):
    def __init__(self) -> None:
        self.model = XGBClassifier(tree_method='hist', verbosity=2)

    def start_process(self):
        self.x = self.x.reshape(self.x.shape[0], -1)
        self.x_test = self.x_test.reshape(self.x_test.shape[0], -1)

        self.x[self.x == np.inf] = 0
        self.x[self.x == -np.inf] = 0
        self.x_test[self.x_test == np.inf] = 0
        self.x_test[self.x_test == -np.inf] = 0

        self.model.fit(self.x, np.argmax(self.y, axis=1).reshape(-1,1))
        self.y_pred = self.model.predict(self.x_test)
        self.y_true = np.argmax(self.y_test, axis=1)
        self.print_prf()