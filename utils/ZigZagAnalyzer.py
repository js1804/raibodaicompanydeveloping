import copy
import pickle

from .IterativeZigzag import IterativeZigzag
from .GlobalVariable import MINIMUM_ZIGZAG_WAVE_COUNT, MIN_COVERAGE, PATH_LIST_OF_ZIGZAG_SEQUENCE_NAME, \
    NUMBER_OF_PIVOTS, NUMBER_OF_RATIOS
from .MyExceptions import MinimumZigzagWaveNotPassed
from .FIbonachiFilter import FibonachiFilter
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
from sklearn.preprocessing import OneHotEncoder
from tqdm import tqdm


class ZigZogAnalyzer:
    NUMBER_OF_RATIOS = NUMBER_OF_RATIOS + 1
    NUMBER_OF_WAVES = NUMBER_OF_PIVOTS
    NORMALIZING_WINDOW_SIZE = 1000

    def __init__(self, price_ratios=True, time_ratios=False, pace_ratios=False, cou=0, plot_pivots=False) -> None:
        self.res = {}
        self.cou = cou
        self.plot_pivots = plot_pivots
        self.functions = {'price': self.calc_price_ratios, 'pace': self.calc_pace_ratios, 'time': self.calc_time_ratios}
        self.normalizer_shifter = {}
        self.list_of_true_ratios_idxs = []
        if pace_ratios:
            self.res['pace'] = []
        if price_ratios:
            self.res['price'] = []
        if time_ratios:
            self.res['time'] = []

    def find_ws(self, data, pivots):
        ws = [0 for _ in range(self.NUMBER_OF_WAVES)]
        ts = [0 for _ in range(self.NUMBER_OF_WAVES)]
        self.list_of_tss = []
        self.data = data
        counter = 0
        last_seen_pivot = 0
        last_itemes = []
        current_trend = 0
        final_pivots = []
        first_reversed_point = []
        deleted_w0 = [-1]
        deleted_t0 = [-1]
        for i, pivot in enumerate(pivots):
            if counter == self.NUMBER_OF_WAVES:
                if pivot == current_trend and last_seen_pivot != 0:
                    ws[-1] = i
                    ts[-1] = i
                    last_itemes = self.get_item_by_index(data, ws)
                    self.calc_ppt_ratios(deleted_w0+last_itemes, deleted_t0+ts)
                    self.list_of_tss.append(deleted_t0+ts)
                    self.list_of_true_ratios_idxs.append(i)

                elif pivot != 0 and pivot != current_trend:
                    last_seen_pivot = pivot

                    self.calc_ppt_ratios(self.get_item_by_index(data, ws + [i]),  ts + [i])
                    self.list_of_tss.append(ts + [i])
                    
                    final_pivots.append(ws[0])
                    first_reversed_point.append(i)
                    deleted_w0 = self.get_item_by_index(data, ws[:1])
                    deleted_t0[0] = ts[0]

                    ws = ws[1:] + [i]
                    ts = ts[1:] + [i]

                    current_trend = pivot
                    last_itemes = self.get_item_by_index(data, ws)
                    self.list_of_true_ratios_idxs.append(i)

                elif last_seen_pivot == -1 or last_seen_pivot == 1:
                    self.calc_ppt_ratios(last_itemes+[data.iloc[i].close], ts+[i])
                    self.list_of_tss.append(ts + [i])

            else:
                self.calc_ppt_ratios([0]*self.NUMBER_OF_RATIOS, [0]*self.NUMBER_OF_RATIOS, before_n_wave=True)
                if pivot != 0:
                    if current_trend != 0 and current_trend == pivot:
                        ws[counter] = i
                        ts[counter] = i
                        self.list_of_tss.append(copy.deepcopy(ts))
                        continue
                    elif current_trend != 0 and current_trend != pivot:
                        counter += 1
                        first_reversed_point.append(i)
                        ws[counter] = i
                        ts[counter] = i
                        current_trend = pivot

                    else:
                        ws[counter] = i
                        ts[counter] = i

                        counter+=1
                        current_trend = -pivot
                    self.list_of_tss.append(copy.deepcopy(ts))
                    
                    if counter == self.NUMBER_OF_WAVES - 1:
                        # not_proved_wave = ws.pop()
                        # ts.pop()
                        deleted_w0 = self.get_item_by_index(data, ws[:1])
                        deleted_t0[0] = ts[0]
                        ws = ws[1:]
                        ts = ts[1:]
                        last_itemes = self.get_item_by_index(data, ws)
                        self.NUMBER_OF_WAVES -= 1
                        last_seen_pivot = -current_trend
                        if i >= self.NORMALIZING_WINDOW_SIZE:
                            self.NORMALIZING_WINDOW_SIZE = i+100
                            print('*'*100, 'normalizing problem')
                else:
                    self.list_of_tss.append(copy.deepcopy(ts))

        self.final_pivots = np.array(final_pivots + ws)
        self.first_reversed_point = np.array(first_reversed_point)
        self.list_of_tss = np.array(self.list_of_tss)
        # self.analyze_pivots_statistics(self.final_pivots, first_reversed_point, 3)

        if self.plot_pivots:
            plt.plot(np.arange(len(data)), data.close)
            plt.scatter(np.array(final_pivots+ws), data.iloc[final_pivots+ws].close, color='r', s = 15)
            plt.scatter(np.array(first_reversed_point), data.iloc[first_reversed_point].close, color='g', s=5)
            plt.show()

    def analyze_pivots_statistics(self, final_pivots, first_reversed_point, sections=4):
        l = []
        for i in range(1, len(final_pivots) -1):
            try:
                l.append((first_reversed_point[i-1] - final_pivots[i])/(final_pivots[i+1] - final_pivots[i]))
            except BaseException as e:
                pass
        for i in range(1, sections):
            print(len(l[np.logical_and(l <= (i)/(sections-1),l > (i-1)/(sections-1))]))

    def calc_ppt_ratios(self, prices, ts, before_n_wave=False):
        if before_n_wave:
            self.res['price'].append(prices) if 'price' in self.res.keys() else None
            self.res['pace'].append(ts) if 'pace' in self.res.keys() else None
            self.res['time'].append(ts) if 'time' in self.res.keys() else None
        else:
            self.res['price'].append(self.calc_price_ratios(prices)) if 'price' in self.res.keys() else None
            self.res['pace'].append(self.calc_pace_ratios(prices, ts)) if 'pace' in self.res.keys() else None
            self.res['time'].append(self.calc_time_ratios(ts)) if 'time' in self.res.keys() else None

    def transform(self, data, pivots, convert_inf_NaN_value=True):
        start = time.time()
        self.find_ws(data, pivots)

        if len(self.final_pivots) < MINIMUM_ZIGZAG_WAVE_COUNT:
            raise MinimumZigzagWaveNotPassed(len(self.final_pivots))
        for key, res in self.res.items():
            values = np.array(res, dtype=np.float32)
            self.normalizer_shifter[key] = []

            if convert_inf_NaN_value:
                values[values == np.Inf] = 0
                values[values == -np.Inf] = 0
                values[np.isnan(values)] = 0

            data = pd.concat([data, pd.DataFrame(values, index=data.index,
                                                 columns=ZigZogAnalyzer.make_list_of_names(self.NUMBER_OF_WAVES+1,
                                                                                           'zigzag_'+key, self.cou))],
                             axis=1)
        # data = pd.concat([data, self.labeling_5wave_structure(data.index)])
        # data = data.iloc[self.list_of_true_ratios_idxs]
        print('*'*30, f'zigzag analyzer time consume: {time.time() - start}', '*'*30, sep='\n')
        return data

    def labeling_5wave_structure(self, index):
        with open(PATH_LIST_OF_ZIGZAG_SEQUENCE_NAME, 'rb') as f:
            types = pickle.load(f)
            types = {item[0]: i for i, item in enumerate(types)}
        cl = np.argsort(self.data.close.values[self.list_of_tss], axis=1)
        seq = [''.join(list(map(lambda x: str(x), item))) for item in cl]
        # return pd.DataFrame(OneHotEncoder(sparse=False).fit_transform(np.array([types[item]
        #                                                                         if item in types else -1
        #                                                                         for item in seq]).reshape(-1, 1)),
        #                     columns=[f'zigzag_structure_{i}' for i in range(123)], index=index)
        return pd.DataFrame(list(map(lambda x: [int(i) for i in bin(x)[2:].zfill(8)],
                                    [types[item]+1 if item in types else 0 for item in seq])),
                            columns=[f'zigzag_structure_{i}' for i in range(8)], index=index)

    @staticmethod
    def get_item_by_index(data, ws):
        return [data['close'].iloc[i] for i in ws]

    @staticmethod
    def change_ts_to_time_period(ts):
        return list(map(lambda x: x[0] - x[1], zip(ts[1:], ts[:-1])))

    @staticmethod
    def make_list_of_vars(waves):
        vars = []
        my_waves = copy.deepcopy(waves)[::-1]
        for i in range(len(my_waves)):
            for j in range(i+1, len(my_waves)):
                vars.append(my_waves[i] - my_waves[j])

        return vars

    @staticmethod
    def make_list_of_names(num_pivot, prefix, postfix=0):
        vars = []
        for i in range(num_pivot):
            for j in range(i+1, num_pivot):
                vars.append(f'waves_{num_pivot - i - 1}__{num_pivot - j - 1}')

        res = []
        for i in range(len(vars)):
            for j in range(i + 1, len(vars)):
                res.append(vars[i] + '_d_' + vars[j])
        res.append('waves_5_d_waves_4')
        res = [prefix + '__' + item + f'_{postfix}' for item in res]
        return res

    @staticmethod
    def calc_price_ratios(ws):
        x = []
        varss = ZigZogAnalyzer.make_list_of_vars(ws)

        for i in range(len(varss)):
            for j in range(i + 1, len(varss)):
                x.append(varss[i] / varss[j])

        x.append(ws[5] / ws[4])

        return x

    @staticmethod
    def calc_time_ratios(ts):
        x = []
        varss = ZigZogAnalyzer.make_list_of_vars(ts)
        for i in range(len(varss)):
            for j in range(i + 1, len(varss)):
                x.append(varss[i] / varss[j])

        x.append(ts[5] / ts[4])

        return x

    @staticmethod
    def calc_pace_ratios(ws, ts):
        pass


if __name__ == '__main__':
    df = pd.read_csv(r"D:\E\projects\test_borse\dataset_4h.csv")
    # pivots = ZigZagCalculator().transform(df, 'close')
    pivots = IterativeZigzag(df.close, .01, -.01).transform()
    df = ZigZogAnalyzer(['close', 'open']).transform(df, pivots)
    print(df)