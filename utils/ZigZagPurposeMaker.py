from .my_zigzag import peak_valley_pivots
from .GlobalVariable import ZIGZAG_UP_THRESHOLD, ZIGZAG_DOWN_THRESHOLD
import copy
import numpy as np


class ZigZagPurposeMaker:
    def __init__(self) -> None:
        pass

    @staticmethod
    def make_label(close, up_thresh, down_thresh):
        y = peak_valley_pivots(close, up_thresh, down_thresh)
        res = []
        current_direction = -1
        for item in y:
            res.append(-current_direction)
            if item == -current_direction:
                current_direction = -current_direction

        res = np.array(res).reshape(-1, 1)
        res[res == -1] = 0
        return res

    @staticmethod
    def transform(x, cols, close, up_thresh=ZIGZAG_UP_THRESHOLD, down_thresh=ZIGZAG_DOWN_THRESHOLD):
        res = ZigZagPurposeMaker.make_label(close, up_thresh, down_thresh)

        if 'purpose' not in cols:
            x = np.append(x, res, axis=1) 
            cols.append('purpose')
        else:
            x[:, cols.index('purpose')] = res

        return x

    def __call__(self, x, cols_name, close, up_thresh=ZIGZAG_UP_THRESHOLD, down_thresh=ZIGZAG_DOWN_THRESHOLD):
        return self.transform(x, cols_name, close, up_thresh, down_thresh)
