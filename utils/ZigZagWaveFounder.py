from sklearn.cluster import MeanShift
from .Zigzag_test_period import ZigZagCalculator
import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
import time


class ZigZagDownTimeFrame:
    def __init__(self, zig_zag_up=.003, zig_zag_down=-.003, shift_interval=.05, price_ratios=True, time_ratios=False, pace_ratios=False) -> None:
        self.zig_zag_up =zig_zag_up
        self.zig_zag_down = zig_zag_down
        self.shift_interval = shift_interval
        self.res = {}
        self.functions = {'price':self.calc_price_ratios, 'pace':self.calc_pace_ratios, 'time':self.calc_time_ratios}
        if pace_ratios:
            self.res['pace'] = []
        if price_ratios:
            self.res['price'] = []
        if time_ratios:
            self.res['time'] = []

    def waves_finder(self):
        data = pd.read_csv('packedDataset.csv')
        data['time'] = pd.to_datetime(data['time'], format='%Y-%m-%d %H:%M:%S')
        data.set_index('time', inplace=True)

        x = data.resample('5min')
        nd = x.agg({'open':'first', 'high':'max', 'low':'min', 'close':'last'})
        data_len = len(nd)
        waves_points = [np.where(ZigZagCalculator(self.zig_zag_up, self.zig_zag_down)(nd.iloc[i: i+48], 'close')!=0)[0] for i in range(0, len(nd) - 48, 48)]
        waves_points.append(np.where(ZigZagCalculator(self.zig_zag_up, self.zig_zag_down)(nd.iloc[data_len - data_len%48:], 'close')!=0)[0])
        close = [nd['close'].iloc[i: i+48].values for i in range(0, len(nd) - 48, 48)] + [nd['close'].iloc[data_len - data_len%48:].values]

        for key in self.res.keys():
            for i in range(len(close)):
                self.res[key].append(self.functions[key](waves_points[i], close[i]))

    def transform(self, data):
        start = time.time()
        self.waves_finder()
        for key,val in self.res.items():
            number_of_cluster = 0
            clusters_points = []
            groups = self.group_by_number_of_member(val)
            additive_interval = 0
            for i, group in enumerate(groups):
                non_nun_group = np.array(group)
                non_nun_group[np.isnan(non_nun_group)] = 0
                while True:
                    cluster = MeanShift(bandwidth=self.shift_interval + additive_interval ).fit(non_nun_group).labels_
                    if np.max(cluster) > 8:
                        additive_interval += .03
                    else:
                        break
                clusters_points.append(cluster + number_of_cluster)
                number_of_cluster += np.max(cluster) + 1

            idxs = [0 for _ in range(len(clusters_points))]
            val_res = []
            for item in val:
                item_len = len(item)-1
                val_res.append(clusters_points[item_len][idxs[item_len]])
                idxs[item_len] += 1
            val_res = np.array(val_res)
            val_res = OneHotEncoder(sparse=False).fit_transform(val_res.reshape(-1, 1))

            print('*'*30,  '*'*30,  '*'*30, number_of_cluster, '*'*30,  '*'*30, sep='\n')
            print('*'*30, f'zigzag subwave time consume: {time.time() - start}', '*'*30, sep='\n')

            for clss_number in range(number_of_cluster):
                data[f'zigzag_sub_wave_{key}_{clss_number}'] = val_res[:, clss_number]
        return data

    @staticmethod
    def group_by_number_of_member(data):
        max_mem_count = max(list(map(lambda x: len(x), data)))
        groups = [[] for _ in range(max_mem_count)]

        for item in data:
            groups[len(item)-1].append(item)
        return groups

    @staticmethod
    def calc_price_ratios(waves, close):
        if len(waves) < 3:
            return np.arctan([close[waves[1]] / close[waves[0]]])/np.pi
        line = close[waves[1:]] - close[waves[:-1]]
        return np.arctan(line[1:] / line[:-1])/np.pi

    @staticmethod
    def calc_time_ratios(waves, close):
        if len(waves) < 3:
            return np.arctan([1])/np.pi
        period = waves[1:] - waves[:-1]
        return np.arctan(period[1:] / period[:-1])/np.pi

    @staticmethod
    def calc_pace_ratios(waves, close):
        if len(waves) < 3:
            return np.arctan([(close[waves[1]] - close[waves[0]]) / (waves[1] - waves[0])])
        period = waves[1:] - waves[:-1]
        line = close[waves[1:]] - close[waves[:-1]]
        return np.arctan((line[1:] / line[:-1]) / (period[1:] / period[:-1]))/np.pi

if __name__== '__main__':
    ZigZagDownTimeFrame().transform(None)