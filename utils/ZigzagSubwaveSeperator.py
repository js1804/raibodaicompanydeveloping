import numpy as np


class ZigzagSubwaveSeperator:
    def __init__(self, final_pivot_positions, candidate_pivot_position, reverse_points, zigzag_res):
        self.final_pivot_positions = final_pivot_positions
        self.candidate_pivot_position = candidate_pivot_position
        self.reverse_points = reverse_points
        self.zigzag_res = zigzag_res

    def get_pivot_to_reverse_point_idx(self):
        reverse_points = self.reverse_points[self.reverse_points > self.final_pivot_positions[0]]
        final_pivots = self.final_pivot_positions[len(self.final_pivot_positions[
                                                          self.final_pivot_positions < reverse_points[0]]) - 1:]
        return list(zip(final_pivots, reverse_points))

    def get_reverse_point_to_pivot_idx(self):
        final_pivots = self.final_pivot_positions[self.final_pivot_positions > self.reverse_points[0]]
        reverse_points = self.reverse_points[len(self.reverse_points[self.reverse_points < final_pivots[0]])-1:]
        return list(zip(reverse_points, final_pivots))

    def get_all_w5_prime_idx(self):
        return list(filter(lambda x: x[1]-x[0] > 0, zip(self.candidate_pivot_position[:-1] + 1,
                                                        self.candidate_pivot_position[1:])))

    def get_all_w4_prime_idx(self):
        return list(map(lambda x: x[0] - 1, self.get_all_w5_prime_idx()))
