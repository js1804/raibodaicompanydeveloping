from .my_zigzag import peak_valley_pivots

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class ZigZagCalculator:
    def __init__(self, up_thresh=.1, down_thresh=-.1) -> None:
        self.up_thresh = up_thresh
        self.down_thresh = down_thresh

    def transform(self, data, col_name='close'):
        return peak_valley_pivots(data[col_name], self.up_thresh, self.down_thresh)

    def __call__(self, data, col_name='close'):
        return self.transform(data, col_name)

    def convert_trades_to_np(self, df, json_data):
        res = []
        final = {}
        k = ["type", "openPrice", "closePrice", "profit", "openStep", "closeState", "margin"]

        for item in json_data.values():
            res.append([])
            for val in item.values():
                res[-1].append([val])

        for i, item in enumerate(k):
            if i==0:
                final[item] = list(map(lambda x: x[i][0], res))
                continue
            final[item] = np.array(list(map(lambda x: x[i][0], res)))

        plt.figure(figsize=(15, 9))
        plt.xlim(0, final['openStep'][-1])
        plt.ylim(final['openPrice'].min()*0.99, final['openPrice'].max()*1.01)
        plt.plot(np.arange(final['openStep'][-1]+1), df.close.iloc[-final['openStep'][-1]-1 -4:-4], 'k:', alpha=0.5, color='black')
        # plt.plot(final['openStep'], final['openPrice'], 'k-')

        for i, step in enumerate(final['openStep']):
            if final['type'][i] == 'sell':
                plt.plot([final['openStep'][i], final['closeState'][i]], [final['openPrice'][i], final['closePrice'][i]], 'k-', color='r')
            else:
                plt.plot([final['openStep'][i], final['closeState'][i]], [final['openPrice'][i], final['closePrice'][i]], 'k-', color='g')
        # plt.show()


        pivots = self.transform(df.iloc[-final['openStep'][-1]-1 -4:-4].set_index(np.arange(final['openStep'][-1]+1)), 'close')
        plt.plot(np.arange(len(df.close.iloc[-final['openStep'][-1]-1 -4:-4])), df.close.iloc[-final['openStep'][-1]-1 -4:-4], 'k:', alpha=0.5)
        plt.plot(np.arange(len(df.close.iloc[-final['openStep'][-1]-1 -4:-4]))[pivots != 0], df.close.iloc[-final['openStep'][-1]-1 -4:-4][pivots != 0], 'k-')
        plt.scatter(np.arange(len(df.close.iloc[-final['openStep'][-1]-1 -4:-4]))[pivots == 1], df.close.iloc[-final['openStep'][-1]-1 -4:-4][pivots == 1], color='g')
        plt.scatter(np.arange(len(df.close.iloc[-final['openStep'][-1]-1 -4:-4]))[pivots == -1], df.close.iloc[-final['openStep'][-1]-1 -4:-4][pivots == -1], color='r')
        plt.show()
        # plt.savefig(r'C:\Users\Asus\Desktop\Figure_1.jpg', dpi=1200)

        return final

    def plot_pivots(self, data, col_name):
        pivots = self.transform(data, col_name)
        plt.figure(figsize=(15, 9))
        plt.xlim(0, len(data[col_name]))
        plt.ylim(data[col_name].min()*0.99, data[col_name].max()*1.01)
        plt.plot(np.arange(len(data[col_name])), data[col_name], 'k:', alpha=0.5)
        plt.plot(np.arange(len(data[col_name]))[pivots != 0], data[col_name][pivots != 0], 'k-')
        plt.scatter(np.arange(len(data[col_name]))[pivots == 1], data[col_name][pivots == 1], color='g')
        plt.scatter(np.arange(len(data[col_name]))[pivots == -1], data[col_name][pivots == -1], color='r')
        plt.show()

if __name__ == '__main__':
    df = pd.read_csv(r"D:\E\projects\test_borse\forex_ohlcv_5m.csv")
    with open(r'D:\E\projects\test_borse\outputs\trades.json', 'r') as f:
        json_data = json.load(f)
    ZigZagCalculator(up_thresh=.002, down_thresh=-.002).convert_trades_to_np(df, json_data)
    ZigZagCalculator().plot_pivots(df, 'close')
