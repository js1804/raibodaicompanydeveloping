import pyzipper
import random
import glob
import os


class ZipProjectPythonFiles:
    def __init__(self, name_to_save=None):
        self.name_to_save = name_to_save if name_to_save is not None else str(random.randint(100000, 10000000)) + '.zip'
        self.founded_py_files = []
        self.save()

    def save(self):
        self.founded_py_files = glob.glob('./**/*.py', recursive=True)
        print(self.name_to_save, self.founded_py_files)
        for file in self.founded_py_files:
            with pyzipper.ZipFile(self.name_to_save, mode='a') as f:
                f.write(file)
        self.name_to_save = os.path.abspath(self.name_to_save)
