# -*- coding: utf-8 -*-
"""
Created on Sun Aug 21 18:34:33 2022

@author: a.h
"""

import matplotlib.pyplot as plt
import numpy as np
import mplfinance as fplt
import json


class showTraderResults:

    def _init_(self, trades, yReal, margins, dirPath):
        self.trades = trades
        self.realTestOutputH4 = yReal
        self.margins = margins
        self.dirPath = dirPath

    def statementResults(self):
        trades_ = self.trades

        numberOfBuy = 0
        numberOfSell = 0
        numberOfPositveSell = 0
        numberOfPositiveBuy = 0
        profitInBuyTrades = 0
        profitInSellTrades = 0
        netProfit = 0
        totallProfit = 0
        totallLoss = 0
        numberOfLossBuy = 0
        lossInBuyTrades = 0
        numberOfLossSell = 0
        lossInSellTrades = 0
        netBuy = 0
        netSell = 0
        listOfMargin = list()

        for i in range(0, len(trades_)):
            ####################################### positive profit section ####################
            listOfMargin.append(trades_["step" + str(i)]["margin"])

            if trades_["step" + str(i)]["profit"] > 0:
                totallProfit += trades_["step" + str(i)]["profit"]

            ####################################### loss section #################################
            if trades_["step" + str(i)]["profit"] < 0:
                totallLoss += trades_["step" + str(i)]["profit"]

            ################################ buy section ############################################
            if trades_["step" + str(i)]["type"] == "buy":
                numberOfBuy += 1
                if trades_["step" + str(i)]["profit"] > 0:
                    numberOfPositiveBuy += 1
                    profitInBuyTrades += trades_["step" + str(i)]["profit"]

                if trades_["step" + str(i)]["profit"] < 0:
                    numberOfLossBuy += 1
                    lossInBuyTrades += trades_["step" + str(i)]["profit"]

            ############################################### sell section #####################################
            if trades_["step" + str(i)]["type"] == "sell":
                numberOfSell += 1

                if trades_["step" + str(i)]["profit"] > 0:
                    numberOfPositveSell += 1
                    profitInSellTrades += trades_["step" + str(i)]["profit"]

                if trades_["step" + str(i)]["profit"] < 0:
                    numberOfLossSell += 1
                    lossInSellTrades += trades_["step" + str(i)]["profit"]

            ################################## total section ###################################################3
            netProfit += trades_["step" + str(i)]["profit"]
        netBuy = profitInBuyTrades - lossInBuyTrades
        netSell = profitInSellTrades - lossInSellTrades

        winRate = ((numberOfPositiveBuy + numberOfPositveSell) / len(trades_)) * 100
        lossRate = ((numberOfLossSell + numberOfLossBuy) / len(trades_)) * 100

        finalResult = dict()

        finalResult["numberOfTrades"] = len(trades_)
        finalResult["numberOfBuy"] = numberOfBuy
        finalResult["numberOfSell"] = numberOfSell
        finalResult["numberOfPositveSell"] = numberOfPositveSell
        finalResult["numberOfPositiveBuy"] = numberOfPositiveBuy
        finalResult["profitInBuyTrades"] = profitInBuyTrades
        finalResult["profitInSellTrades"] = profitInSellTrades
        finalResult["netProfit"] = netProfit
        finalResult["totallProfit"] = totallProfit
        finalResult["totallLoss"] = totallLoss
        finalResult["numberOfLossBuy"] = numberOfLossBuy
        finalResult["lossInBuyTrades"] = lossInBuyTrades
        finalResult["numberOfLossSell"] = numberOfLossSell
        finalResult["lossInSellTrades"] = lossInSellTrades
        finalResult["netBuy"] = netBuy
        finalResult["netSell"] = netSell
        finalResult["winRate"] = winRate
        finalResult["lossRate"] = lossRate
        with open(f'{self.dirPath}/statement.json', 'w') as jFile:
            json.dump(finalResult, jFile)

        return (trades_, finalResult)