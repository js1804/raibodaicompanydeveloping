from .GlobalVariable import *
from .BackTraderTesting import backTradeTesting
import keras_tuner as ktu

from .LSTM import LSTMModel
import numpy as np
import time

FEATURE_COUNT = None
WINDOW_SIZE_TUNER = 3
class ModelToTune(ktu.HyperModel):
    def build(self, hp):
        model = LSTMModel([], ACTIVATION_FUNCTION, OPTIMIZER, LR, 
                    None, DROP_OUT, LOSS_FUNCTION, EPOCHS, BATCH_SIZE, False, WINDOW_SIZE_TUNER, FEATURE_COUNT, 
                    dif_shape_lstms=[[2,30,1]]*len(COUPLES), feature_count_diff=[2]*len(COUPLES), prediction_mode=PREDICTION_MODE)
        return model.build_ds_model(hp)

    def fit(self, hp, model, yReal, x, y, x_t, y_t, epochs, callbacks=None, **kwargs):
        def run_train_step(in_x, in_y):
            model.fit(in_x, in_y, epochs=1, batch_size=BATCH_SIZE, verbose=False, validation_split=0.2)

        for callback in callbacks:
            callback.model = model

        best_profit = -float("inf")

        for epoch in range(epochs):
            print(f"Epoch: {epoch + 1}")

            run_train_step(x, y)

            profit = self.calc_profit(model, yReal, x_t, y_t)
            for callback in callbacks:
                callback.on_epoch_end(epoch, logs={"profit": profit})

            best_profit = max(best_profit, profit)

        # Return the evaluation metric value.
        return best_profit

    def calc_profit(self, model, yReal, x_t, y_t):
        res = backTradeTesting(model, x_t, np.argmax(y_t, axis=1), 100000, 100, yReal, in_tune_mode=True).final()
        print(res['netProfit'])
        return res['netProfit']

class KerasTunner():
    def __init__(self, handle, epochs=10, directory='./tunning', project_name='tuner', search_mode='BayesianOptimization'):
        
        self.handle = handle
        self.epochs = epochs
        self.directory = directory
        self.project_name = project_name
        self.tunner = getattr(ktu, search_mode)

    def start(self, max_trial=1000, wait_window=1, future_step=1):
        x, y, x_test, y_test = self.handle.make_train_test_data_dictionary(COUPLES, future_step=future_step)
        yReal = self.handle.close_.iloc[-y_test.shape[0]-wait_window: -wait_window if wait_window!=0 else None].values
        global FEATURE_COUNT
        FEATURE_COUNT = self.handle.feature_count
        tuner = self.tunner(hypermodel=ModelToTune(),
                                objective=ktu.Objective('profit', 'max'), 
                                max_trials=max_trial,
                                directory=self.directory,
                                project_name=self.project_name,)
        # tuner.search_space_summary()
        tuner.search(yReal=yReal, x=x, y=y, x_t=x_test, y_t=y_test, epochs=self.epochs)
        print('*'*40, 'tunning history results:\n\n', sep='\n')
        tuner.results_summary()
        print('*'*40, 'tunning best result:', sep='\n')
        print(*tuner.get_best_hyperparameters(1)[0].get_config()['values'].items(), sep='\n')
        print('*'*40, sep='\n')


    def load(self, save_best_model=False):
        global FEATURE_COUNT
        FEATURE_COUNT = self.handle.feature_count
        tuner = self.tunner(hypermodel=ModelToTune(),
                                objective=ktu.Objective('profit', 'max'), 
                                max_trials=0,
                                directory=self.directory,
                                project_name=self.project_name,
                                overwrite=False)

        print('*'*40, 'tunning history results:\n\n', sep='\n')
        tuner.results_summary()
        print('*'*40, 'tunning best result:', sep='\n')
        print(*tuner.get_best_hyperparameters(1)[0].get_config()['values'].items(), sep='\n')
        print('*'*40, sep='\n')

        if save_best_model==True:
            best_model = tuner.get_best_models(1)[0]
            time_format = '%Y_%m_%d_%H_%M_%S'
            name = 'LSTM_DS'
            best_model.save(PATH_TO_SAVE_MODEL_AFTER_TRAIN + f'{time.strftime(time_format)}_{name}')
